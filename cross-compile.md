## Install stuff
```
sudo dpkg --add-architecture arm64
sudo apt update
sudo apt remove -y libpsl-dev libsoup-3.0-dev libshumate-dev
sudo apt install -y libshumate-dev:arm64 libgtk-4-dev:arm64 libadwaita-1-dev:arm64 libcairo2-dev:arm64 libstd-rust-dev:arm64 cargo rustc gcc-aarch64-linux-gnu libssl-dev:arm64 libfeedback-dev:arm64
```

## Build stuff
```
PKG_CONFIG_PATH=/usr/lib/aarch64-linux-gnu/pkgconfig PKG_CONFIG_ALLOW_CROSS=1 cargo build --target aarch64-unknown-linux-gnu --release
```

https://users.rust-lang.org/t/cross-compilation-using-debian-rust-not-rustup/20022/18
