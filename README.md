# Electrolysis

Sadly public charging of electric vehicles often requires either an app or at least an account.
Until now, those apps only existed for Android and iOS. It's time we changed that and created
something for Mobile Linux.

Electrolysis provides a native GTK application for EV charging.  
![Electrolysis main screen](doc/electrolysis-main-screen.png "Electrolysis main screen")

## Support
Currently Electrolysis supports the following charging networks:
* Chargefox
    * Login, map, charging, payment and RFID card management working
* Tesla
    * Login, and map tested and working.
    * Charging should work, but untested.
    * Payments and RFID card management not supported.
* BP Pulse
    * Login, map and RFID card management working.
    * Charging not working.
    * Payments, and stopping a charge not supported.
* Evie
    * Map only.

If you have a charge network you'd like to add please reach out.

## Current features:
* View chargers in an area
* View Charger details
* Start a charge session
* View current state of a charge session
* Stop a charge session
* List and add order RFID charge cards
* Add payment details
