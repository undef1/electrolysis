mod imp;

pub use crate::chargefox::station::location_drawer::LocationDrawerLocationChargeStationsConnectors as Connector;
use crate::gui::Network;
use adw::subclass::prelude::*;
use glib::Object;
use gtk::glib;

glib::wrapper! {
    pub struct Station(ObjectSubclass<imp::Station>);
}

impl Station {
    pub fn new(data: StationData) -> Self {
        let s: Station = Object::builder()
            .property("enabled", data.enabled)
            .property("id", data.clone().id)
            .property("power", data.clone().power)
            .property("idle-fee", data.clone().idle_fee)
            .property("name", data.clone().name)
            .property("online", data.online)
            .property("planned", data.planned)
            .property("pricing-plan", data.clone().pricing_plan)
            .property("public-charger", data.public_charger)
            .property("unavailable", data.unavailable)
            .property("unavailable-message", data.clone().unavailable_message)
            .build();
        *s.imp().data.write().unwrap() = data;
        s
    }

    pub fn station_data(&self) -> StationData {
        self.imp().data.read().unwrap().clone()
    }

    pub fn from_station_data(data: StationData) -> Self {
        Self::new(data)
    }

    pub fn available(&self) -> bool {
        !self.unavailable()
    }

    pub fn network(&self) -> Network {
        self.imp().data.read().unwrap().network
    }
}

#[derive(Default, Clone)]
pub struct StationData {
    pub enabled: bool,
    pub id: String,
    pub idle_fee: String,
    pub name: String,
    pub online: bool,
    pub planned: bool,
    pub power: String,
    pub price_id: i64,
    pub pricing_plan: String,
    pub public_charger: bool,
    pub unavailable: bool,
    pub unavailable_message: String,
    pub connectors: Option<Vec<Connector>>,
    pub network: Network,
    pub site_guid: Option<String>,
    pub site_id: Option<String>,
}
