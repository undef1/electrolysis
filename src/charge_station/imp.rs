use std::sync::RwLock;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::Properties;
use gtk::glib;

use super::StationData;

#[derive(Properties, Default)]
#[properties(wrapper_type = super::Station)]
pub struct Station {
    #[property(name = "enabled", get, set, type = bool, member = enabled)]
    #[property(name = "id", get, set, type = String, member = id)]
    #[property(name = "power", get, set, type = String, member = power)]
    #[property(name = "idle-fee", get, set, type = String, member = idle_fee)]
    #[property(name = "name", get, set, type = String, member = name)]
    #[property(name = "online", get, set, type = bool, member = online)]
    #[property(name = "planned", get, set, type = bool, member = planned)]
    #[property(name = "pricing-plan", get, set, type = String, member = pricing_plan)]
    #[property(name = "public-charger", get, set, type = bool, member = public_charger)]
    #[property(name = "unavailable", get, set, type = bool, member = unavailable)]
    #[property(name = "unavailable-message", get, set, type = String, member = unavailable_message)]
    pub data: RwLock<StationData>,
}

#[glib::object_subclass]
impl ObjectSubclass for Station {
    const NAME: &'static str = "Station";
    type Type = super::Station;
}

#[glib::derived_properties]
impl ObjectImpl for Station {}
