use super::window::Window;
use adw::prelude::*;
use adw::Application;
use gtk::gio::ApplicationFlags;
use gtk::glib;
use std::sync::OnceLock;
use tokio::runtime::Runtime;

pub const APP_ID: &str = "com.gitlab.undef1.Electrolysis";

pub fn runtime() -> &'static Runtime {
    static RUNTIME: OnceLock<Runtime> = OnceLock::new();
    RUNTIME.get_or_init(|| Runtime::new().expect("Setup of Tokio Runtime Failed"))
}

pub fn gui_exec() {
    let app = Application::builder()
        .application_id(APP_ID)
        .flags(ApplicationFlags::HANDLES_OPEN)
        .build();

    libfeedback::init(APP_ID).unwrap();
    app.connect_activate(build_ui);
    app.run();
    libfeedback::functions::uninit();
}

fn build_ui(app: &Application) {
    let window = Window::new(app);
    window.present();
}

#[derive(Debug, Default, Copy, Clone, PartialEq, Eq, glib::Enum, glib::Variant)]
#[enum_type(name = "Network")]
#[enum_value]
pub enum Network {
    Chargefox,
    Tesla,
    #[default]
    All,
    Evie,
    Bppulse,
    Exploren,
}

impl Network {
    pub fn from_str(n: &str) -> Network {
        match n {
            "chargefox" => Network::Chargefox,
            "tesla" => Network::Tesla,
            "all" => Network::All,
            "evie" => Network::Evie,
            "bppulse" => Network::Bppulse,
            "exploren" => Network::Exploren,
            _ => Network::default(),
        }
    }
    pub fn to_str(self) -> &'static str {
        match self {
            Network::Chargefox => "chargefox",
            Network::Tesla => "tesla",
            Network::Evie => "evie",
            Network::Bppulse => "bppulse",
            Network::Exploren => "exploren",
            Network::All => "all",
        }
    }
}

impl std::fmt::Display for Network {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Network::Chargefox => write!(f, "Chargefox"),
            Network::Tesla => write!(f, "Tesla"),
            Network::Evie => write!(f, "Evie"),
            Network::Bppulse => write!(f, "BP Pulse"),
            Network::Exploren => write!(f, "Exploren"),
            Network::All => write!(f, "All"),
        }
    }
}
