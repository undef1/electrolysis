mod imp;

use crate::gui::Network;
use adw::subclass::prelude::*;
use glib::Object;
use gtk::glib;

glib::wrapper! {
    pub struct Charger(ObjectSubclass<imp::Charger>)
        @extends gtk::Widget;
}

impl Charger {
    pub fn new(data: ChargerData) -> Self {
        let s: Charger = Object::builder()
            .property("id", data.clone().id)
            .property("lat", data.lat)
            .property("long", data.long)
            .property("free", data.free)
            .property("location", data.clone().location)
            .property("maxpower", data.maxpower)
            .build();
        *s.imp().data.write().unwrap() = data;
        s
    }

    pub fn from_charger_data(data: ChargerData) -> Self {
        Self::new(data)
    }

    pub fn charger_type(&self) -> ChargerType {
        self.imp().data.read().unwrap().chargertype
    }
    pub fn network(&self) -> Network {
        self.imp().data.read().unwrap().network
    }
}

#[derive(Default, Clone, PartialEq, Eq, Copy)]
pub enum ChargerType {
    #[default]
    AC,
    DC,
}

#[derive(Default, Clone)]
pub struct ChargerData {
    pub id: String,
    pub lat: f64,
    pub long: f64,
    pub free: bool,
    pub location: String,
    pub maxpower: i64,
    pub chargertype: ChargerType,
    pub network: Network,
}
