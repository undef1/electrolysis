use std::sync::RwLock;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::Properties;
use gtk::glib;

use super::ChargerData;

#[derive(Properties, Default)]
#[properties(wrapper_type = super::Charger)]
pub struct Charger {
    #[property(name = "id", get, set, type = String, member = id)]
    #[property(name = "lat", get, set, type = f64, member = lat)]
    #[property(name = "long", get, set, type = f64, member = long)]
    #[property(name = "free", get, set, type = bool, member = free)]
    #[property(name = "location", get, set, type = String, member = location)]
    #[property(name = "maxpower", get, set, type = i64, member = maxpower)]
    pub data: RwLock<ChargerData>,
}

#[glib::object_subclass]
impl ObjectSubclass for Charger {
    const NAME: &'static str = "Charger";
    type Type = super::Charger;
    type ParentType = gtk::Widget;
}

#[glib::derived_properties]
impl ObjectImpl for Charger {}

impl WidgetImpl for Charger {}
