mod bp_pulse;
mod charge_session_window;
mod charge_station;
mod charge_station_window;
mod chargefox;
mod charger;
mod evie;
mod exploren;
mod gui;
mod loginwindow;
mod payments_window;
mod preferenceswindow;
mod rfidcard;
mod rfidwindow;
mod secret;
mod tesla;
mod user;
mod window;
use crate::gui::gui_exec;

fn main() {
    env_logger::init();
    gui_exec();
}
