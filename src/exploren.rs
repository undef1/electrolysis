pub mod imp;

pub mod consts {
    pub const URL: &str = "https://exploren.au.charge.ampeco.tech";
    pub const TOKEN_HEADER: &str = "Authorization";
    pub const VERSION_HEADER: &str = "X-App-Version";
    pub const APP_ID_HEADER: &str = "X-Mobile-App-Bundle-Id";
    pub const APP_ID: &str = "au.com.exploren.cp.app";
    pub const INTERNAL_VERSION_HEADER: &str = "X-Internal-App-Version";
    pub const VERSION_VALUE: &str = "2.168.0";
    pub const USER_AGENT: &str = "okhttp/4.9.2";
    pub const GRANT_TYPE: &str = "password";
    pub const CLIENT_ID: &str = "1";
    pub const CLIENT_SECRET: &str = "URqvV7q90Tbi8SJrrhdhlH8aKScw0UugvnbuhyWW";
}
