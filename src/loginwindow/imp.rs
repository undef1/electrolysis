use crate::tesla::login as tlogin;
use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::subclass::InitializingObject;
use gtk::gio::Settings;
use gtk::{glib, CompositeTemplate};
use std::cell::RefCell;

use crate::gui::{Network, APP_ID};

#[derive(CompositeTemplate, Default)]
#[template(file = "../../resources/login.ui")]
pub struct LoginWindow {
    #[template_child]
    pub sign_in: TemplateChild<gtk::Button>,
    #[template_child]
    pub username: TemplateChild<adw::EntryRow>,
    #[template_child]
    pub password: TemplateChild<adw::PasswordEntryRow>,
    #[template_child]
    pub prompt: TemplateChild<gtk::Label>,
    #[template_child]
    pub link: TemplateChild<gtk::LinkButton>,
    // Some auth methods require us to store something between initiating and finishing the auth
    // dance
    pub stashed_token: RefCell<String>,
}

#[glib::object_subclass]
impl ObjectSubclass for LoginWindow {
    const NAME: &'static str = "LoginWindow";
    type Type = super::LoginWindow;
    type ParentType = adw::PreferencesDialog;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for LoginWindow {
    fn constructed(&self) {
        let settings = Settings::new(APP_ID);
        let network = Network::from_str(settings.string("network").as_ref());
        self.parent_constructed();
        let obj = self.obj();
        if network == Network::Tesla {
            let prompt = self.prompt.get();
            prompt.set_label("Please press the below link then copy the URL after login into the URL field below");
            prompt.set_visible(true);
            let (tesla_link, code_verifier) = tlogin::generate_login_url();
            let link = self.link.get();
            link.set_uri(tesla_link.as_ref());
            link.set_visible(true);
            link.set_label("auth.tesla.com");

            self.stashed_token.replace(code_verifier);

            self.username.get().set_visible(false);
            self.password.get().set_title("URL");
        }
        let obj_clone = obj.clone();
        self.sign_in.connect_clicked(move |_| {
            let obj_clone2 = obj_clone.clone();
            glib::spawn_future_local(async move {
                obj_clone2.sign_in_button_triggered(network).await;
            });
        });
    }
}

impl WidgetImpl for LoginWindow {}
impl PreferencesDialogImpl for LoginWindow {}
impl AdwDialogImpl for LoginWindow {}
