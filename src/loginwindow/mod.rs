mod imp;

use crate::bp_pulse::imp as bpimp;
use crate::chargefox::imp as cfimp;
use crate::exploren::imp as exploren;
use crate::gui::Network;
use crate::secret::secret;
use crate::tesla::login as tlogin;
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::{gio, glib};
use glib::Object;

glib::wrapper! {
    pub struct LoginWindow(ObjectSubclass<imp::LoginWindow>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}
impl Default for LoginWindow {
    fn default() -> Self {
        Self::new()
    }
}

impl LoginWindow {
    pub fn new() -> Self {
        Object::builder().build()
    }

    async fn sign_in_button_triggered(&self, network: Network) {
        let username = self.imp().username.get().text().to_string();
        let password = self.imp().password.get().text().to_string();
        let token = match network {
            Network::Chargefox => match cfimp::login(username, password).await {
                Ok(t) => t,
                Err(e) => panic!("{}", e),
            },
            Network::Tesla => {
                let code_verifier = self.imp().stashed_token.borrow();
                match tlogin::login(password, code_verifier.to_string()) {
                    Ok(t) => t,
                    Err(e) => panic!("{}", e),
                }
            }
            Network::All => "".to_owned(),
            Network::Evie => "".to_owned(),
            Network::Exploren => match exploren::login(username, password).await {
                Ok(t) => t,
                Err(e) => panic!("{}", e),
            },
            Network::Bppulse => match bpimp::login(username, password).await {
                Ok(t) => t,
                Err(e) => panic!("{}", e),
            },
        };
        let res = secret::store_login(network.to_str(), token.as_ref());
        if res.is_err() {
            panic!("{:#?}", res.err());
        }
        let token = secret::retreive_login_token(network.to_str()).unwrap();
        println!("{:#?}", token);
        self.close();
    }
}
