use adw::subclass::prelude::*;

use glib::subclass::InitializingObject;
use gtk::{gio, glib, CompositeTemplate};
use std::cell::RefCell;

#[derive(CompositeTemplate, Default)]
#[template(file = "../../resources/charge_station.ui")]
pub struct ChargeStationWindow {
    #[template_child]
    pub toast_overlay: TemplateChild<adw::ToastOverlay>,
    #[template_child]
    pub sidebar_lv: TemplateChild<gtk::ListView>,
    pub stations: RefCell<Option<gio::ListStore>>,
}

#[glib::object_subclass]
impl ObjectSubclass for ChargeStationWindow {
    const NAME: &'static str = "ChargeStationWindow";
    type Type = super::ChargeStationWindow;
    type ParentType = adw::Dialog;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for ChargeStationWindow {
    fn constructed(&self) {
        self.parent_constructed();
    }
}

impl WidgetImpl for ChargeStationWindow {}
impl PreferencesDialogImpl for ChargeStationWindow {}
impl AdwDialogImpl for ChargeStationWindow {}
