mod imp;

use crate::bp_pulse::imp as bp;
use crate::charge_station::Station;
use crate::chargefox;
use crate::charger::Charger;
use crate::evie;
use crate::exploren::imp as exploren;
use crate::gui::Network;
use crate::tesla;
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::{gio, glib, ActionRow, ExpanderRow, Toast};
use glib::Object;
use gtk::{Align, Button, Label, ListItem, NoSelection, SignalListItemFactory};
use libfeedback::Event;

glib::wrapper! {
    pub struct ChargeStationWindow(ObjectSubclass<imp::ChargeStationWindow>)
        @extends adw::Dialog, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl Default for ChargeStationWindow {
    fn default() -> Self {
        Self::new()
    }
}

impl ChargeStationWindow {
    pub fn new() -> Self {
        let s: ChargeStationWindow = Object::builder().build();
        s
    }

    fn stations(&self) -> gio::ListStore {
        self.imp()
            .stations
            .borrow()
            .clone()
            .expect("Could not get stations")
    }

    pub async fn charger_triggered(&self, charger: Charger) {
        self.setup_stations().await;
        self.setup_station_factory().await;
        let toast_overlay = self.imp().toast_overlay.get();
        let (toast_sender, toast_receiver) = async_channel::bounded(10);
        let (station_sender, station_receiver) = async_channel::bounded(10);
        glib::spawn_future_local(async move {
            match charger.network() {
                Network::Chargefox => {
                    let stations = match chargefox::imp::populate_stations(charger).await {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("Toast channel must be open");
                            return;
                        }
                    };
                    if stations.is_empty() {
                        toast_sender
                            .send("No stations found for this charger".to_owned())
                            .await
                            .expect("Toast channel must be open");
                        return;
                    }
                    station_sender
                        .send(stations)
                        .await
                        .expect("Station channel must be open");
                }
                Network::Tesla => {
                    let stations = match tesla::imp::populate_stations(charger).await {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("Toast channel must be open");
                            return;
                        }
                    };
                    if stations.is_empty() {
                        toast_sender
                            .send("No stations found for this charger".to_owned())
                            .await
                            .expect("Toast channel must be open");
                        return;
                    }
                    station_sender
                        .send(stations)
                        .await
                        .expect("Station channel must be open");
                }
                Network::All => panic!("Network all cannot have chargers"),
                Network::Evie => {
                    let stations = match evie::imp::populate_stations(charger).await {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("Toast channel must be open");
                            return;
                        }
                    };
                    if stations.is_empty() {
                        toast_sender
                            .send("No stations found for this charger".to_owned())
                            .await
                            .expect("Toast channel must be open");
                        return;
                    }
                    station_sender
                        .send(stations)
                        .await
                        .expect("Station channel must be open");
                }
                Network::Bppulse => {
                    let stations = match bp::populate_stations(charger).await {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("Toast channel must be open");
                            return;
                        }
                    };
                    if stations.is_empty() {
                        toast_sender
                            .send("No stations found for this charger".to_owned())
                            .await
                            .expect("Toast channel must be open");
                        return;
                    }
                    station_sender
                        .send(stations)
                        .await
                        .expect("Station channel must be open");
                }
                Network::Exploren => {
                    let stations = match exploren::populate_stations(charger).await {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("Toast channel must be open");
                            return;
                        }
                    };
                    if stations.is_empty() {
                        toast_sender
                            .send("No stations found for this charger".to_owned())
                            .await
                            .expect("Toast channel must be open");
                        return;
                    }
                    station_sender
                        .send(stations)
                        .await
                        .expect("Station channel must be open");
                }
            };
        });
        let stations = self.stations();
        glib::spawn_future_local(async move {
            while let Ok(new_stations) = station_receiver.recv().await {
                for station in new_stations {
                    stations.append(&station);
                }
            }
        });
        glib::spawn_future_local(async move {
            while let Ok(title) = toast_receiver.recv().await {
                let toast = Toast::builder().timeout(5).title(title).build();
                toast_overlay.add_toast(toast);
            }
        });
    }

    async fn setup_stations(&self) {
        let model = gio::ListStore::new::<Station>();
        self.imp().stations.replace(Some(model));
        let sorter = gtk::CustomSorter::new(|s1, s2| {
            let station1 = s1
                .downcast_ref::<Station>()
                .expect("Items must be stations");
            let station2 = s2
                .downcast_ref::<Station>()
                .expect("Items must be stations");
            station1.name().cmp(&station2.name()).into()
        });
        let sort_model = gtk::SortListModel::new(Some(self.stations()), Some(sorter.clone()));
        let selection_model = NoSelection::new(Some(sort_model));
        self.imp().sidebar_lv.set_model(Some(&selection_model));
    }

    async fn setup_station_factory(&self) {
        let factory = SignalListItemFactory::new();
        let obj = self.clone();

        factory.connect_setup(move |_, list_item| {
            let row = ExpanderRow::new();
            list_item
                .downcast_ref::<ListItem>()
                .expect("Needs to be a list item")
                .set_child(Some(&row));
        });
        factory.connect_bind(move |_, list_item| {
            let obj_clone2 = obj.clone();
            // Charge Location
            let station = list_item
                .downcast_ref::<ListItem>()
                .expect("Needs to be list item")
                .item()
                .and_downcast::<Station>()
                .expect("Must be a station");
            let row = list_item
                .downcast_ref::<ListItem>()
                .expect("Needs to be a list item")
                .child()
                .and_downcast::<ExpanderRow>()
                .expect("child must be an action row");
            station
                .bind_property("name", &row, "title")
                .sync_create()
                .build();
            let network_row = ActionRow::builder()
                .hexpand(true)
                .vexpand(true)
                .title("Network")
                .build();
            network_row.add_suffix(&Label::new(Some(&format!("{}", station.network()))));
            row.add_row(&network_row);
            let pricing_row = ActionRow::builder()
                .hexpand(true)
                .vexpand(true)
                .title("Pricing Plan")
                .build();
            let price = station.pricing_plan();
            if price.len() > 30 {
                pricing_row.set_subtitle(&price);
            } else {
                pricing_row.add_suffix(&Label::new(Some(&price)));
            }
            row.add_row(&pricing_row);
            let power_row = ActionRow::builder()
                .hexpand(true)
                .vexpand(true)
                .title("Power")
                .build();
            power_row.add_suffix(&Label::new(Some(&station.power())));
            row.add_row(&power_row);
            let idle_row = ActionRow::builder()
                .hexpand(true)
                .vexpand(true)
                .title("Idle Fee")
                .build();
            idle_row.add_suffix(&Label::new(Some(&station.idle_fee())));
            row.add_row(&idle_row);
            let online_row = ActionRow::builder()
                .hexpand(true)
                .vexpand(true)
                .title("Online")
                .build();
            online_row.add_suffix(&Label::new(Some(&station.online().to_string())));
            row.add_row(&online_row);
            let public_row = ActionRow::builder()
                .hexpand(true)
                .vexpand(true)
                .title("Public")
                .build();
            public_row.add_suffix(&Label::new(Some(&station.public_charger().to_string())));
            row.add_row(&public_row);
            if station.unavailable() {
                let unavailable_note_row = ActionRow::builder()
                    .hexpand(true)
                    .vexpand(true)
                    .title("Unavailable Reason")
                    .build();
                unavailable_note_row.add_suffix(&Label::new(Some(&station.unavailable_message())));
                row.add_row(&unavailable_note_row);
            }
            // connectors
            let station_data = station.station_data();
            if station_data.connectors.is_some() {
                for connector in station_data.connectors.unwrap() {
                    let available = connector.clone().available.unwrap_or(false);
                    let teathered = connector.clone().tethered.unwrap_or(true);
                    let mut start_title = "Start Charge".to_owned();
                    if !available {
                        let soc = match connector.clone().active_charge_session {
                            Some(a) => a.state_of_charge.unwrap_or("".to_owned()),
                            None => "".to_owned(),
                        };
                        "Occupied ".clone_into(&mut start_title);
                        start_title.push_str(&soc);
                    }
                    if connector.name.is_some() {
                        let name_row = ActionRow::builder()
                            .hexpand(true)
                            .vexpand(true)
                            .title("Connector Name")
                            .build();
                        name_row.add_suffix(&Label::new(connector.clone().name.as_deref()));
                        row.add_row(&name_row);
                    }
                    let mut plug_type = connector.clone().plug.type_name;
                    if !teathered {
                        plug_type.push_str(" - BYO Cable");
                    }
                    let mut connector_name_title = "";
                    if !plug_type.is_empty() {
                        connector_name_title = "Plug";
                    }
                    let connector_row = ActionRow::builder()
                        .hexpand(true)
                        .vexpand(true)
                        .title(connector_name_title)
                        .subtitle(plug_type)
                        .build();
                    let charge_button = Button::builder()
                        .valign(Align::Center)
                        .label(&start_title)
                        .build();
                    let station_clone = station.clone();
                    let connector_clone = connector.clone();
                    let obj_clone3 = obj_clone2.clone();
                    charge_button.connect_clicked(move |_| {
                        let station_clone2 = station_clone.clone();
                        let connector_clone2 = connector_clone.clone();
                        let obj_clone4 = obj_clone3.clone();
                        glib::spawn_future_local(async move {
                            let toast_overlay = obj_clone4.imp().toast_overlay.get();
                            let response = match station_clone2.network() {
                                Network::Chargefox => {
                                    obj_clone4.notify_charge_status(station_clone2.network());
                                    chargefox::imp::start_charge(connector_clone2).await
                                }
                                Network::Tesla => {
                                    match tesla::imp::start_charge(station_clone2).await {
                                        Ok(r) => r,
                                        Err(e) => e.to_string(),
                                    }
                                }
                                Network::All => {
                                    let toast = Toast::builder()
                                        .timeout(5)
                                        .title("Network 'All' cannot start a charge session")
                                        .build();
                                    toast_overlay.add_toast(toast);
                                    return;
                                }
                                Network::Evie => {
                                    let toast = Toast::builder()
                                        .timeout(5)
                                        .title("Starting charging not supported on Evie")
                                        .build();
                                    toast_overlay.add_toast(toast);
                                    return;
                                }
                                Network::Bppulse => bp::start_charge(connector_clone2).await,
                                Network::Exploren => todo!(),
                            };
                            let toast = Toast::builder().timeout(5).title(response).build();
                            toast_overlay.add_toast(toast);
                        });
                    });
                    charge_button.set_sensitive(available);
                    connector_row.add_suffix(&charge_button);
                    row.add_row(&connector_row);
                }
            }
        });

        factory.connect_unbind(move |_, list_item| {
            let row = ExpanderRow::new();
            list_item
                .downcast_ref::<ListItem>()
                .expect("Needs to be a list item")
                .set_child(Some(&row));
        });

        self.imp().sidebar_lv.set_factory(Some(&factory));
    }

    fn notify_charge_status(&self, network: Network) {
        let obj = self.clone();
        glib::spawn_future_local(async move {
            let conn = gio::bus_get_sync(gio::BusType::Session, None::<&gio::Cancellable>).unwrap();
            let mut feedback_sent = false;
            loop {
                glib::timeout_future_seconds(60).await;
                let data = match network {
                    Network::Chargefox => chargefox::imp::charge_session().await.unwrap(),
                    _ => break,
                };

                let notification = gio::Notification::new("Vehicle Battery Charging");
                notification.set_body(Some(&format!("Current SOC {}%", data.soc / 100.0)));
                if !feedback_sent && data.soc > 80.0 {
                    feedback_sent = true;
                    let event = Event::new("message-new-instant");
                    if event.trigger_feedback().is_err() {
                        println!("Unable to trigger feedback")
                    }
                }
                obj.parent()
                    .unwrap()
                    .downcast_ref::<adw::ApplicationWindow>()
                    .expect("Dialog's parent must be the main window")
                    .application()
                    .unwrap()
                    .send_notification(Some("charge-status"), &notification);
                let dict = glib::VariantDict::new(None);
                dict.insert("progress-visible", true);
                dict.insert("count-visible", false);
                dict.insert("progress", data.soc / 100.0);
                dict.insert("count", 0);

                let variant =
                    ("application://com.gitlab.undef1.Electrolysis.desktop", dict).to_variant();
                let result = conn.emit_signal(
                    None,
                    "/com/gitlab/undef1/electrolysis",
                    "com.canonical.Unity.LauncherEntry",
                    "Update",
                    Some(&variant),
                );
                match result {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Error sending dbus signal in: {e:#?}");
                    }
                }
                if data.soc >= 99.0 {
                    let event = Event::new("message-new-instant");
                    if event.trigger_feedback().is_err() {
                        println!("Unable to trigger feedback")
                    }
                    break;
                }
            }
        });
    }
}
