use adw::prelude::*;
use adw::subclass::prelude::*;

use glib::subclass::InitializingObject;
use gtk::{glib, CompositeTemplate};

#[derive(CompositeTemplate, Default)]
#[template(file = "../../resources/payments.ui")]
pub struct PaymentsWindow {
    #[template_child]
    pub send: TemplateChild<gtk::Button>,
    #[template_child]
    pub number: TemplateChild<adw::EntryRow>,
    #[template_child]
    pub name: TemplateChild<adw::EntryRow>,
    #[template_child]
    pub cvc: TemplateChild<adw::EntryRow>,
    #[template_child]
    pub expiry_month: TemplateChild<adw::EntryRow>,
    #[template_child]
    pub expiry_year: TemplateChild<adw::EntryRow>,
    #[template_child]
    pub toast_overlay: TemplateChild<adw::ToastOverlay>,
}

#[glib::object_subclass]
impl ObjectSubclass for PaymentsWindow {
    const NAME: &'static str = "PaymentsWindow";
    type Type = super::PaymentsWindow;
    type ParentType = adw::PreferencesDialog;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for PaymentsWindow {
    fn constructed(&self) {
        self.parent_constructed();
        let obj = self.obj();
        let obj_clone = obj.clone();
        self.send
            .connect_clicked(move |_| obj_clone.sign_in_button_triggered());
    }
}

impl WidgetImpl for PaymentsWindow {}
impl PreferencesDialogImpl for PaymentsWindow {}
impl AdwDialogImpl for PaymentsWindow {}
