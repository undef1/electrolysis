mod imp;

use crate::chargefox::addPaymentCard::{self, add_logged_in_user_payment_mechanism};
use crate::chargefox::consts as cf;
use crate::chargefox::pin;
use crate::secret::secret;
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::Toast;
use adw::{gio, glib};
use glib::Object;
use graphql_client::reqwest::post_graphql_blocking as post_graphql;
use reqwest::blocking::Client;
use reqwest::header::{HeaderMap, HeaderValue};

glib::wrapper! {
    pub struct PaymentsWindow(ObjectSubclass<imp::PaymentsWindow>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}
impl Default for PaymentsWindow {
    fn default() -> Self {
        Self::new()
    }
}

impl PaymentsWindow {
    pub fn new() -> Self {
        Object::builder().build()
    }

    fn sign_in_button_triggered(&self) {
        let token = match secret::retreive_login_token("chargefox") {
            Ok(t) => t,
            Err(_) => "".to_owned(),
        };
        if token.is_empty() {
            self.imp().number.get().set_text("No user logged in");
            return;
        }
        let number = self.imp().number.get().text().to_string();
        let name = self.imp().name.get().text().to_string();
        let cvc = self.imp().cvc.get().text().to_string();
        let expiry_month = self.imp().expiry_month.get().text().to_string();
        let expiry_year = self.imp().expiry_year.get().text().to_string();
        let body = pin::Body {
            number,
            name,
            cvc,
            expiry_month,
            expiry_year,
            address_line1: "na".to_owned(),
            address_city: "na".to_owned(),
            address_country: "na".to_owned(),
            publishable_api_key: cf::PINAPIKEY.to_owned(),
        };
        println!("{:#?}", body);
        let client = Client::new();
        // API Docs: https://pinpayments.com/developers/api-reference/cards
        let resp = match client.post(cf::PINURL).json(&body).send() {
            Ok(r) => r,
            Err(e) => {
                self.imp().number.get().set_text("Error sending request");
                println!("{}", e);
                return;
            }
        };
        println!("{:#?}", resp);
        let body = match resp.json::<pin::Response>() {
            Ok(r) => r,
            Err(e) => {
                self.imp().number.get().set_text("Invalid response");
                println!("{}", e);
                return;
            }
        };
        println!("{:#?}", body);
        let vars = add_logged_in_user_payment_mechanism::Variables {
            input: add_logged_in_user_payment_mechanism::AddLoggedInUserPaymentMechanismInput {
                payment_platform: add_logged_in_user_payment_mechanism::PaymentPlatform::pin,
                card_token: Some(body.response.token),
                client_mutation_id: None,
            },
        };
        println!("{:#?}", vars);
        let mut headers = HeaderMap::new();
        headers.insert(
            "Authorization",
            HeaderValue::from_str(format!("Bearer {}", token.trim()).as_str()).unwrap(),
        );
        let client = Client::builder()
            .user_agent(cf::UA)
            .default_headers(headers)
            .build()
            .unwrap();
        let resp = post_graphql::<addPaymentCard::AddLoggedInUserPaymentMechanism, _>(
            &client,
            cf::URL,
            vars,
        );
        println!("{:#?}", resp);
        let data: add_logged_in_user_payment_mechanism::ResponseData = match resp {
            Ok(r) => r.data.expect("Missing card update data"),
            Err(e) => panic!("{:#?}", e),
        };
        println!("{:#?}", data);
        if data.add_logged_in_user_payment_mechanism.is_some() {
            let pm = data.add_logged_in_user_payment_mechanism.unwrap();
            if !pm.errors.is_empty() {
                let toast = Toast::builder()
                    .timeout(5)
                    .title("Error Updating Card")
                    .build();
                self.imp().toast_overlay.add_toast(toast);
            } else {
                let toast = Toast::builder()
                    .timeout(5)
                    .title("Successfully Added Card")
                    .build();
                self.imp().toast_overlay.add_toast(toast);
            }
        } else {
            let toast = Toast::builder()
                .timeout(5)
                .title("Card details sent but no valid response")
                .build();
            self.imp().toast_overlay.add_toast(toast);
        }
    }
}
