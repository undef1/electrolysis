use crate::charge_station::{Connector, Station, StationData};
use crate::chargefox::station::location_drawer::ConnectorStatus;
use crate::chargefox::station::location_drawer::LocationDrawerLocationChargeStationsConnectorsPlug;
use crate::charger::{Charger, ChargerData, ChargerType};
use crate::evie::consts;
use crate::gui::{runtime, Network};
use gtk::glib::clone;
use reqwest::Client;
use serde::Deserialize;
use std::error::Error;
use std::str::FromStr;
use std::sync::RwLock;

static CACHED_CHARGERS: std::sync::RwLock<std::option::Option<std::vec::Vec<EvieChargerResponse>>> =
    RwLock::new(None::<Vec<EvieChargerResponse>>);

pub async fn populate_chargers(
    lat_ne: f64,
    long_ne: f64,
    lat_sw: f64,
    long_sw: f64,
    min_speed: i64,
) -> Result<Vec<Charger>, Box<dyn Error>> {
    let lat_center = lat_ne + ((lat_ne - lat_sw) / 2.0);
    let long_center = long_ne + ((long_ne - long_sw) / 2.0);

    let client = Client::builder().build()?;
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            // While the API doesn't respond without these query parameters, they actually don't do
            // anything. It always responds with everything.
            let resp = client
                .get(consts::API_URL.to_owned())
                .query(&[
                    ("action", "store_search".to_string()),
                    ("lat", lat_center.to_string()),
                    ("lng", long_center.to_string()),
                    ("max_results", "50".to_string()),
                    ("search_radius", "100".to_string()),
                    ("autoload", "1".to_string()),
                ])
                .send()
                .await;
            // https://evie.com.au/wp-admin/admin-ajax.php?action=store_search&lat=-25.2744&lng=133.7751&max_results=50&search_radius=100&autoload=1
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let mut ev_chargers: Vec<EvieChargerResponse> = resp?.json().await?;
        let chargers = ev_chargers
            .iter_mut()
            .filter_map(|ev_charger| {
                let ev_chargers_str = ev_charger
                    .evie_chargers
                    .replace('\\', "")
                    .replace("&#8211;", "-")
                    .replace("&#8217;", "'");
                let evie_chargers: Vec<EvieCharger> =
                    serde_json::from_str(&ev_chargers_str).ok()?;
                ev_charger.decoded_chargers.clone_from(&evie_chargers);
                let mut chargertype = ChargerType::AC;
                let mut max_power = 0;
                let mut available = true;
                for c in evie_chargers {
                    for con in c.connectors {
                        if chargertype != ChargerType::DC
                        // && con.connector_type.is_some()
                        && (con.connector_type == "CHADEMO"
                            || con.connector_type == "CCS2")
                        {
                            chargertype = ChargerType::DC
                        }
                        if con.max_power > max_power {
                            max_power = con.max_power / 1000
                        }
                        if available && con.status != "AVAILABLE" {
                            available = false;
                        }
                    }
                }
                if max_power < min_speed {
                    return None;
                }
                let charger_data = ChargerData {
                    id: ev_charger.id.clone().replace("&#8211;", "-"),
                    lat: f64::from_str(&ev_charger.lat).ok()?,
                    long: f64::from_str(&ev_charger.lng).ok()?,
                    free: true,
                    location: ev_charger
                        .store
                        .clone()
                        .replace("&#8211;", "-")
                        .replace("&amp;", "&")
                        .replace("&#8217;", "'"),
                    maxpower: max_power,
                    chargertype,
                    network: Network::Evie,
                };
                Some(Charger::new(charger_data))
            })
            .collect();
        let mut cache = CACHED_CHARGERS.write().unwrap();
        *cache = Some(ev_chargers);
        return Ok(chargers);
    }
    Ok(vec![])
}

pub async fn populate_stations(charger: Charger) -> Result<Vec<Station>, Box<dyn Error>> {
    let mut stations = Vec::<Station>::new();
    let cached_chargers = CACHED_CHARGERS.read().unwrap();
    let ev_chargers = cached_chargers.as_ref().unwrap();
    for ev_charger in ev_chargers {
        // The Everty API uses one entry per-plug, everyone else uses one-per-site,
        // so we have to merge multiple entries based on location here.
        if ev_charger.id == charger.id() {
            for c in &ev_charger.decoded_chargers {
                let mut connectors = Vec::<Connector>::new();
                let mut max_power = 0;
                let mut price = "".to_string();
                let mut available = false;
                for con in &c.connectors {
                    if con.max_power > max_power {
                        max_power = con.max_power;
                        price.clone_from(&con.price);
                    }
                    // let connector_status = match con.status.as_str() {
                    if !available && con.status == "AVAILABLE" {
                        available = true
                    }
                    let connector = Connector {
                        active_charge_session: None,
                        available: Some(con.status == "AVAILABLE"),
                        name: Some(con.id.to_string()),
                        id: con.id.to_string(),
                        status: Some(ConnectorStatus::Other("Unsupported".to_string())),
                        tethered: Some(true),
                        plug: LocationDrawerLocationChargeStationsConnectorsPlug {
                            type_name: con.connector_type.clone(),
                            icon_url: "".to_owned(),
                        },
                    };
                    connectors.push(connector);
                }
                let data = StationData {
                    connectors: Some(connectors),
                    enabled: available,
                    id: c.id.clone(),
                    idle_fee: 0.to_string(),
                    name: c.id.clone(),
                    online: available,
                    planned: false,
                    power: (max_power / 1000).to_string() + "kW",
                    price_id: 0,
                    pricing_plan: price,
                    public_charger: true,
                    unavailable: available,
                    unavailable_message: "".to_string(),
                    network: Network::Evie,
                    site_guid: None,
                    site_id: None,
                };
                let station = Station::new(data);
                stations.push(station);
            }
        }
    }
    Ok(stations)
}

#[derive(Deserialize, Clone)]
struct EvieChargerResponse {
    id: String,
    store: String,
    lat: String,
    lng: String,
    evie_chargers: String,
    #[serde(skip_deserializing)]
    decoded_chargers: Vec<EvieCharger>,
}

#[derive(Deserialize, Clone)]
struct EvieCharger {
    #[serde(rename = "physicalReference")]
    id: String,
    connectors: Vec<EvieConnector>,
}

#[derive(Deserialize, Clone)]
struct EvieConnector {
    #[serde(rename = "connectorId")]
    id: i32,
    #[serde(rename = "connectorType")]
    connector_type: String,
    #[serde(rename = "maxElectricPowerW")]
    max_power: i64,
    status: String,
    price: String,
}
