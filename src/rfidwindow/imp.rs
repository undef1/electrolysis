use adw::prelude::*;
use adw::subclass::prelude::*;

use crate::gui::{Network, APP_ID};
use glib::subclass::InitializingObject;
use gtk::gio::Settings;
use gtk::{gio, glib, CompositeTemplate};
use std::cell::RefCell;

#[derive(CompositeTemplate, Default)]
#[template(file = "../../resources/rfid.ui")]
pub struct RfidWindow {
    #[template_child]
    pub submit: TemplateChild<gtk::Button>,
    #[template_child]
    pub id: TemplateChild<adw::EntryRow>,
    #[template_child]
    pub cards: TemplateChild<gtk::ListBox>,
    pub cardstore: RefCell<Option<gio::ListStore>>,
}

#[glib::object_subclass]
impl ObjectSubclass for RfidWindow {
    const NAME: &'static str = "RfidWindow";
    type Type = super::RfidWindow;
    type ParentType = adw::PreferencesDialog;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for RfidWindow {
    fn constructed(&self) {
        self.parent_constructed();
        let obj = self.obj().clone();
        self.submit.connect_clicked(move |_| {
            let settings = Settings::new(APP_ID);
            let network = Network::from_str(settings.string("network").as_ref());
            let obj_clone = obj.clone();
            glib::spawn_future_local(async move {
                match obj_clone.submit_button_triggered(network).await {
                    Ok(_) => {}
                    Err(e) => println!("Unable to submit RFID card: {e:?}"),
                };
            });
        });
    }
}

impl WidgetImpl for RfidWindow {}
impl PreferencesDialogImpl for RfidWindow {}
impl AdwDialogImpl for RfidWindow {}
