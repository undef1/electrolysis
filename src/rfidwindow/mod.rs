mod imp;

use crate::bp_pulse::imp as bp;
use crate::chargefox::imp as cf;
use crate::exploren::imp as exploren;
use crate::gui::Network;
use crate::rfidcard::RfidCard;
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::{gio, glib, ActionRow};
use glib::{clone, Object};
use gtk::{Align, Button, CustomFilter, FilterListModel, SingleSelection};
use std::error::Error;

glib::wrapper! {
    pub struct RfidWindow(ObjectSubclass<imp::RfidWindow>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}
impl Default for RfidWindow {
    fn default() -> Self {
        Self::new()
    }
}

impl RfidWindow {
    pub fn new() -> Self {
        let s: RfidWindow = Object::builder().build();
        s.setup_cards();
        s
    }

    pub async fn set_cards(&self, network: Network) -> Result<(), Box<dyn Error>> {
        let cards = match network {
            Network::Chargefox => cf::rfid_cards().await?,
            Network::Bppulse => bp::rfid_cards().await?,
            Network::Exploren => exploren::rfid_cards().await?,
            _ => {
                return Err(format!("Network {network} does not support RFID cards").into());
            }
        };
        for card in cards {
            self.cards().append(&card);
        }
        Ok(())
    }
    fn cards(&self) -> gio::ListStore {
        self.imp()
            .cardstore
            .borrow()
            .clone()
            .expect("could not get cards")
    }

    fn setup_cards(&self) {
        let model = gio::ListStore::new::<RfidCard>();

        self.imp().cardstore.replace(Some(model));
        let filter_model = FilterListModel::new(Some(self.cards()), self.filter());
        let selection_model = SingleSelection::new(Some(filter_model));
        self.imp().cards.bind_model(
            Some(&selection_model),
            clone!(
                #[weak(rename_to = window)]
                self,
                #[upgrade_or_panic]
                move |obj| {
                    let card = obj
                        .downcast_ref()
                        .expect("the object must be of type station");
                    let row = window.create_card_row(card);
                    row.upcast()
                }
            ),
        );
    }

    fn filter(&self) -> Option<CustomFilter> {
        None
    }

    fn create_card_row(&self, card: &RfidCard) -> ActionRow {
        let row = ActionRow::builder().build();
        let delete_button = Button::builder()
            .valign(Align::Center)
            .label("Cannot Delete")
            .build();
        card.bind_property("id-tag", &row, "title")
            .sync_create()
            .build();
        card.bind_property("id", &row, "subtitle")
            .sync_create()
            .build();
        delete_button.set_sensitive(false);
        row.add_suffix(&delete_button);
        row
    }

    async fn submit_button_triggered(&self, network: Network) -> Result<(), Box<dyn Error>> {
        let id = self.imp().id.get().text().to_string();
        match network {
            Network::Chargefox => cf::submit_rfid_card(id).await,
            Network::Bppulse => bp::submit_rfid_card(id).await,
            Network::Exploren => exploren::submit_rfid_card(id).await,
            _ => Err("Network not supported".into()),
        }
    }
}
