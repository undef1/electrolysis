pub mod imp;

pub mod consts {
    pub const URL: &str = "https://api.evconnect.com";
    pub const TOKEN_HEADER: &str = "Evc-Api-Token";
    pub const VERSION_HEADER: &str = "Mobile-App-Version";
    pub const VERSION_VALUE: &str = "4.0.0";
    pub const USER_AGENT: &str = "okhttp/4.9.2";
    pub const NETWORK_ID: &str = "bp-au";
}
