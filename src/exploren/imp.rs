use crate::charge_session_window::ChargeSession;
use crate::charge_station::{Station, StationData};
use crate::chargefox::station::location_drawer::LocationDrawerLocationChargeStationsConnectorsActiveChargeSession;
use crate::chargefox::station::location_drawer::{
    LocationDrawerLocationChargeStationsConnectors,
    LocationDrawerLocationChargeStationsConnectorsPlug,
};
use crate::exploren::consts;
use crate::rfidcard::{RfidCard, RfidCardData};
use crate::secret::secret;
use crate::user::User;

use crate::charger::{Charger, ChargerData, ChargerType};
use crate::gui::{runtime, Network};
use gtk::glib::clone;
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;

#[derive(Serialize)]
struct Login {
    pub username: String,
    pub password: String,
    pub grant_type: String,
    pub client_id: String,
    pub client_secret: String,
}

#[derive(Deserialize)]
struct LoginResponse {
    pub access_token: String,
    pub refresh_token: String,
}

pub async fn login(user: String, password: String) -> Result<String, Box<dyn Error>> {
    let vars = Login {
        username: user,
        password,
        grant_type: String::from(consts::GRANT_TYPE),
        client_id: String::from(consts::CLIENT_ID),
        client_secret: String::from(consts::CLIENT_SECRET),
    };
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::INTERNAL_VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::APP_ID_HEADER,
        HeaderValue::from_str(consts::APP_ID).unwrap(),
    );
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .post(consts::URL.to_owned() + "/api/v1/app/oauth/token")
                .json(&vars)
                .send()
                .await
                .unwrap();
            let body: LoginResponse = resp.json().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        if !resp.refresh_token.is_empty() {
            let _ = secret::store_login(
                &(Network::Exploren.to_str().to_owned() + "-refresh"),
                resp.refresh_token.as_ref(),
            );
        }
        return Ok(resp.access_token);
    }
    Err("Failed to make request".into())
}

#[derive(Deserialize)]
struct PinsResponse {
    pins: Vec<ExplorenPin>,
}

#[derive(Deserialize)]
struct ExplorenPin {
    id: i64,
    // geo: String,
    // av: String,
}

#[derive(Serialize)]
struct ExplorenLocationsBody {
    locations: HashMap<String, Option<String>>,
}

#[derive(Deserialize)]
struct ExplorenLocationsResponse {
    locations: Vec<ExplorenLocation>,
    tariffs: Vec<ExplorenTariff>,
}
#[derive(Deserialize)]
struct ExplorenLocation {
    id: i64,
    name: Option<String>,
    location: String,
    zones: Vec<ExplorenZone>,
}

#[derive(Deserialize)]
struct ExplorenZone {
    evses: Vec<ExplorenEvse>,
}
#[derive(Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
struct ExplorenEvse {
    identifier: String,
    max_power: Option<i64>,
    is_available: bool,
    current_type: Option<String>,
    connectors: Vec<ExplorenConnector>,
    soc_percent: Option<i64>,
    tariff_id: Option<String>,
}

#[derive(Deserialize)]
struct ExplorenConnector {
    name: String,
    format: String,
}

#[derive(Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
struct ExplorenTariff {
    id: String,
    description: Option<String>,
    price_for_energy: Option<f64>,
    price_for_duration: Option<f64>,
    pricing_period_in_minutes: Option<i64>,
}

pub async fn populate_chargers(
    lat_ne: f64,
    long_ne: f64,
    lat_sw: f64,
    long_sw: f64,
    min_speed: i64,
) -> Result<Vec<Charger>, Box<dyn Error>> {
    let mut chargers = Vec::<Charger>::new();
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::INTERNAL_VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let pins: PinsResponse = client
                .get(consts::URL.to_owned() + "/api/v2/app/pins")
                .query(&[
                    ("minLatitude", lat_sw.to_string()),
                    ("minLongitude", long_sw.to_string()),
                    ("maxLatitude", lat_ne.to_string()),
                    ("maxLongitude", long_ne.to_string()),
                    ("includeAvailability", "true".to_string()),
                    ("limit", "1000".to_string()),
                ])
                .send()
                .await
                .unwrap()
                .json()
                .await
                .unwrap();
            let locations: HashMap<String, Option<String>> =
                pins.pins.iter().map(|p| (p.id.to_string(), None)).collect();
            let body = ExplorenLocationsBody { locations };
            let resp = client
                .post(consts::URL.to_owned() + "/api/v2/app/locations")
                .json(&body)
                .send()
                .await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let mut ev_chargers: ExplorenLocationsResponse = resp?.json().await?;
        for ev_charger in &mut ev_chargers.locations {
            let mut chargertype = ChargerType::AC;
            let mut power = 0;
            let mut free = true;
            for zone in &ev_charger.zones {
                for evse in &zone.evses {
                    if chargertype != ChargerType::DC
                        && evse.current_type.is_some()
                        && evse.current_type.clone().unwrap() != "ac"
                    {
                        chargertype = ChargerType::DC
                    }
                    if evse.max_power.is_some() && evse.max_power.unwrap() / 1000 > power {
                        power = evse.max_power.unwrap() / 1000;
                    }
                    free = free || evse.is_available
                }
            }
            if power < min_speed {
                continue;
            }
            let splits: Vec<&str> = ev_charger.location.split(",").collect();
            let lat = splits.first().unwrap().parse::<f64>()?;
            let long = splits.get(1).unwrap().parse::<f64>()?;

            let charger_data = ChargerData {
                id: ev_charger.id.to_string(),
                lat,
                long,
                free,
                location: ev_charger.name.clone().unwrap_or("".to_string()),
                maxpower: power,
                chargertype,
                network: Network::Exploren,
            };
            let charger = Charger::new(charger_data);
            chargers.push(charger);
        }
    }
    Ok(chargers)
}
pub async fn populate_stations(charger: Charger) -> Result<Vec<Station>, Box<dyn Error>> {
    let mut stations = Vec::<Station>::new();
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::INTERNAL_VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    let charger_id = charger.id();
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let locations: HashMap<String, Option<String>> = HashMap::from([(charger_id, None)]);
            let body = ExplorenLocationsBody { locations };
            let resp = client
                .post(consts::URL.to_owned() + "/api/v2/app/locations")
                .json(&body)
                .send()
                .await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let mut ev_chargers: ExplorenLocationsResponse = resp?.json().await?;
        for ev_charger in &mut ev_chargers.locations {
            let mut power = 0;
            let mut pricing_plan = "".to_string();
            let connectors: Vec<LocationDrawerLocationChargeStationsConnectors> = ev_charger
                .zones
                .first()
                .unwrap()
                .evses
                .iter()
                .map(|c| {
                    let tethered = c.connectors.first().unwrap().format != "socket";
                    let plug = &c.connectors.first().unwrap().name;
                    if c.max_power.is_some() && c.max_power.unwrap() / 1000 > power {
                        power = c.max_power.unwrap() / 1000;
                    }
                    if pricing_plan.is_empty() && c.tariff_id.is_some() {
                        for tariff in &ev_chargers.tariffs {
                            if tariff.id == c.tariff_id.clone().unwrap() {
                                if tariff.price_for_energy.is_some() {
                                    let en = format!("${}/kWh", tariff.price_for_energy.unwrap());
                                    let mut time = "".to_string();
                                    if tariff.price_for_duration.is_some()
                                        && tariff.price_for_duration.unwrap() != 0.0
                                        && tariff.pricing_period_in_minutes.is_some()
                                    {
                                        time = format!(
                                            " plus ${}/{}min",
                                            tariff.price_for_duration.unwrap(),
                                            tariff.pricing_period_in_minutes.unwrap()
                                        );
                                    }
                                    pricing_plan = en + &time;
                                    break;
                                } else if tariff.description.is_some() {
                                    pricing_plan = tariff
                                        .description
                                        .clone()
                                        .unwrap()
                                        .replace("<br>", "\n")
                                        .replace("<div>", "")
                                        .replace("</div>", "");
                                    break;
                                }
                            }
                        }
                    }
                    let session = c.soc_percent.map(|p| {
                        LocationDrawerLocationChargeStationsConnectorsActiveChargeSession {
                            start_time: "".to_owned(),
                            state_of_charge: Some(p.to_string() + "%"),
                        }
                    });
                    LocationDrawerLocationChargeStationsConnectors {
                        active_charge_session: session,
                        available: Some(c.is_available),
                        name: Some(c.identifier.clone()),
                        id: c.identifier.clone(),
                        status: None,
                        tethered: Some(tethered),
                        plug: LocationDrawerLocationChargeStationsConnectorsPlug {
                            type_name: plug.to_string(),
                            icon_url: "".to_string(),
                        },
                    }
                })
                .collect();
            let data = StationData {
                connectors: Some(connectors),
                enabled: true,
                idle_fee: "".to_owned(),
                id: ev_charger.id.to_string(),
                name: ev_charger.name.clone().unwrap_or("".to_string()),
                online: true,
                planned: false,
                power: format!("{}kW", power),
                price_id: 0,
                pricing_plan,
                public_charger: true,
                unavailable: false,
                unavailable_message: "".to_owned(),
                network: Network::Exploren,
                site_id: None,
                site_guid: None,
            };
            let station = Station::new(data);
            stations.push(station);
        }
    }
    Ok(stations)
}

#[derive(Serialize)]
struct ExpRfidCard {
    token: String,
    name: String,
}

pub async fn submit_rfid_card(id: String) -> Result<(), Box<dyn Error>> {
    let vars = ExpRfidCard {
        token: id.clone(),
        name: id,
    };
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::INTERNAL_VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::APP_ID_HEADER,
        HeaderValue::from_str(consts::APP_ID).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .http1_title_case_headers()
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .post(consts::URL.to_owned() + "/api/v1/app/profile/rfids")
                .json(&vars)
                .send()
                .await
                .unwrap();
            let body = resp.text().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        println!("{:#?}", resp);
        return Ok(());
    }
    Err("Failed to make request".into())
}

#[derive(Deserialize)]
struct ExpRfidCardResp {
    data: Vec<ExpRfidCardRespData>,
}
#[derive(Deserialize)]
struct ExpRfidCardRespData {
    id: i64,
    name: String,
    status: String,
}

pub async fn rfid_cards() -> Result<Vec<RfidCard>, Box<dyn Error>> {
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::INTERNAL_VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::APP_ID_HEADER,
        HeaderValue::from_str(consts::APP_ID).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .http1_title_case_headers()
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .get(consts::URL.to_owned() + "/api/v1/app/profile/rfids?type=rfid")
                .send()
                .await
                .unwrap();
            let body: ExpRfidCardResp = resp.json().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let cards = resp
            .data
            .into_iter()
            .filter_map(|card| {
                if card.status != "active" {
                    return None;
                }
                let data = RfidCardData {
                    id: card.id.to_string(),
                    id_tag: card.name,
                };
                Some(RfidCard::new(data))
            })
            .collect();
        return Ok(cards);
    }
    Ok(vec![])
}

/// check_or_refresh_token automatically refreshes the currently logged in user's token.
pub async fn check_or_refresh_token() -> Result<String, Box<dyn Error>> {
    let token = secret::retreive_login_token(Network::Exploren.to_str())?;
    Ok(String::from("Bearer ") + &token)
}

#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "camelCase"))]
struct ExpStatus {
    profile: ExpProfile,
}
#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "camelCase"))]
struct ExpProfile {
    email: Option<String>,
    first_name: Option<String>,
    last_name: Option<String>,
    #[serde(rename = "email_verified_at")]
    email_verified_at: Option<String>,
    country: Option<String>,
}

pub async fn user_details() -> Result<User, Box<dyn Error>> {
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::INTERNAL_VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::APP_ID_HEADER,
        HeaderValue::from_str(consts::APP_ID).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .http1_title_case_headers()
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .get(consts::URL.to_owned() + "/api/v1/app/status")
                .send()
                .await
                .unwrap();
            let body: ExpStatus = resp.json().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        return Ok(User {
            email: resp.profile.email,
            first_name: resp.profile.first_name,
            last_name: resp.profile.last_name,
            confirmed: Some(resp.profile.email_verified_at.is_some()),
            home_country_code: resp.profile.country,
            has_disabled_push_notifications: None,
        });
    };
    Err("Unable to receive user data".into())
}

pub async fn charge_session() -> Result<ChargeSession, Box<dyn Error>> {
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::INTERNAL_VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(
        consts::APP_ID_HEADER,
        HeaderValue::from_str(consts::APP_ID).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .http1_title_case_headers()
        .default_headers(headers)
        .connection_verbose(true)
        .build()?;
    let (station_sender, station_receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .get(consts::URL.to_owned() + "/api/v1/app/session/active")
                .send()
                .await
                .unwrap()
                .text()
                .await
                .unwrap();
            station_sender.send(resp).await.unwrap();
        }
    ));
    let body = station_receiver.recv().await?;
    if !body.is_empty() {
        println!("Exploren Charge Response: {body}");
        return Err("Cannot currently interpret exploren charge session response".into());
    }
    Err("No charge session found".into())
}
