#[allow(clippy::module_inception)]
pub mod secret {
    use oo7::dbus::Error as oError;
    use oo7::Keyring;
    use std::collections::HashMap;
    use std::error::Error;

    use tokio::runtime::Runtime;

    pub fn store_login(service: &str, token: &str) -> Result<(), Box<dyn Error>> {
        let fut = async {
            let keyring = Keyring::new().await.unwrap();
            keyring.unlock().await?;

            keyring
                .create_item(
                    "electrolysis login",
                    HashMap::from([("service", service), ("application", "electrolysis")]),
                    token,
                    true,
                )
                .await
        };
        Runtime::new().unwrap().block_on(fut)?;
        Ok(())
    }

    pub fn retreive_login_token(service: &str) -> Result<String, Box<dyn Error>> {
        let fut = async {
            let keyring = Keyring::new().await?;
            keyring.unlock().await?;
            let items = match keyring
                .search_items(HashMap::from([
                    ("service", service),
                    ("application", "electrolysis"),
                ]))
                .await
            {
                Ok(i) => i,
                Err(e) => return Err(e),
            };
            if items.is_empty() {
                return Err(oo7::Error::DBus(oError::NotFound(
                    "no such secret".to_owned(),
                )));
            }
            let zerosecret = items[0].secret().await?;
            let secret = String::from_utf8(zerosecret.to_vec()).unwrap();
            Ok(secret)
        };
        Ok(Runtime::new().unwrap().block_on(fut)?)
    }
}
