use crate::bp_pulse::consts;
use crate::charge_session_window::ChargeSession;
use crate::charge_station::{Station, StationData};
use crate::chargefox::station::location_drawer::{
    LocationDrawerLocationChargeStationsConnectors,
    LocationDrawerLocationChargeStationsConnectorsPlug,
};
use crate::charger::{Charger, ChargerData, ChargerType};
use crate::gui::{runtime, Network};
use crate::rfidcard::{RfidCard, RfidCardData};
use crate::secret::secret;
use crate::user::User;
use data_encoding::BASE64_NOPAD;
use gtk::glib::clone;
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::error::Error;

#[derive(Serialize)]
struct Login {
    pub password: String,
    pub email: String,
    #[serde(rename = "networkId")]
    pub network_id: String,
}

#[derive(Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
struct LoginResponse {
    pub access_token: String,
    pub refresh_token: String,
}

pub async fn login(user: String, password: String) -> Result<String, Box<dyn Error>> {
    let vars = Login {
        email: user,
        password,
        network_id: consts::NETWORK_ID.to_owned(),
    };
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .post(consts::URL.to_owned() + "/mobile/rest/v6/auth")
                .json(&vars)
                .send()
                .await
                .unwrap();
            let body: LoginResponse = resp.json().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        if !resp.refresh_token.is_empty() {
            let _ = secret::store_login(
                &(Network::Bppulse.to_str().to_owned() + "-refresh"),
                resp.refresh_token.as_ref(),
            );
        }
        return Ok(resp.access_token);
    }
    Err("Failed to make request".into())
}

#[derive(Deserialize)]
#[allow(unused)]
struct LocationsResponse {
    pub c: i64,
    pub a: i64,
    pub s: i64,
    pub w: i64,
    pub e: String,
    pub p: Point,
    pub tz: String,
    pub rc: bool,
}

#[derive(Deserialize)]
struct Point {
    pub x: f64,
    pub y: f64,
}

pub async fn populate_chargers(
    lat_ne: f64,
    long_ne: f64,
    lat_sw: f64,
    long_sw: f64,
    distance: f64,
) -> Result<Vec<Charger>, Box<dyn Error>> {
    let distance = distance.round() as i64 + 60;
    let lat_center = lat_ne + ((lat_ne - lat_sw) / 2.0);
    let long_center = long_ne + ((long_ne - long_sw) / 2.0);

    let mut chargers = Vec::<Charger>::new();
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .get(consts::URL.to_owned() + "/mobile/rest/v6/networks/bp-au/locations-geo-search")
                .query(&[
                    ("latitude", lat_center.to_string()),
                    ("longitude", long_center.to_string()),
                    ("distance", distance.to_string()),
                    ("metric", "MILES".to_string()),
                    ("availableOnly", "false".to_string()),
                    ("includeRoaming", "false".to_string()),
                    ("voucherDiscountOnly", "false".to_string()),
                ])
                .send()
                .await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let mut ev_chargers: Vec<LocationsResponse> = resp?.json().await?;
        for ev_charger in &mut ev_chargers {
            let chargertype = ChargerType::DC;
            let available = ev_charger.a > 0;
            let charger_data = ChargerData {
                id: ev_charger.e.clone(),
                lat: ev_charger.p.y,
                long: ev_charger.p.x,
                free: available,
                location: "BP Pulse".to_owned(),
                maxpower: 75,
                chargertype,
                network: Network::Bppulse,
            };
            let charger = Charger::new(charger_data);
            chargers.push(charger);
        }
    }
    Ok(chargers)
}

#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "camelCase"))]
#[allow(unused)]
struct ChargerResponse {
    location_id: String,
    organization_name: String,
    location_name: String,
    address: String,
    connectors_count: i64,
    available_connectors_count: i64,
    ports: Vec<Port>,
    restrict_charging: bool,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "camelCase"))]
struct Port {
    // port_id: String,
    qr_code: String,
    // station_id: String,
    // evse_id: String,
    // power_level: String,
    // status: String,
    // available_connectors_count: i64,
    // occupied_connectors_count: i64,
    // unavailable_connectors_count: i64,
    // service_status: String,
    // trailer_accessibility: bool,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all(deserialize = "camelCase"))]
struct StationPortsResponse {
    // access_type: String,
    qr_code: String,
    organization_id: String,
    location_id: String,
    port_id: String,
    // power_level: String,
    // port_status: String,
    location_name: String,
    connectors: Vec<StationPortsConnectors>,
    connector_access_policies: Vec<StationPortsConnectorAccessPolicies>,
    // trailer_accessibility: bool,
    restrict_charging: bool,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all(deserialize = "camelCase"))]
struct StationPortsConnectors {
    external_id: String,
    // connector_id: i64,
    connector_name: String,
    connector_type: String,
    connector_format: String,
    // power_type: String,
    output_kilo_watts: f64,
    connector_status: String,
    // service_status: String,
    tariff_id: String, // Matches External ID in connectorAccessPolicies.chargingPolicies.tariff
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all(deserialize = "camelCase"))]
struct StationPortsConnectorAccessPolicies {
    // authorization_status: String,
    charging_policies: Vec<StationPortsChargingPolicies>,
}
#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all(deserialize = "camelCase"))]
struct StationPortsChargingPolicies {
    tariff: StationPortsTariff,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all(deserialize = "camelCase"))]
struct StationPortsTariff {
    external_id: String,
    description: String,
}

pub async fn populate_stations(charger: Charger) -> Result<Vec<Station>, Box<dyn Error>> {
    let mut stations = Vec::<Station>::new();
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let charger_id = charger.id();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .get(
                    consts::URL.to_owned()
                        + "/mobile/rest/v6/networks/bp-au/locations/"
                        + &charger_id,
                )
                .send()
                .await
                .unwrap();
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let data: ChargerResponse = resp.json().await?;
        for station in data.ports {
            let (station_sender, station_receiver) = async_channel::bounded(1);
            runtime().spawn(clone!(
                #[strong]
                client,
                async move {
                    let resp = client
                        .get(consts::URL.to_owned() + "/mobile/rest/v6/users/current/station-ports")
                        .query(&[("qrCode", station.qr_code.clone())])
                        .send()
                        .await
                        .unwrap();
                    station_sender.send(resp).await.unwrap();
                }
            ));
            if let Ok(spr_resp) = station_receiver.recv().await {
                let spr: StationPortsResponse = spr_resp.json().await?;
                let connectors: Vec<LocationDrawerLocationChargeStationsConnectors> = spr
                    .connectors
                    .clone()
                    .into_iter()
                    .map(|c| LocationDrawerLocationChargeStationsConnectors {
                        active_charge_session: None,
                        available: Some(c.connector_status == "AVAILABLE"),
                        name: Some(c.connector_name),
                        id: c.external_id,
                        status: None,
                        tethered: Some(c.connector_format == "CABLE"),
                        plug: LocationDrawerLocationChargeStationsConnectorsPlug {
                            type_name: c.connector_type,
                            icon_url: spr.qr_code.clone(),
                        },
                    })
                    .collect();
                let power = spr
                    .connectors
                    .clone()
                    .into_iter()
                    .fold(f64::MIN, |a, b| a.max(b.output_kilo_watts));
                let tariff_id = spr
                    .connectors
                    .into_iter()
                    .find(|c| c.output_kilo_watts == power)
                    .unwrap()
                    .tariff_id;
                let mut tariff = "".to_owned();
                for cap in spr.connector_access_policies {
                    for policy in cap.charging_policies {
                        if policy.tariff.external_id == tariff_id {
                            tariff = policy.tariff.description;
                            break;
                        }
                    }
                }
                let data = StationData {
                    connectors: Some(connectors),
                    enabled: spr.restrict_charging,
                    id: spr.qr_code,
                    idle_fee: "".to_owned(),
                    name: spr.location_name.clone(),
                    online: true,
                    planned: false,
                    power: format!("{}kW", power),
                    price_id: 0,
                    pricing_plan: tariff,
                    public_charger: true,
                    unavailable: data.connectors_count == data.available_connectors_count,
                    unavailable_message: "".to_string(),
                    network: Network::Bppulse,
                    site_id: None,
                    site_guid: None,
                };
                let station = Station::new(data);
                stations.push(station);
            }
        }
    }
    Ok(stations)
}

#[derive(Serialize)]
struct BPRfidCard {
    code: String,
}

pub async fn submit_rfid_card(id: String) -> Result<(), Box<dyn Error>> {
    let vars = BPRfidCard { code: id };
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .post(consts::URL.to_owned() + "/mobile/rest/v6/users/current/key-fobs")
                .json(&vars)
                .send()
                .await
                .unwrap();
            let body = resp.text().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        println!("{:#?}", resp);
        return Ok(());
    }
    Err("Failed to make request".into())
}

#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "camelCase"))]
struct KeyFob {
    id: String,
    code: String,
}

pub async fn rfid_cards() -> Result<Vec<RfidCard>, Box<dyn Error>> {
    let mut cards = vec![];
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .get(consts::URL.to_owned() + "/mobile/rest/v6/users/current/key-fobs")
                .send()
                .await
                .unwrap();
            let body: Vec<KeyFob> = resp.json().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        for card in resp {
            let data = RfidCardData {
                id: card.id,
                id_tag: card.code,
            };
            let card = RfidCard::new(data);
            cards.push(card)
        }
    }
    Ok(cards)
}

#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "camelCase"))]
struct BPUser {
    email: String,
    first_name: String,
    last_name: String,
    verified: bool,
}

pub async fn user_details() -> Result<User, Box<dyn Error>> {
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .get(consts::URL.to_owned() + "/mobile/rest/v6/users/current")
                .send()
                .await
                .unwrap();
            let body: BPUser = resp.json().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        return Ok(User {
            email: Some(resp.email),
            first_name: Some(resp.first_name),
            last_name: Some(resp.last_name),
            confirmed: Some(resp.verified),
            home_country_code: None,
            has_disabled_push_notifications: None,
        });
    };
    Err("Unable to receive user data".into())
}

#[derive(Deserialize, Debug, Clone)]
struct Claims {
    exp: u64,
}

#[derive(Serialize, Debug, Clone)]
struct RefreshBody {
    token: String,
}

/// check_or_refresh_token automatically refreshes the currently logged in user's token.
pub async fn check_or_refresh_token() -> Result<String, Box<dyn Error>> {
    let token = secret::retreive_login_token(Network::Bppulse.to_str())?;
    let current_exp = std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .expect("time is broken")
        .as_secs();
    let body = match token.split('.').nth(1) {
        Some(b) => b.to_owned(),
        None => return Err("split: Token was not a valid JWT, please re-login".into()),
    };
    let body_dec = match BASE64_NOPAD.decode(&body.into_bytes()) {
        Ok(d) => d,
        Err(_) => return Err("base64: Token was not a valid JWT, please re-login".into()),
    };
    let claims: Claims = match serde_json::from_slice(body_dec.as_ref()) {
        Ok(c) => c,
        Err(_) => return Err("decode: Token was not a valid JWT, please re-login".into()),
    };
    if claims.exp + 10 > current_exp {
        return Ok(token);
    }
    let token = secret::retreive_login_token(&(Network::Bppulse.to_str().to_owned() + "-refresh"))?;
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let body = RefreshBody { token };
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = client
                .put(consts::URL.to_owned() + "/mobile/rest/v6/auth")
                .json(&body)
                .send()
                .await
                .unwrap();
            let tresp = resp.json::<LoginResponse>().await.unwrap();
            sender.send(tresp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        if !resp.refresh_token.is_empty() {
            let _ = secret::store_login(
                &(Network::Bppulse.to_str().to_owned() + "-refresh"),
                resp.refresh_token.as_ref(),
            );
        }
        return Ok(resp.access_token);
    };
    Err("No token refresh response recived".into())
}

#[derive(Serialize, Debug)]
#[serde(rename_all(serialize = "camelCase"))]
struct StartChargeBody {
    network_id: String,
    organization_id: String,
    station_port_id: String,
    connector_external_id: String,
    operator_id: Option<String>,
    location_id: String,
}

pub async fn start_charge(connector: LocationDrawerLocationChargeStationsConnectors) -> String {
    let token = match check_or_refresh_token().await {
        Ok(t) => t,
        Err(e) => return e.to_string(),
    };
    if token.is_empty() {
        return "No token found".to_string();
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE).unwrap(),
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token).unwrap());
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (station_sender, station_receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp: StationPortsResponse = client
                .get(consts::URL.to_owned() + "/mobile/rest/v6/users/current/station-ports")
                .query(&[("qrCode", connector.plug.icon_url)])
                .send()
                .await
                .unwrap()
                .json()
                .await
                .unwrap();
            station_sender.send(resp).await.unwrap();
        }
    ));
    let body = match station_receiver.recv().await {
        Ok(s) => StartChargeBody {
            network_id: "bp-au".to_string(),
            organization_id: s.organization_id,
            station_port_id: s.port_id,
            connector_external_id: connector.id,
            operator_id: None,
            location_id: s.location_id,
        },
        Err(e) => return e.to_string(),
    };
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let _ = client
                .get(consts::URL.to_owned() + "/mobile/rest/v6/networks/bp-au/payments/token")
                .send()
                .await
                .unwrap();
            let resp = client
                .patch(
                    consts::URL.to_owned() + "/mobile/rest/v6/users/current/commands/start-charge",
                )
                .json(&body)
                .send()
                .await
                .unwrap();
            let body = resp.text().await.unwrap();
            sender.send(body).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        return resp;
    };
    "Unable to receive response".to_string()
}

#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "camelCase"))]
struct BPChargeSessionsResponse {
    session_external_id: String,
    cost: f64,
    soc: i64,
    charge_session_status: String,
    connected_duration: i64,
    power: f64,
}

pub async fn charge_session() -> Result<ChargeSession, Box<dyn Error>> {
    let token = check_or_refresh_token().await?;
    if token.is_empty() {
        return Err("No token found".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        consts::VERSION_HEADER,
        HeaderValue::from_str(consts::VERSION_VALUE)?,
    );
    headers.insert(consts::TOKEN_HEADER, HeaderValue::from_str(&token)?);
    let client = Client::builder()
        .user_agent(consts::USER_AGENT)
        .default_headers(headers)
        .connection_verbose(true)
        .build()?;
    let (station_sender, station_receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp: Vec<BPChargeSessionsResponse> = client
                .get(consts::URL.to_owned() + "/mobile/rest/v6/users/current/active-sessions")
                .send()
                .await
                .unwrap()
                .json()
                .await
                .unwrap();
            station_sender.send(resp).await.unwrap();
        }
    ));
    let body = station_receiver.recv().await?;
    if !body.is_empty() {
        let s = &body[0];
        return Ok(ChargeSession {
            id: s.session_external_id.clone(),
            charge: format!("${}", s.cost),
            soc: s.soc as f64,
            duration: format!("{}m", s.connected_duration as f64 / 1000.0 / 60.0),
            idle_grace_end: "".to_owned(),
            stopped: s.charge_session_status == "FINISHED",
            reduced: false,
            paused: false,
            total: format!("{}kWh", s.power),
        });
    }
    Err("No charge session found".into())
}
