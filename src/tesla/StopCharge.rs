#![allow(clippy::all, warnings)]
pub struct StopCharging;
pub mod stop_charging {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "StopChargingSessionById";
    pub const QUERY: &str = "mutation StopChargingSessionById($authorizationSessionId: String!) {
  charging {
    stopSession(authorizationSessionId: $authorizationSessionId) {
      isRequestDelivered
    }
  }
}";
    use super::*;
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        #[serde(rename = "authorizationSessionId")]
        pub authorization_session_id: String,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub charging: Charging,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Charging {
        #[serde(rename = "stopSession")]
        pub stop_session: StopSession,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct StopSession {
        #[serde(rename = "isRequestDelivered")]
        pub is_request_delivered: Option<bool>,
    }
}
impl graphql_client::GraphQLQuery for StopCharging {
    type Variables = stop_charging::Variables;
    type ResponseData = stop_charging::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: stop_charging::QUERY,
            operation_name: stop_charging::OPERATION_NAME,
        }
    }
}
