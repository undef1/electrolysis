#![allow(clippy::all, warnings)]
pub struct PreStartChargingValidation;
pub mod pre_start_charging_validation {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "preStartChargingValidation";
    pub const QUERY : & str = "query preStartChargingValidation($trtId: Int!, $programType: EPSChargingProgramType!, $deviceLanguage: String!, $deviceCountry: String!) {
  me {
    charging {
      canStartCharging(
        trtId: $trtId
        programType: $programType
        deviceCountry: $deviceCountry
        deviceLanguage: $deviceLanguage
      ) {
        enabled
        errors
      }
    }
  }
}
";
    use super::*;
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        #[serde(rename = "trtId")]
        pub trt_id: i64,
        #[serde(rename = "programType")]
        pub program_type: String,
        #[serde(rename = "deviceLanguage")]
        pub device_language: String,
        #[serde(rename = "deviceCountry")]
        pub device_country: String,
        #[serde(rename = "ttpLocale")]
        pub ttp_locale: String,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub me: Me,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Me {
        pub charging: Charging,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Charging {
        #[serde(rename = "canStartCharging")]
        pub can_start_charging: CanStartCharging,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct CanStartCharging {
        pub enabled: bool,
        pub errors: Option<String>,
    }
}
impl graphql_client::GraphQLQuery for PreStartChargingValidation {
    type Variables = pre_start_charging_validation::Variables;
    type ResponseData = pre_start_charging_validation::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: pre_start_charging_validation::QUERY,
            operation_name: pre_start_charging_validation::OPERATION_NAME,
        }
    }
}
