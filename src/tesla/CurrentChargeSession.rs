#![allow(clippy::all, warnings)]
pub struct CurrentChargeSession;
pub mod current_charge_session {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "GetActiveChargingSessions";
    pub const QUERY: &str = "query GetActiveChargingSessions($vin: String) {
  charging {
    activeSessions(vin: $vin) {
      authorizationSessionId
      siteId
      postId
      sessionEvents
      createdTimeEpoch {
        ...GoogleProtoTimeStampFragment
      }
      lastUpdatedTimeEpoch {
        ...GoogleProtoTimeStampFragment
      }
      chargeEndTimeEpoch {
        ...GoogleProtoTimeStampFragment
      }
      isDcEnforced
      trtId {
        value
      }
      locationGUID
      programType
    }
  }
}
    
    fragment GoogleProtoTimeStampFragment on GoogleProtoTimeStamp {
  nanos
  seconds
} ";
    use super::*;
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub charging: Charging,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Charging {
        #[serde(rename = "activeSessions")]
        pub active_sessions: Option<ActiveSessions>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ActiveSessions {
        #[serde(rename = "authorizationSessionId")]
        pub authorization_session_id: Option<String>,
        #[serde(rename = "siteId")]
        pub site_id: Option<String>,
        #[serde(rename = "postId")]
        pub post_id: Option<String>,
        #[serde(rename = "sessionEvents")]
        pub session_events: Option<Vec<String>>,
        #[serde(rename = "chargeEndTimeEpoch")]
        pub charge_end_time: Option<TimeStamp>,
        #[serde(rename = "isDcEnforced")]
        pub is_dc_enforced: Option<bool>,
        #[serde(rename = "locationGUID")]
        pub location_guid: Option<String>,
        #[serde(rename = "programType")]
        pub programType: Option<String>,
        #[serde(rename = "createdTimeEpoch")]
        pub created_time: Option<TimeStamp>,
        #[serde(rename = "lastUpdatedTimeEpoch")]
        pub last_updated_time: Option<TimeStamp>,
        #[serde(rename = "trtId")]
        pub trt_id: Option<TrtID>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct TimeStamp {
        pub nanos: i64,
        pub seconds: i64,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct TrtID {
        pub Value: Option<i64>,
    }
}
impl graphql_client::GraphQLQuery for CurrentChargeSession {
    type Variables = current_charge_session::Variables;
    type ResponseData = current_charge_session::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: current_charge_session::QUERY,
            operation_name: current_charge_session::OPERATION_NAME,
        }
    }
}
