#![allow(clippy::all, warnings)]
pub struct ChargeSiteInfo;
pub mod charge_site_info {
    #![allow(dead_code)]
    use crate::tesla::nearbySites::nearby_sites;
    use std::result::Result;
    pub const OPERATION_NAME: &str = "getChargingSiteInformation";
    pub const QUERY : & str = "query getChargingSiteInformation($id: ChargingSiteIdentifierInputType!, $vehicleMakeType: ChargingVehicleMakeTypeEnum, $deviceCountry: String!, $deviceLanguage: String!) {\n  charging {\n    site(\n      id: $id\n      deviceCountry: $deviceCountry\n      deviceLanguage: $deviceLanguage\n      vehicleMakeType: $vehicleMakeType\n    ) {\n      siteStatic {\n        ...SiteStaticFragmentNoHoldAmt\n      }\n      siteDynamic {\n        ...SiteDynamicFragment\n      }\n      pricing(vehicleMakeType: $vehicleMakeType) {\n        userRates {\n          ...ChargingActiveRateFragment\n        }\n        memberRates {\n          ...ChargingActiveRateFragment\n        }\n        hasMembershipPricing\n        hasMSPPricing\n        canDisplayCombinedComparison\n      }\n      holdAmount(vehicleMakeType: $vehicleMakeType) {\n        currencyCode\n        holdAmount\n      }\n      congestionPriceHistogram(vehicleMakeType: $vehicleMakeType) {\n        ...CongestionPriceHistogramFragment\n      }\n      upsellingBanner(vehicleMakeType: $vehicleMakeType) {\n        header\n        caption\n        backgroundImageUrl\n        routeName\n      }\n    }\n  }\n}\n    \n    fragment SiteStaticFragmentNoHoldAmt on ChargingSiteStaticType {\n  address {\n    ...AddressFragment\n  }\n  amenities\n  centroid {\n    ...EnergySvcCoordinateTypeFields\n  }\n  entryPoint {\n    ...EnergySvcCoordinateTypeFields\n  }\n  id {\n    text\n  }\n  locationGUID\n accessCode {\n    value\n  }\n  localizedSiteName {\n    value\n  }\n  maxPowerKw {\n    value\n  }\n  name\n  openToPublic\n  chargers {\n    id {\n      text\n    }\n    label {\n      value\n    }\n  }\n  publicStallCount\n  timeZone {\n    id\n    version\n  }\n  fastchargeSiteId {\n    value\n  }\n  siteType\n  accessType\n  isMagicDockSupportedSite\n  trtId {\n    value\n  }\n  siteDisclaimer\n}\n    \n    fragment AddressFragment on EnergySvcAddressType {\n  streetNumber {\n    value\n  }\n  street {\n    value\n  }\n  district {\n    value\n  }\n  city {\n    value\n  }\n  state {\n    value\n  }\n  postalCode {\n    value\n  }\n  country\n}\n    \n\n    fragment EnergySvcCoordinateTypeFields on EnergySvcCoordinateType {\n  latitude\n  longitude\n}\n    \n\n    fragment SiteDynamicFragment on ChargingSiteDynamicType {\n  id {\n    text\n  }\n  chargersAvailable {\n    value\n  }\n  chargerDetails {\n    charger {\n      id {\n        text\n      }\n      label {\n        value\n      }\n      name\n    }\n    availability\n  }\n  waitEstimateBucket\n  currentCongestion\n  usabilityArchetype\n}\n    \n\n    fragment ChargingActiveRateFragment on ChargingActiveRateType {\n  activePricebook {\n    charging {\n      ...ChargingUserRateFragment\n    }\n    parking {\n      ...ChargingUserRateFragment\n    }\n    congestion {\n      ...ChargingUserRateFragment\n    }\n    service {\n      ...ChargingUserRateFragment\n    }\n    electricity {\n      ...ChargingUserRateFragment\n    }\n    priceBookID\n  }\n}\n    \n    fragment ChargingUserRateFragment on ChargingUserRateType {\n  currencyCode\n  programType\n  rates\n  buckets {\n    start\n    end\n  }\n  bucketUom\n  touRates {\n    enabled\n    activeRatesByTime {\n      startTime\n      endTime\n      rates\n    }\n  }\n  uom\n  vehicleMakeType\n  stateOfCharge\n  congestionGracePeriodSecs\n  congestionPercent\n}\n    \n\n    fragment CongestionPriceHistogramFragment on HistogramData {\n  axisLabels {\n    index\n    value\n  }\n  regionLabels {\n    index\n    value {\n      ...ChargingPriceFragment\n      ... on HistogramRegionLabelValueString {\n        value\n      }\n    }\n  }\n  chargingUom\n  parkingUom\n  parkingRate {\n    ...ChargingPriceFragment\n  }\n  data\n  activeBar\n  maxRateIndex\n  whenRateChanges\n  dataAttributes {\n    congestionThreshold\n    label\n  }\n}\n    \n    fragment ChargingPriceFragment on ChargingPrice {\n  currencyCode\n  price\n}";
    use super::*;
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct GeoLocationInput {
        pub latitude: f64,
        pub longitude: f64,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct ID {
        pub id: String,
        #[serde(rename = "type")]
        pub tpe: String,
        #[serde(rename = "programType")]
        pub program_type: String,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub id: ID,
        #[serde(rename = "deviceLanguage")]
        pub device_language: String,
        #[serde(rename = "deviceCountry")]
        pub device_country: String,
        #[serde(rename = "ttpLocale")]
        pub ttp_locale: String,
        #[serde(rename = "vehicleMakeType")]
        pub vehicle_make: String,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub charging: Charging,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Charging {
        #[serde(rename = "site")]
        pub site: Site,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Site {
        #[serde(rename = "siteStatic")]
        pub site_static: SiteStatic,
        #[serde(rename = "siteDynamic")]
        pub site_dynamic: SiteDynamic,
        pub pricing: Pricing,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SiteStatic {
        pub amenities: Vec<String>,
        #[serde(rename = "availableStalls")]
        pub available_stalls: Option<nearby_sites::ValueType>,
        pub centroid: GeoLocationInput,
        pub id: Option<nearby_sites::TextType>,
        #[serde(rename = "localizedSiteName")]
        pub localized_site_name: Option<nearby_sites::ValueType>,
        #[serde(rename = "accessCode")]
        pub access_code: Option<nearby_sites::ValueType>,
        #[serde(rename = "locationGUID")]
        pub location_guid: String,
        #[serde(rename = "maxPowerKw")]
        pub max_power: Option<nearby_sites::IntValueType>,
        #[serde(rename = "openToPublic")]
        pub open_to_public: bool,
        #[serde(rename = "trtId")]
        pub trt_id: nearby_sites::IntValueType,
        #[serde(rename = "totalStalls")]
        pub total_stalls: Option<nearby_sites::IntValueType>,
        #[serde(rename = "siteType")]
        pub site_type: nearby_sites::SiteType,
        #[serde(rename = "accessType")]
        pub access_type: nearby_sites::AccessType,
        #[serde(rename = "hasHighCongestion")]
        pub has_high_congestion: Option<bool>,
        #[serde(rename = "teslaExclusive")]
        pub tesla_exclusive: Option<bool>,
        pub chargers: Vec<ChargerStatic>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SiteDynamic {
        pub id: nearby_sites::TextType,
        #[serde(rename = "chargersAvailable")]
        pub chargers_available: Option<nearby_sites::IntValueType>,
        #[serde(rename = "chargerDetails")]
        pub charger_details: Vec<ChargerDynamic>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ChargerStatic {
        pub id: nearby_sites::TextType,
        pub label: nearby_sites::ValueType,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ChargerDynamic {
        pub charger: ChargerDynamicInner,
        pub availability: Availability,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ChargerDynamicInner {
        pub id: nearby_sites::TextType,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Pricing {
        #[serde(rename = "userRates")]
        pub user_rates: UserRates,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct UserRates {
        #[serde(rename = "activePricebook")]
        pub active_price: ActivePrice,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ActivePrice {
        pub charging: ChargingPrice,
        pub parking: ChargingPrice,
        #[serde(rename = "priceBookID")]
        pub price_book_id: i64,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ChargingPrice {
        #[serde(rename = "currencyCode")]
        pub currency_code: String,
        pub rates: Vec<f64>,
        pub uom: String,
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum Availability {
        Available,
        Occupied,
        Other(String),
    }
    impl ::serde::Serialize for Availability {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                Availability::Available => "CHARGER_AVAILABILITY_AVAILABLE",
                Availability::Occupied => "CHARGER_AVAILABILITY_OCCUPIED",
                Availability::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for Availability {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "CHARGER_AVAILABILITY_AVAILABLE" => Ok(Availability::Available),
                "CHARGER_AVAILABILITY_OCCUPIED" => Ok(Availability::Occupied),
                _ => Ok(Availability::Other(s)),
            }
        }
    }
    impl Default for Availability {
        fn default() -> Availability {
            Availability::Available
        }
    }
    impl Availability {
        pub fn to_string(&self) -> String {
            match self {
                Availability::Available => "Available".to_owned(),
                Availability::Occupied => "Occupied".to_owned(),
                _ => "Unknown".to_owned(),
            }
        }
    }
}
impl graphql_client::GraphQLQuery for ChargeSiteInfo {
    type Variables = charge_site_info::Variables;
    type ResponseData = charge_site_info::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: charge_site_info::QUERY,
            operation_name: charge_site_info::OPERATION_NAME,
        }
    }
}
