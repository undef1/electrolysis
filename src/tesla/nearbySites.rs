#![allow(clippy::all, warnings)]
pub struct NearbySites;
pub mod nearby_sites {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "GetNearbyChargingSites";
    pub const QUERY : & str = "\n    query GetNearbyChargingSites($args: GetNearbyChargingSitesRequestType!) {\n  charging {\n    nearbySites(args: $args) {\n      sitesAndDistances {\n        ...ChargingNearbySitesFragment\n      }\n    }\n               }\n}\n    \n    fragment ChargingNearbySitesFragment on ChargerSiteAndDistanceType {\n  availableStalls {\n    value\n  }\n  centroid {\n    ...EnergySvcCoordinateTypeFields\n  }\n  drivingDistanceMiles {\n    value\n  }\n               entryPoint {\n    ...EnergySvcCoordinateTypeFields\n  }\n  haversineDistanceMiles {\n    value\n  }\n  id {\n    text\n  }\n  locationGUID\n  localizedSiteName {\n    value\n  }\n  maxPowerKw {\n    value\n  }\n  trtId {\n               value\n  }\n  totalStalls {\n    value\n  }\n  siteType\n  accessType\n  waitEstimateBucket\n  hasHighCongestion\n  teslaExclusive\n  amenities\n  chargingAccessibility\n  ownerType\n  usabilityArchetype\n  accessHours {\n               shouldDisplay\n    openNow\n    hour\n  }\n  isMagicDockSupportedSite\n}\n    \n    fragment EnergySvcCoordinateTypeFields on EnergySvcCoordinateType {\n  latitude\n  longitude\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct GeoLocationInput {
        pub latitude: f64,
        pub longitude: f64,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct ID {
        pub id: String,
        #[serde(rename = "type")]
        pub tpe: String,
        #[serde(rename = "programType")]
        pub programType: String,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Args {
        #[serde(rename = "userLocation")]
        pub user_location: GeoLocationInput,
        #[serde(rename = "northwestCorner")]
        pub northwest_corner: GeoLocationInput,
        #[serde(rename = "southeastCorner")]
        pub southeast_corner: GeoLocationInput,
        #[serde(rename = "languageCode")]
        pub language_code: String,
        #[serde(rename = "countryCode")]
        pub country_code: String, // Seems to always be "US"
        pub filters: Vec<Filters>,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Filters {
        pub name: String, // always "myVehicles"
        #[serde(rename = "type")]
        pub tpe: String, // Always COMBO
        pub value: FiltersValue,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct FiltersValue {
        pub value: String, // Always empty
        #[serde(rename = "additionalProps")]
        pub additional_props: AdditionalProps,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct AdditionalProps {
        #[serde(rename = "chargingAccessibility")]
        pub charging_accessibility: String, // Always "ALL VEHICLES"
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub args: Args,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub charging: Charging,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Charging {
        #[serde(rename = "nearbySites")]
        pub nbs: NearBySites,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct NearBySites {
        #[serde(rename = "sitesAndDistances")]
        pub sites_and_distanes: Vec<Site>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Site {
        #[serde(rename = "availableStalls")]
        pub available_stalls: Option<IntValueType>,
        pub centroid: GeoLocationInput,
        pub id: TextType,
        #[serde(rename = "localizedSiteName")]
        pub localized_site_name: Option<ValueType>,
        #[serde(rename = "maxPowerKw")]
        pub max_power: Option<IntValueType>,
        #[serde(rename = "trtId")]
        pub trt_id: IntValueType,
        #[serde(rename = "totalStalls")]
        pub total_stalls: Option<IntValueType>,
        #[serde(rename = "siteType")]
        pub site_type: SiteType,
        #[serde(rename = "accessType")]
        pub access_type: AccessType,
        #[serde(rename = "hasHighCongestion")]
        pub has_high_congestion: bool,
        #[serde(rename = "teslaExclusive")]
        pub tesla_exclusive: Option<bool>,
    }

    #[derive(Clone, Debug)]
    pub enum SiteType {
        destination,
        supercharger,
        Other(String),
    }
    impl ::serde::Serialize for SiteType {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                SiteType::destination => "SITE_TYPE_DESTINATION_CHARGING",
                SiteType::supercharger => "SITE_TYPE_SUPERCHARGER",
                SiteType::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for SiteType {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "SITE_TYPE_DESTINATION_CHARGING" => Ok(SiteType::destination),
                "SITE_TYPE_SUPERCHARGER" => Ok(SiteType::supercharger),
                _ => Ok(SiteType::Other(s)),
            }
        }
    }
    impl Default for SiteType {
        fn default() -> SiteType {
            SiteType::supercharger
        }
    }

    #[derive(Clone, Debug)]
    pub enum AccessType {
        public,
        private,
        Other(String),
    }
    impl ::serde::Serialize for AccessType {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                AccessType::public => "ACCESS_TYPE_PUBLIC",
                AccessType::private => "ACCESS_TYPE_PRIVATE",
                AccessType::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for AccessType {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "ACCESS_TYPE_PUBLIC" => Ok(AccessType::public),
                "ACCESS_TYPE_PRIVATE" => Ok(AccessType::private),
                _ => Ok(AccessType::Other(s)),
            }
        }
    }
    impl Default for AccessType {
        fn default() -> AccessType {
            AccessType::public
        }
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct TextType {
        pub text: String,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ValueType {
        pub value: String,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct IntValueType {
        pub value: i64,
    }
}
impl graphql_client::GraphQLQuery for NearbySites {
    type Variables = nearby_sites::Variables;
    type ResponseData = nearby_sites::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: nearby_sites::QUERY,
            operation_name: nearby_sites::OPERATION_NAME,
        }
    }
}
