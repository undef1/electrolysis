#![allow(clippy::all, warnings)]
pub struct StartCharge;
pub mod start_charge {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "StartChargingSession";
    pub const QUERY : & str = "
    mutation StartChargingSession($postId: String!, $programType: ChargingProgramType!, $pricebookId: Int!, $deviceCountry: String, $deviceLanguage: String, $vehicleId: String) {
  charging {
    startSession(
      postId: $postId
      pricebookId: $pricebookId
      programType: $programType
      deviceCountry: $deviceCountry
      deviceLanguage: $deviceLanguage
      vehicleId: $vehicleId
    ) {
      authorizationSessionId
      sessionEvents
      startMessages
      reason
    }
  }
}";
    use super::*;
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        #[serde(rename = "postId")]
        pub post_id: String,
        #[serde(rename = "programType")]
        pub program_type: String,
        #[serde(rename = "pricebookId")]
        pub price_book_id: i64,
        #[serde(rename = "deviceLanguage")]
        pub device_language: String,
        #[serde(rename = "deviceCountry")]
        pub device_country: String,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub charging: Charging,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Charging {
        #[serde(rename = "startSession")]
        pub start_session: StartSession,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct StartSession {
        #[serde(rename = "authorizationSessionId")]
        pub authorization_session_id: Option<String>,
        #[serde(rename = "sessionEvents")]
        pub session_events: Option<String>,
        #[serde(rename = "startMessages")]
        pub start_messages: Option<String>,
        pub reason: Option<String>,
    }
}
impl graphql_client::GraphQLQuery for StartCharge {
    type Variables = start_charge::Variables;
    type ResponseData = start_charge::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: start_charge::QUERY,
            operation_name: start_charge::OPERATION_NAME,
        }
    }
}
