use crate::charge_session_window::ChargeSession;
use crate::charge_station::{Connector, Station, StationData};
use crate::chargefox::station::location_drawer::LocationDrawerLocationChargeStationsConnectorsPlug;
use crate::charger::{Charger, ChargerData, ChargerType};
use crate::gui::{runtime, Network};
use crate::tesla::consts;
use crate::tesla::login;
use crate::tesla::nearbySites::{nearby_sites, NearbySites};
use crate::tesla::preStartChargingValidation::{self, pre_start_charging_validation};
use crate::tesla::ChargeSessionByID::{self, charge_session_by_id};
use crate::tesla::ChargeSiteInfo::{self, charge_site_info};
use crate::tesla::CurrentChargeSession::{self, current_charge_session};
use crate::tesla::StartCharge::{self, start_charge};
use crate::tesla::StopCharge::{self, stop_charging};
use crate::user::User;
use graphql_client::reqwest::post_graphql;
use gtk::glib::clone;
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::Client;
use serde::Deserialize;
use std::cmp::Ordering;
use std::error::Error;

pub async fn populate_chargers(
    lat_ne: f64,
    long_ne: f64,
    lat_sw: f64,
    long_sw: f64,
    min_speed: i64,
) -> Result<Vec<Charger>, Box<dyn Error>> {
    // Tesla API expects NW and SE... We have to swap lat and long
    let mut chargers = Vec::<Charger>::new();
    let filters = vec![nearby_sites::Filters {
        name: "myVehicles".to_owned(),
        tpe: "COMBO".to_owned(),
        value: nearby_sites::FiltersValue {
            value: "".to_owned(),
            additional_props: nearby_sites::AdditionalProps {
                charging_accessibility: "ALL_VEHICLES".to_owned(),
            },
        },
    }];

    let vars = nearby_sites::Variables {
        args: nearby_sites::Args {
            user_location: nearby_sites::GeoLocationInput {
                latitude: lat_ne,
                longitude: long_ne,
            },
            northwest_corner: nearby_sites::GeoLocationInput {
                latitude: lat_ne,
                longitude: long_sw,
            },
            southeast_corner: nearby_sites::GeoLocationInput {
                latitude: lat_sw,
                longitude: long_ne,
            },
            language_code: "en".to_owned(),
            country_code: "US".to_owned(),
            filters,
        },
    };

    let token = login::check_or_refresh_token()?;
    let mut headers = HeaderMap::new();
    headers.insert(consts::XUA_HEADER, HeaderValue::from_str(consts::XUA)?);
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token).as_ref())?,
    );
    let client = Client::builder()
        .user_agent(consts::UA)
        .default_headers(headers)
        .build()?;
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<NearbySites, _>(&client, consts::URL, vars).await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        println!("{:#?}", resp);
        let data: nearby_sites::ResponseData = match resp?.data {
            Some(r) => r,
            None => return Ok(chargers),
        };
        for site in data.charging.nbs.sites_and_distanes {
            let name = match site.localized_site_name {
                Some(n) => n.value,
                None => "Unknown site".to_owned(),
            };
            let free = match site.available_stalls {
                Some(s) => s.value > 0,
                None => true,
            };
            let speed = match site.max_power {
                Some(p) => p.value,
                None => 0,
            };
            if speed < min_speed {
                continue;
            }
            let chargertype = match site.site_type {
                nearby_sites::SiteType::destination => ChargerType::AC,
                nearby_sites::SiteType::supercharger => ChargerType::DC,
                _ => ChargerType::AC,
            };
            let charger_data = ChargerData {
                id: site.trt_id.value.to_string(),
                lat: site.centroid.latitude,
                long: site.centroid.longitude,
                free,
                location: name,
                maxpower: speed,
                chargertype,
                network: Network::Tesla,
            };
            chargers.push(Charger::new(charger_data));
        }
    }
    Ok(chargers)
}

pub async fn populate_stations(charger: Charger) -> Result<Vec<Station>, Box<dyn Error>> {
    let mut stations = Vec::<Station>::new();
    let program_type = match charger.maxpower().cmp(&80) {
        Ordering::Less => consts::PROGRAM_DESTINATION,
        Ordering::Equal => consts::PROGRAM_SUPERCHARGER,
        Ordering::Greater => consts::PROGRAM_SUPERCHARGER,
    };
    let vars = charge_site_info::Variables {
        id: charge_site_info::ID {
            id: charger.id(),
            tpe: "TRT_ID".to_owned(),
            program_type: program_type.to_owned(),
        },
        device_language: "en".to_owned(),
        device_country: "us".to_owned(),
        ttp_locale: "en_US".to_owned(),
        vehicle_make: "NON_TESLA".to_owned(),
    };
    let token = login::check_or_refresh_token()?;
    let mut headers = HeaderMap::new();
    headers.insert(consts::XUA_HEADER, HeaderValue::from_str(consts::XUA)?);
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token).as_ref())?,
    );
    let client = Client::builder()
        .user_agent(consts::UA)
        .connection_verbose(true)
        .default_headers(headers)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp =
                post_graphql::<ChargeSiteInfo::ChargeSiteInfo, _>(&client, consts::URL, vars).await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let data: charge_site_info::ResponseData = match resp?.data {
            Some(r) => r,
            None => return Ok(stations),
        };
        println!("RESP: {:#?}", data.charging);
        let charge_stations = data.charging.site.site_static.chargers;
        let charge_stations_dyn = data.charging.site.site_dynamic.charger_details;
        let price: String;
        let uom: String;
        let idle: String;
        let idle_uom: String;
        if !data
            .charging
            .site
            .pricing
            .user_rates
            .active_price
            .charging
            .rates
            .is_empty()
        {
            price = format!(
                "${}",
                data.charging
                    .site
                    .pricing
                    .user_rates
                    .active_price
                    .charging
                    .rates[0]
            );
            uom = format!(
                "/{}",
                data.charging
                    .site
                    .pricing
                    .user_rates
                    .active_price
                    .charging
                    .uom
            );
        } else {
            price = "Unknown".to_owned();
            uom = "".to_owned();
        }
        if !data
            .charging
            .site
            .pricing
            .user_rates
            .active_price
            .parking
            .rates
            .is_empty()
        {
            idle = format!(
                "${}",
                data.charging
                    .site
                    .pricing
                    .user_rates
                    .active_price
                    .parking
                    .rates[0]
            );
            idle_uom = format!(
                "/{}",
                data.charging
                    .site
                    .pricing
                    .user_rates
                    .active_price
                    .parking
                    .uom
            );
        } else {
            idle = "Unknown".to_owned();
            idle_uom = "".to_owned();
        }
        let power = match data.charging.site.site_static.max_power {
            Some(p) => p.value,
            None => 0,
        };
        for station in charge_stations {
            println!("{:#?}", station);
            let mut availability = false;
            let mut availability_message = "Station details not found".to_owned();
            for sdyn in &charge_stations_dyn {
                if station.id.text == sdyn.charger.id.text {
                    availability = sdyn.availability == charge_site_info::Availability::Available;
                    availability_message = sdyn.availability.to_string();
                    break;
                }
            }
            let connector = Connector {
                active_charge_session: None,
                available: Some(availability),
                name: None,
                id: station.id.text.clone(),
                status: None,
                tethered: Some(true),
                plug: LocationDrawerLocationChargeStationsConnectorsPlug {
                    type_name: "".to_owned(),
                    icon_url: "".to_owned(),
                },
            };
            let data = StationData {
                connectors: Some(vec![connector]),
                enabled: true,
                id: station.id.text,
                idle_fee: format!("{}{}", idle, idle_uom),
                name: station.label.value,
                online: true,
                planned: false,
                power: format!("{}kW", power),
                pricing_plan: format!("{}{}", price.clone(), uom),
                public_charger: data.charging.site.site_static.open_to_public,
                unavailable: !availability,
                unavailable_message: availability_message,
                price_id: data
                    .charging
                    .site
                    .pricing
                    .user_rates
                    .active_price
                    .price_book_id,
                network: Network::Tesla,
                site_guid: Some(data.charging.site.site_static.location_guid.clone()),
                site_id: Some(charger.id()),
            };
            let station = Station::new(data);
            stations.push(station);
        }
    }
    Ok(stations)
}

#[allow(unused)]
pub async fn start_charge(station: Station) -> Result<String, Box<dyn Error>> {
    let power = station
        .power()
        .replace("kW", "")
        .parse::<i32>()
        .unwrap_or(0);
    let program_type = match power.cmp(&80) {
        Ordering::Less => consts::PROGRAM_DESTINATION,
        Ordering::Equal => consts::PROGRAM_SUPERCHARGER,
        Ordering::Greater => consts::PROGRAM_SUPERCHARGER,
    };
    let full_program_type = match power.cmp(&80) {
        Ordering::Less => consts::FULL_PROGRAM_DESTINATION,
        Ordering::Equal => consts::FULL_PROGRAM_SUPERCHARGER,
        Ordering::Greater => consts::FULL_PROGRAM_SUPERCHARGER,
    };
    let payment_source = match power.cmp(&80) {
        Ordering::Less => consts::PAYMENT_SOURCE_DESTINATION,
        Ordering::Equal => consts::PAYMENT_SOURCE_SUPERCHARGE,
        Ordering::Greater => consts::PAYMENT_SOURCE_SUPERCHARGE,
    };

    let vars = start_charge::Variables {
        post_id: station.id(),
        program_type: program_type.to_string(),
        price_book_id: station.station_data().price_id,
        device_language: "en".to_owned(),
        device_country: "US".to_owned(),
    };
    println!("VARS: {:#?}", vars);

    let token = login::check_or_refresh_token()?;
    let mut headers = HeaderMap::new();
    headers.insert(consts::XUA_HEADER, HeaderValue::from_str(consts::XUA)?);
    headers.insert("Charset", HeaderValue::from_str("utf-8")?);
    headers.insert("Cache-Control", HeaderValue::from_str("no-cache")?);
    headers.insert("Accept-Language", HeaderValue::from_str("en")?);
    headers.insert("Connection", HeaderValue::from_str("keep-alive")?);
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token).as_ref())?,
    );
    headers.insert(
        "X-Txid",
        HeaderValue::from_str(&uuid::Uuid::new_v4().to_string())?,
    );
    headers.insert(
        "X-Request-Id",
        HeaderValue::from_str(&uuid::Uuid::new_v4().to_string())?,
    );
    let client = Client::builder()
        .user_agent(consts::UA)
        .cookie_store(true)
        .connection_verbose(true)
        .http1_title_case_headers()
        .default_headers(headers)
        .build()
        .unwrap();
    // let user = user_details().await?;
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(#[strong] client, async move {
        let trt_id = station.station_data().site_id.unwrap().parse::<i64>().unwrap();
        let validation_vars = pre_start_charging_validation::Variables {
            trt_id,
            program_type: full_program_type.to_string(),
            device_language: "en".to_owned(),
            device_country: "US".to_owned(),
            ttp_locale: "en_US".to_owned(),
        };
        let validation_resp = post_graphql::<preStartChargingValidation::PreStartChargingValidation, _>(&client, consts::URL, validation_vars).await.unwrap().data.unwrap();
        println!("Validation Response: {:#?}", validation_resp);
        if !validation_resp.me.charging.can_start_charging.enabled {
            let ret = start_charge::ResponseData{
                charging: start_charge::Charging{
                    start_session: start_charge::StartSession{
                        authorization_session_id: None,
                        session_events: None,
                        start_messages: Some(format!("Pre-validation failed: {:#?}", validation_resp.me.charging.can_start_charging.errors)),
                        reason: None,
                    },
                },
            };
            sender.send(Err(std::io::Error::new(std::io::ErrorKind::InvalidData, format!("Pre-validation failed: {:#?}", validation_resp.me.charging.can_start_charging.errors)))).await.unwrap();
            sender.close();
            return
        }

        let resp = post_graphql::<StartCharge::StartCharge, _>(&client, consts::URL.to_owned() + "?deviceLanguage=en&deviceCountry=US&ttpLocale=en_US&operationName=StartChargingSession", vars).await;
        sender.send(Ok(resp)).await.unwrap();
    }));
    let resp = receiver.recv().await;
    let data = match resp???.data {
        Some(d) => d,
        None => return Err("No response received".into()),
    };
    match data.charging.start_session.start_messages {
        Some(m) => Ok(m),
        None => {
            println!("{:#?}", data);
            match data.charging.start_session.reason {
                Some(r) => Err(r.into()),
                None => Err("Starting charge failed for unknown reason".into()),
            }
        }
    }
}

#[derive(Deserialize)]
struct TeslaUser {
    email: Option<String>,
    #[serde(rename = "firstName")]
    first_name: Option<String>,
    #[serde(rename = "lastName")]
    last_name: Option<String>,
    #[serde(rename = "countryCode")]
    country_code: Option<String>,
}

#[derive(Deserialize)]
struct TeslaUserResponse {
    data: TeslaUser,
}
pub async fn user_details() -> Result<User, Box<dyn Error>> {
    let token = login::check_or_refresh_token()?;
    let mut headers = HeaderMap::new();
    headers.insert(consts::XUA_HEADER, HeaderValue::from_str(consts::XUA)?);
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token).as_ref())?,
    );
    let client = Client::builder()
        .user_agent(consts::UA)
        .default_headers(headers)
        .connection_verbose(true)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(#[strong] client, async move {
        let resp = client.get(consts::REST_URL.to_owned() + "/mobile-app/account/details?deviceLanguage=en&deviceCountry=US&ttpLocale=en_US").send().await;
        sender.send(resp).await.unwrap();
    }));
    let resp = receiver.recv().await??;
    let data: TeslaUserResponse = match resp.json().await {
        Ok(d) => d,
        Err(e) => return Err(format!("Unable to decode response {e:?}").into()),
    };
    Ok(User {
        email: data.data.email,
        first_name: data.data.first_name,
        last_name: data.data.last_name,
        confirmed: None,
        home_country_code: data.data.country_code,
        has_disabled_push_notifications: None,
    })
}

pub async fn stop_charge_session(id: &str) -> Result<(), Box<dyn Error>> {
    let token = login::check_or_refresh_token()?;
    let mut headers = HeaderMap::new();
    headers.insert(consts::XUA_HEADER, HeaderValue::from_str(consts::XUA)?);
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token).as_ref())?,
    );
    let client = Client::builder()
        .user_agent(consts::UA)
        .cookie_store(true)
        .connection_verbose(true)
        .http1_title_case_headers()
        .default_headers(headers)
        .build()
        .unwrap();
    let vars = stop_charging::Variables {
        authorization_session_id: id.to_string(),
    };
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp =
                post_graphql::<StopCharge::StopCharging, _>(&client, consts::URL, vars).await;
            sender.send(resp).await.unwrap();
        }
    ));
    let resp = receiver.recv().await;
    let data = match resp??.data {
        Some(d) => d,
        None => return Err("No response received".into()),
    };
    if data.charging.stop_session.is_request_delivered.is_none() {
        return Err("No indication of stop success from Tesla".into());
    } else if !data.charging.stop_session.is_request_delivered.unwrap() {
        return Err("Stop Charge Request was not delivered".into());
    }
    Ok(())
}

pub async fn charge_session() -> Result<ChargeSession, Box<dyn Error>> {
    let token = login::check_or_refresh_token()?;
    let mut headers = HeaderMap::new();
    headers.insert(consts::XUA_HEADER, HeaderValue::from_str(consts::XUA)?);
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token).as_ref())?,
    );
    let client = Client::builder()
        .user_agent(consts::UA)
        .cookie_store(true)
        .connection_verbose(true)
        .http1_title_case_headers()
        .default_headers(headers)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<CurrentChargeSession::CurrentChargeSession, _>(
                &client,
                consts::URL,
                current_charge_session::Variables {},
            )
            .await;
            sender.send(resp).await.unwrap();
        }
    ));
    let data = receiver
        .recv()
        .await??
        .data
        .expect("Unable to read charge session response");
    println!("{:#?}", data);
    if data.charging.active_sessions.is_none()
        || data
            .charging
            .active_sessions
            .clone()
            .unwrap()
            .authorization_session_id
            .is_none()
    {
        return Err("No current charge session found".into());
    }
    let cur_session = data.charging.active_sessions.unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(#[strong] client, async move {
        let resp = post_graphql::<ChargeSessionByID::ChargeSessionByID, _>(
            &client,
            consts::URL.to_owned() + "?deviceLanguage=en&deviceCountry=US&ttpLocale=en_US&operationName=GetChargingSessionById",
            charge_session_by_id::Variables{
                authorization_session_id: cur_session.authorization_session_id.unwrap(),
            },
        ).await;
        sender.send(resp).await.unwrap();
    }));
    let data = receiver
        .recv()
        .await??
        .data
        .expect("Unable to read charge session response");
    if data.charging.session_info.is_none() {
        return Err("Unable to read current charging session".into());
    }
    let session = data.charging.session_info.unwrap();
    let charge = "unknown".to_string();
    // let soc_str = match session.state_of_charge {
    //     Some(s) => s,
    //     None => "0".to_owned(),
    // };
    // let soc = f64::from_str(soc_str.as_ref()).unwrap_or(-1.0);
    let soc = match session.state_of_charge {
        Some(s) => s.value,
        None => -1,
    } as f64;
    let mut duration = std::time::Duration::new(0, 0);
    if session.charge_end_time.is_some() && session.charge_start_time_epoch.is_some() {
        duration = std::time::Duration::new(
            (session.charge_end_time.unwrap().seconds
                - session.charge_start_time_epoch.unwrap().seconds)
                .try_into()
                .unwrap(),
            0,
        );
    } else if session.charge_start_time_epoch.is_some() {
        let start = std::time::UNIX_EPOCH
            + std::time::Duration::from_secs(
                session
                    .charge_start_time_epoch
                    .unwrap()
                    .seconds
                    .try_into()
                    .unwrap(),
            );
        duration = std::time::SystemTime::now().duration_since(start)?;
    }
    let idle_grace_end = "Unknown".to_string();
    let paused = false;
    let stopped = false;
    let reduced = false;
    let total = "Unknown".to_string();
    Ok(ChargeSession {
        id: session.authorization_session_id.unwrap_or("".to_string()),
        charge,
        soc,
        duration: format!("{}s", duration.as_secs()),
        idle_grace_end,
        paused,
        stopped,
        reduced,
        total,
    })
}
