#![allow(clippy::all, warnings)]
pub struct ChargeSessionByID;
pub mod charge_session_by_id {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "GetChargingSessionById";
    pub const QUERY: &str = "query GetChargingSessionById($authorizationSessionId: String!) {
  charging {
    sessionInfo(authorizationSessionId: $authorizationSessionId) {
      ...SessionInfoFragment
    }
  }
}
    
    fragment SessionInfoFragment on ChargingSessionInfoType {
  authorizationSessionId
  cumulativeEnergy
  hasCumulativeEnergy
  cumulativeCost
  currency
  sessionEvents
  authorizationEndReason
  chargeEndTimeEpoch {
    ...GoogleProtoTimeStampFragment
  }
  chargeStartTimeEpoch {
    ...GoogleProtoTimeStampFragment
  }
  instantaneousPowerW {
    value
  }
  voltageV {
    value
  }
  currentA {
    value
  }
  currentServerTime
  stateOfCharge {
    value
  }
  timeToFull {
    ...GoogleProtoDurationFragment
  }
  timeToBulk {
    ...GoogleProtoDurationFragment
  }
}
    
    fragment GoogleProtoTimeStampFragment on GoogleProtoTimeStamp {
  nanos
  seconds
}
    

    fragment GoogleProtoDurationFragment on GoogleProtoDuration {
  nanos
  seconds
}
";
    use super::*;
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        #[serde(rename = "authorizationSessionId")]
        pub authorization_session_id: String,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub charging: Charging,
    }

    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Charging {
        #[serde(rename = "sessionInfo")]
        pub session_info: Option<SessionInfo>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SessionInfo {
        #[serde(rename = "authorizationSessionId")]
        pub authorization_session_id: Option<String>,
        #[serde(rename = "cumulativeEnergy")]
        pub site_id: String,
        #[serde(rename = "cumulativeCost")]
        pub cumulative_cost: f64,
        pub currency: String,
        #[serde(rename = "sessionEvents")]
        pub session_events: Option<Vec<String>>,
        #[serde(rename = "AuthorizationEndReason")]
        pub authorization_end_reason: Option<String>,
        #[serde(rename = "chargeEndTimeEpoch")]
        pub charge_end_time: Option<TimeStamp>,
        #[serde(rename = "chargeStartTimeEpoch")]
        pub charge_start_time_epoch: Option<TimeStamp>,
        #[serde(rename = "stateOfCharge")]
        pub state_of_charge: Option<Value>,
        #[serde(rename = "timeToFull")]
        pub time_to_full: Option<TimeStamp>,
        #[serde(rename = "timeToBulk")]
        pub time_to_bulk: Option<TimeStamp>,
        #[serde(rename = "instantaneousPowerW")]
        pub instantaneous_power_w: Option<Value>,
        #[serde(rename = "voltageV")]
        pub voltage: Option<Value>,
        #[serde(rename = "currentA")]
        pub current: Option<Value>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct TimeStamp {
        pub nanos: i64,
        pub seconds: i64,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Value {
        pub value: i64,
    }
}
impl graphql_client::GraphQLQuery for ChargeSessionByID {
    type Variables = charge_session_by_id::Variables;
    type ResponseData = charge_session_by_id::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: charge_session_by_id::QUERY,
            operation_name: charge_session_by_id::OPERATION_NAME,
        }
    }
}
