use crate::secret::secret;
use crate::tesla::consts;
use data_encoding::{BASE64, BASE64_NOPAD};
use rand::distributions::{Alphanumeric, DistString};
use reqwest::blocking::Client;
use reqwest::Url;
use ring::digest::{self, SHA256};
use serde::{Deserialize, Serialize};
use std::borrow::{Borrow, Cow};
use std::error::Error;

const AUTH_URL: &str = "https://auth.tesla.com/oauth2/v3/authorize";
const CLIENT_ID: &str = "ownerapi";
const CODE_CHALLENGE_METHOD: &str = "S256";
const REDIRECT_URI: &str = "tesla://auth/callback";
const RESPONSE_TYPE: &str = "code";
const SCOPE: &str = "openid%20email%20offline_access%20phone";
const TOKEN_URL: &str = "https://auth.tesla.com/en_au/oauth2/v3/token";
const GRANT_TYPE: &str = "authorization_code";
const GRANT_TYPE_REFRESH: &str = "refresh_token";

#[derive(Serialize, Debug, Clone)]
struct TokenBody {
    grant_type: String,
    client_id: String,
    code: String,
    code_verifier: String,
    redirect_uri: String,
}

#[derive(Serialize, Debug, Clone)]
struct RefreshTokenBody {
    grant_type: String,
    client_id: String,
    refresh_token: String,
    redirect_uri: String,
}

#[derive(Deserialize, Debug, Clone)]
struct TokenResp {
    access_token: String,
    #[allow(unused)]
    refresh_token: Option<String>,
    #[allow(unused)]
    expires_in: i64,
}

#[derive(Deserialize, Debug, Clone)]
struct Claims {
    exp: u64,
}

pub fn generate_login_url() -> (String, String) {
    let code_verifier = Alphanumeric.sample_string(&mut rand::thread_rng(), 86);
    // See https://github.com/timdorr/tesla-api/discussions/689 for advice on this code
    let code_challenge = BASE64
        .encode(digest::digest(&SHA256, &code_verifier.clone().into_bytes()).as_ref())
        .replace('+', "-")
        .replace('/', "_")
        .replace([' ', '='], "")
        .trim()
        .to_string();
    let state = Alphanumeric.sample_string(&mut rand::thread_rng(), 51);
    let query = [
        ("audience", ""),
        ("client_id", CLIENT_ID),
        ("code_challenge", &code_challenge),
        ("code_challenge_method", CODE_CHALLENGE_METHOD),
        ("is_in_app", "true"),
        ("locale", "en-US"),
        ("prompt", "login"),
        ("redirect_uri", REDIRECT_URI),
        ("response_type", RESPONSE_TYPE),
        ("scope", SCOPE),
        ("state", &state),
    ];
    let mut url = AUTH_URL.to_owned();
    url.push('?');
    for (q, v) in query {
        url.push_str(format!("{}={}&", q, v).as_ref());
    }
    (url, code_verifier)
}

pub fn login(redirect: String, code_verifier: String) -> Result<String, Box<dyn Error>> {
    let client = Client::builder()
        .user_agent(consts::UA)
        .cookie_store(true)
        .redirect(reqwest::redirect::Policy::none())
        .connection_verbose(true)
        .build()?;
    let url = Url::parse(redirect.as_ref())?;
    let mut code = "".to_owned();
    for (id, value) in url.query_pairs() {
        if <Cow<'_, str> as Borrow<str>>::borrow(&id) == "code" {
            code = <Cow<'_, str> as Borrow<str>>::borrow(&value).to_string();
            break;
        }
    }

    let body = TokenBody {
        grant_type: GRANT_TYPE.to_owned(),
        client_id: CLIENT_ID.to_owned(),
        code,
        code_verifier,
        redirect_uri: REDIRECT_URI.to_owned(),
    };
    let resp = client.post(TOKEN_URL).json(&body).send()?;
    let tresp = resp.json::<TokenResp>()?;
    if tresp.refresh_token.is_some() {
        secret::store_login("tesla-refresh", tresp.refresh_token.unwrap().as_ref())?;
    }
    Ok(tresp.access_token)
}

#[allow(dead_code)]
pub fn check_or_refresh_token() -> Result<String, Box<dyn Error>> {
    let token = secret::retreive_login_token("tesla")?;
    let current_exp = std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .expect("time is broken")
        .as_secs();
    let body = match token.split('.').nth(1) {
        Some(b) => b.to_owned(),
        None => return Err("split: Token was not a valid JWT, please re-login".into()),
    };
    let body_dec = match BASE64_NOPAD.decode(&body.into_bytes()) {
        Ok(d) => d,
        Err(_) => return Err("base64: Token was not a valid JWT, please re-login".into()),
    };
    let claims: Claims = match serde_json::from_slice(body_dec.as_ref()) {
        Ok(c) => c,
        Err(_) => return Err("decode: Token was not a valid JWT, please re-login".into()),
    };
    if claims.exp + 10 > current_exp {
        return Ok(token);
    }
    let refresh_token = match secret::retreive_login_token("tesla-refresh") {
        Ok(r) => r,
        Err(_) => return Err("secert: Could not find refresh token, please re-login".into()),
    };
    let client = match Client::builder()
        .user_agent(consts::UA)
        .cookie_store(true)
        .redirect(reqwest::redirect::Policy::none())
        .connection_verbose(true)
        .build()
    {
        Ok(c) => c,
        Err(_) => return Err("client: Could not refresh token, please re-login".into()),
    };
    let body = RefreshTokenBody {
        grant_type: GRANT_TYPE_REFRESH.to_owned(),
        client_id: CLIENT_ID.to_owned(),
        refresh_token,
        redirect_uri: REDIRECT_URI.to_owned(),
    };
    let resp = match client.post(TOKEN_URL).json(&body).send() {
        Ok(r) => r,
        Err(_) => return Err("request: Could not refresh token, please re-login".into()),
    };
    let tresp = match resp.json::<TokenResp>() {
        Ok(t) => t,
        Err(_) => return Err("decode: Could not refresh token, please re-login".into()),
    };
    if tresp.refresh_token.is_some() {
        let _ = secret::store_login("tesla-refresh", tresp.refresh_token.unwrap().as_ref());
    }
    Ok(tresp.access_token)
}
