pub mod InitiateChargeSession;
pub mod addPaymentCard;
pub mod addRfidCard;
pub mod currentUserSession;
pub mod homemap;
pub mod imp;
pub mod pin;
pub mod rfidorder;
pub mod signin;
pub mod signup;
pub mod station;
pub mod stopCharge;
pub mod consts {
    pub const URL: &str = "https://app.chargefox.com/graphql";
    pub const PINURL: &str = "https://api.pin.net.au/1/cards";
    pub const PINAPIKEY: &str = "pk_loZQYBzJdiD26AHBpzjk8Q";
    pub const UA: &str =
        "Mozilla/5.0 (Android 22 (5.1.1); SM-T280) Chargefox/2.42.0 (250) Expo/49.0.16";
}
pub mod scalars {
    pub type ISO8601DateTime = String;
}
