mod imp;

use adw::subclass::prelude::*;
use glib::Object;
use gtk::glib;
use serde::{Deserialize, Serialize};

glib::wrapper! {
    pub struct RfidCard(ObjectSubclass<imp::RfidCard>);
}

impl RfidCard {
    pub fn new(data: RfidCardData) -> Self {
        let s: RfidCard = Object::builder()
            .property("id", data.clone().id)
            .property("id-tag", data.clone().id_tag)
            .build();
        s.imp().data.replace(data);
        s
    }

    pub fn rfid_card_data(&self) -> RfidCardData {
        self.imp().data.borrow().clone()
    }

    pub fn from_rfid_card_data(data: RfidCardData) -> Self {
        Self::new(data)
    }
}

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct RfidCardData {
    pub id: String,
    pub id_tag: String,
}
