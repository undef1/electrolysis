use std::cell::RefCell;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::Properties;
use gtk::glib;

use super::RfidCardData;

#[derive(Properties, Default)]
#[properties(wrapper_type = super::RfidCard)]
pub struct RfidCard {
    #[property(name = "id", get, set, type = String, member = id)]
    #[property(name = "id-tag", get, set, type = String, member = id_tag)]
    pub data: RefCell<RfidCardData>,
}

#[glib::object_subclass]
impl ObjectSubclass for RfidCard {
    const NAME: &'static str = "RfidCard";
    type Type = super::RfidCard;
}

#[glib::derived_properties]
impl ObjectImpl for RfidCard {}
