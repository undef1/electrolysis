pub mod ChargeSessionByID;
pub mod ChargeSiteInfo;
pub mod CurrentChargeSession;
pub mod StartCharge;
pub mod StopCharge;
pub mod imp;
pub mod login;
pub mod nearbySites;
pub mod preStartChargingValidation;
pub mod consts {
    pub const URL: &str = "https://akamai-apigateway-charging-ownership.tesla.com/graphql";
    pub const REST_URL: &str = "https://ownership.tesla.com";
    pub const XUA: &str = "TeslaApp/4.38.0-2965/1003af026d/android/30";
    pub const XUA_HEADER: &str = "X-Tesla-User-Agent";
    pub const UA: &str = "okhttp/4.11.0";
    pub const PROGRAM_SUPERCHARGER: &str = "PTSCH";
    pub const PROGRAM_DESTINATION: &str = "PTWCR";
    pub const FULL_PROGRAM_SUPERCHARGER: &str = "PROGRAM_TYPE_PTSCH";
    pub const FULL_PROGRAM_DESTINATION: &str = "PROGRAM_TYPE_PTWCR";
    pub const PAYMENT_SOURCE_SUPERCHARGE: &str = "SUPERCHARGE";
    pub const PAYMENT_SOURCE_DESTINATION: &str = "DESTINATION";
}
