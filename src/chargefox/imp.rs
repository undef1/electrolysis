use crate::charge_session_window::ChargeSession;
use crate::charge_station::{Station, StationData};
use crate::chargefox::addRfidCard::{self, add_rfid_card};
use crate::chargefox::consts as cf;
use crate::chargefox::currentUserSession::{self, current_user_session};
use crate::chargefox::homemap::{self, home_map};
use crate::chargefox::signin::{self, sign_in};
use crate::chargefox::station::{location_drawer, LocationDrawer};
use crate::chargefox::stopCharge::{self, stop_charge_session};
use crate::chargefox::InitiateChargeSession::{self, initiate_charge_session};
use crate::charger::{Charger, ChargerData, ChargerType};
use crate::gui::{runtime, Network};
use crate::rfidcard::{RfidCard, RfidCardData};
use crate::secret::secret;
use crate::user::User;
use graphql_client::reqwest::post_graphql;
use gtk::glib::clone;
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::Client;
use std::error::Error;
use std::str::FromStr;

const SECRET_NAME: &str = "chargefox";

pub async fn populate_chargers(
    lat_ne: f64,
    long_ne: f64,
    lat_sw: f64,
    long_sw: f64,
    min_speed: i64,
) -> Result<Vec<Charger>, Box<dyn Error>> {
    let vars = home_map::Variables {
        ne: home_map::GeoLocationInput {
            lat: lat_ne,
            lng: long_ne,
        },
        sw: home_map::GeoLocationInput {
            lat: lat_sw,
            lng: long_sw,
        },
        filters: None,
    };
    let client = Client::builder().user_agent(cf::UA).build().unwrap();
    println!("Sending request");
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<homemap::HomeMap, _>(&client, cf::URL, vars).await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let data: home_map::ResponseData = match resp?.data {
            Some(r) => r,
            None => return Ok(vec![]),
        };
        let locations = match data.locations_by_bounds {
            Some(l) => l,
            None => {
                println!("No locations found");
                return Ok(vec![]);
            }
        };
        let chargers: Vec<Charger> = locations
            .into_iter()
            .filter_map(|location| {
                let speed = location
                    .map_location
                    .location_drawer_location_lean
                    .max_power
                    .unwrap_or("".to_string())
                    .to_lowercase()
                    .replace("kw", "")
                    .parse::<f64>()
                    .unwrap_or(0.0) as i64;
                if speed < min_speed {
                    return None;
                }
                let name = match location.map_location.location_drawer_location_lean.name {
                    Some(n) => n,
                    None => String::from("Unknown charger"),
                };
                let lat = location
                    .map_location
                    .markers_location
                    .markers_build_feature_collection_location
                    .lat;
                let long = location
                    .map_location
                    .markers_location
                    .markers_build_feature_collection_location
                    .lng;

                let free = location
                    .map_location
                    .markers_location
                    .markers_build_feature_collection_location
                    .markers_build_feature_collection_get_icon_key_location
                    .available_for_charging
                    .unwrap_or(false);

                let chargertype = match location
                    .map_location
                    .markers_location
                    .markers_build_feature_collection_location
                    .markers_build_feature_collection_get_icon_key_location
                    .charging_speed
                    .unwrap_or(home_map::ChargingSpeed::standard)
                {
                    home_map::ChargingSpeed::ultra_rapid => ChargerType::DC,
                    home_map::ChargingSpeed::fast => ChargerType::DC,
                    home_map::ChargingSpeed::standard => ChargerType::AC,
                    _ => ChargerType::AC,
                };

                let charger_data = ChargerData {
                    id: location.map_location.location_drawer_location_lean.id,
                    lat,
                    long,
                    free,
                    location: name,
                    maxpower: speed,
                    chargertype,
                    network: Network::Chargefox,
                };
                Some(Charger::new(charger_data))
            })
            .collect();
        return Ok(chargers);
    }
    Ok(vec![])
}

pub async fn populate_stations(charger: Charger) -> Result<Vec<Station>, Box<dyn Error>> {
    let mut stations = Vec::<Station>::new();
    let vars = location_drawer::Variables {
        location_id: charger.id(),
    };
    let client = Client::builder().user_agent(cf::UA).build().unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<LocationDrawer, _>(&client, cf::URL, vars).await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let data: location_drawer::ResponseData = match resp?.data {
            Some(r) => r,
            None => return Ok(stations),
        };
        let location = match data.location {
            Some(l) => l,
            None => {
                println!("No locations found");
                return Ok(stations);
            }
        };
        let name = match location.name {
            Some(n) => n,
            None => "Unknown".to_owned(),
        };
        let id = location.id;
        let power = match location.max_power {
            Some(p) => p,
            None => "Unknown".to_owned(),
        };
        let pricing_plan = match location.price_range {
            Some(p) => match p.description {
                Some(d) => d,
                None => "Unknown".to_owned(),
            },
            None => "Unknown".to_owned(),
        };
        let idle_fee = match location.idle_fee {
            Some(p) => match p.price_per_minute {
                Some(m) => match m.formatted {
                    Some(f) => f,
                    None => "Unknown".to_owned(),
                },
                None => "Unknown".to_owned(),
            },
            None => "Unknown".to_owned(),
        };
        let charge_stations = location.charge_stations.unwrap_or_default();
        for station in charge_stations {
            println!("{:#?}", station);
            let data = StationData {
                connectors: station.connectors,
                enabled: station.enabled.unwrap_or(true),
                id: id.clone(),
                idle_fee: idle_fee.clone(),
                name: station.name.unwrap_or(name.clone()),
                online: station.online.unwrap_or(true),
                planned: station.planned.unwrap_or(false),
                power: format!("{}kW", power.clone()),
                price_id: 0,
                pricing_plan: pricing_plan.clone(),
                public_charger: true, // The Chargefox API now errors when this is requested
                unavailable: station.unavailable.unwrap_or(false),
                unavailable_message: station.unavailable_message.unwrap_or("".to_owned()),
                network: Network::Chargefox,
                site_id: Some(charger.id()),
                site_guid: None,
            };
            let station = Station::new(data);
            stations.push(station);
        }
    }
    Ok(stations)
}

pub async fn login(user: String, password: String) -> Result<String, Box<dyn Error>> {
    let vars = sign_in::Variables {
        input: sign_in::SignInInput {
            email: user,
            password,
            client_mutation_id: None,
        },
    };
    println!("{:#?}", vars);
    let client = Client::builder().user_agent(cf::UA).build().unwrap();
    let resp = post_graphql::<signin::SignIn, _>(&client, cf::URL, vars);
    let data: sign_in::ResponseData = match resp.await?.data {
        Some(d) => d,
        None => return Err("No data returned from server".into()),
    };
    println!("{}", serde_json::to_string(&data).unwrap());
    let token = match data.sign_in {
        Some(s) => match s.token {
            Some(t) => t,
            None => return Err("No token returned".into()),
        },
        None => return Err("Null signin data returned".into()),
    };
    Ok(token)
}
pub async fn start_charge(
    connector: location_drawer::LocationDrawerLocationChargeStationsConnectors,
) -> String {
    let token = match secret::retreive_login_token(SECRET_NAME) {
        Ok(t) => t,
        Err(_) => "".to_owned(),
    };
    if token.is_empty() {
        return "No token found".to_owned();
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token.trim()).as_str()).unwrap(),
    );
    let client = Client::builder()
        .user_agent(cf::UA)
        .default_headers(headers)
        .build()
        .unwrap();
    let vars = initiate_charge_session::Variables {
        input: initiate_charge_session::InitiateChargeSessionInput {
            connector_id: connector.id,
            client_mutation_id: None,
            fleet_ownership_id: None,
        },
    };
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<InitiateChargeSession::InitiateChargeSession, _>(
                &client,
                cf::URL,
                vars,
            )
            .await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let data: initiate_charge_session::ResponseData = match resp {
            Ok(r) => r.data.expect("Missing user data"),
            Err(e) => panic!("{:#?}", e),
        };
        println!("{data:#?}");
        if data.initiate_charge_session.is_some()
            && !data.initiate_charge_session.unwrap().errors.is_empty()
        {
            "Error Starting Charge".to_owned()
        } else {
            "Charge Started Successfully".to_owned()
        }
    } else {
        "Error Starting Charge".to_owned()
    }
}

pub async fn charge_session() -> Result<ChargeSession, Box<dyn Error>> {
    let token = secret::retreive_login_token(SECRET_NAME)?;
    if token.is_empty() {
        return Err("No user logged in".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token.trim()).as_str()).unwrap(),
    );
    let client = Client::builder()
        .user_agent(cf::UA)
        .default_headers(headers)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<currentUserSession::currentUserSession, _>(
                &client,
                cf::URL,
                current_user_session::Variables {},
            )
            .await;
            sender.send(resp).await.unwrap();
        }
    ));
    let data = receiver
        .recv()
        .await??
        .data
        .expect("Unable to read charge session response");
    // Ok(resp??.data.expect("Missing charge session data"))
    println!("{:#?}", data);
    if data
        .current_user_session
        .user
        .active_charge_session
        .is_none()
    {
        return Err("No current charge session found".into());
    }
    let session = data
        .current_user_session
        .user
        .active_charge_session
        .unwrap();
    let charge = match session.invoice {
        Some(c) => match c.amount_due {
            Some(a) => match a.format {
                Some(f) => f,
                None => "Unknown".to_owned(),
            },
            None => "Unknown".to_owned(),
        },
        None => "Unknown".to_owned(),
    };
    let soc_str = match session.state_of_charge {
        Some(s) => s,
        None => "0".to_owned(),
    };
    let soc = f64::from_str(soc_str.as_ref()).unwrap_or(-1.0);
    let duration = match session.duration {
        Some(d) => d.formatted,
        None => "Unknown".to_owned(),
    };
    let idle_grace_end = match session.idle_fee_grace_period_end_time {
        Some(e) => e,
        None => "Unknown".to_owned(),
    };
    let paused = session.paused.unwrap_or(false);
    let stopped = session.stopped.unwrap_or(false);
    let reduced = session.reduced.unwrap_or(false);
    let total = match session.total_consumption_formatted {
        Some(t) => t,
        None => "Unknown".to_owned(),
    };
    Ok(ChargeSession {
        id: session.id,
        charge,
        soc,
        duration,
        idle_grace_end,
        paused,
        stopped,
        reduced,
        total,
    })
}

pub async fn stop_charge_session(id: &str) -> Result<(), Box<dyn Error>> {
    let token = secret::retreive_login_token(SECRET_NAME)?;
    if token.is_empty() {
        return Err("No user logged in".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token.trim()).as_str()).unwrap(),
    );
    let client = Client::builder()
        .user_agent(cf::UA)
        .default_headers(headers)
        .build()
        .unwrap();
    let vars = stop_charge_session::Variables {
        input: stop_charge_session::StopChargeSessionInput {
            charge_session_id: id.to_string(),
            client_mutation_id: None,
        },
    };
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp =
                post_graphql::<stopCharge::StopChargeSession, _>(&client, cf::URL, vars).await;
            sender.send(resp).await.unwrap();
        }
    ));
    let resp = receiver.recv().await;
    let errs = resp??.data.unwrap().stop_charge_session.unwrap().errors;
    if !errs.is_empty() {
        let errors = errs
            .iter()
            .map(|e| e.message.clone())
            .collect::<Vec<String>>()
            .join("; ");
        return Err(errors.into());
    }
    Ok(())
    // Ok(resp??.data.expect("Missing stop charge response"))
}

pub async fn user_details() -> Result<User, Box<dyn Error>> {
    let token = match secret::retreive_login_token(SECRET_NAME) {
        Ok(t) => t,
        Err(_) => "".to_owned(),
    };
    if token.is_empty() {
        return Err("No user logged in".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token.trim()).as_str()).unwrap(),
    );
    let client = Client::builder()
        .user_agent(cf::UA)
        .default_headers(headers)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<currentUserSession::currentUserSession, _>(
                &client,
                cf::URL,
                current_user_session::Variables {},
            )
            .await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let data: current_user_session::ResponseData = match resp {
            Ok(r) => r.data.expect("Missing user data"),
            Err(e) => return Err(Box::new(e)),
        };
        return Ok(User {
            email: data.current_user_session.user.email,
            first_name: data.current_user_session.user.first_name,
            last_name: data.current_user_session.user.last_name,
            confirmed: data.current_user_session.user.confirmed,
            home_country_code: data.current_user_session.user.home_country_code,
            has_disabled_push_notifications: data
                .current_user_session
                .user
                .has_disabled_push_notifications,
        });
    };
    Err("Unable to receive user data".into())
}

pub async fn rfid_cards() -> Result<Vec<RfidCard>, Box<dyn Error>> {
    let mut cards = vec![];
    let token = match secret::retreive_login_token("chargefox") {
        Ok(t) => t,
        Err(_) => "".to_owned(),
    };
    if token.is_empty() {
        return Err("No user logged in".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token.trim()).as_str()).unwrap(),
    );
    let client = Client::builder()
        .user_agent(cf::UA)
        .default_headers(headers)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<currentUserSession::currentUserSession, _>(
                &client,
                cf::URL,
                current_user_session::Variables {},
            )
            .await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        let data: current_user_session::ResponseData = match resp {
            Ok(r) => r.data.expect("Missing user data"),
            Err(e) => panic!("{:#?}", e),
        };
        for card in data.current_user_session.user.id_cards {
            let data = RfidCardData {
                id: card.id,
                id_tag: card.id_tag.unwrap_or("Unknown".to_owned()),
            };
            let card = RfidCard::new(data);
            cards.push(card);
        }
    };
    Ok(cards)
}

pub async fn submit_rfid_card(id: String) -> Result<(), Box<dyn Error>> {
    let vars = add_rfid_card::Variables {
        input: add_rfid_card::AddIdCardInput {
            id_tag: id,
            client_mutation_id: None,
        },
    };
    println!("{:#?}", vars);
    let token = match secret::retreive_login_token("chargefox") {
        Ok(t) => t,
        Err(_) => "".to_owned(),
    };
    if token.is_empty() {
        return Err("No user logged in".into());
    }
    let mut headers = HeaderMap::new();
    headers.insert(
        "Authorization",
        HeaderValue::from_str(format!("Bearer {}", token.trim()).as_str()).unwrap(),
    );
    let client = Client::builder()
        .user_agent(cf::UA)
        .default_headers(headers)
        .build()
        .unwrap();
    let (sender, receiver) = async_channel::bounded(1);
    runtime().spawn(clone!(
        #[strong]
        client,
        async move {
            let resp = post_graphql::<addRfidCard::AddRfidCard, _>(&client, cf::URL, vars).await;
            sender.send(resp).await.unwrap();
        }
    ));
    if let Ok(resp) = receiver.recv().await {
        println!("{:#?}", resp);
        let data: add_rfid_card::ResponseData = match resp {
            Ok(r) => r.data.expect("Missing sign in data"),
            Err(e) => panic!("{:#?}", e),
        };
        println!("{}", serde_json::to_string(&data).unwrap());
    };
    Ok(())
}
