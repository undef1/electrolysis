#![allow(clippy::all, warnings)]
pub struct HomeMap;
pub mod home_map {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "HomeMap";
    pub const QUERY : & str = "query HomeMap($sw: GeoLocationInput!, $ne: GeoLocationInput!, $filters: LocationFilterInput) {\n  locationsByBounds(sw: $sw, ne: $ne, filters: $filters) {\n    ...Map_Location\n    __typename\n  }\n  loggedInUser {\n    id\n    ...UseActiveChargeSessionMonitor_LoggedInUser\n    __typename\n  }\n}\n\nfragment Map_Location on Location {\n  ...LocationDrawer_Location_Lean\n  ...Markers_Location\n  __typename\n}\n\nfragment LocationDrawer_Location_Lean on Location {\n  id\n  brandingLogoUrl\n  maxPower\n  name\n  planned\n  __typename\n}\n\nfragment Markers_Location on Location {\n  ...Markers_BuildFeatureCollection_Location\n  __typename\n}\n\nfragment Markers_BuildFeatureCollection_Location on Location {\n  ...Markers_BuildFeatureCollection_GetIconKey_Location\n  lat\n  lng\n  __typename\n}\n\nfragment Markers_BuildFeatureCollection_GetIconKey_Location on Location {\n  planned\n  availableForCharging\n  chargingSpeed\n  __typename\n}\n\nfragment UseActiveChargeSessionMonitor_LoggedInUser on User {\n  activeChargeSession {\n    id\n    __typename\n  }\n  __typename\n}\n\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Clone, Debug)]
    pub enum ChargingSpeed {
        ultra_rapid,
        fast,
        standard,
        Other(String),
    }
    impl ::serde::Serialize for ChargingSpeed {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                ChargingSpeed::ultra_rapid => "ultra_rapid",
                ChargingSpeed::fast => "fast",
                ChargingSpeed::standard => "standard",
                ChargingSpeed::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for ChargingSpeed {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "ultra_rapid" => Ok(ChargingSpeed::ultra_rapid),
                "fast" => Ok(ChargingSpeed::fast),
                "standard" => Ok(ChargingSpeed::standard),
                _ => Ok(ChargingSpeed::Other(s)),
            }
        }
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct GeoLocationInput {
        pub lat: Float,
        pub lng: Float,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct LocationFilterInput {
        #[serde(rename = "chargingSpeeds")]
        pub charging_speeds: Option<Vec<ChargingSpeed>>,
        #[serde(rename = "plugIds")]
        pub plug_ids: Option<Vec<ID>>,
        #[serde(rename = "freeForUser")]
        pub free_for_user: Option<Boolean>,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub sw: GeoLocationInput,
        pub ne: GeoLocationInput,
        pub filters: Option<LocationFilterInput>,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Map_Location {
        #[serde(flatten)]
        pub location_drawer_location_lean: LocationDrawer_Location_Lean,
        #[serde(flatten)]
        pub markers_location: Markers_Location,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawer_Location_Lean {
        pub id: ID,
        #[serde(rename = "brandingLogoUrl")]
        pub branding_logo_url: Option<String>,
        #[serde(rename = "maxPower")]
        pub max_power: Option<String>,
        pub name: Option<String>,
        pub planned: Option<Boolean>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Markers_Location {
        #[serde(flatten)]
        pub markers_build_feature_collection_location: Markers_BuildFeatureCollection_Location,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Markers_BuildFeatureCollection_Location {
        #[serde(flatten)]
        pub markers_build_feature_collection_get_icon_key_location:
            Markers_BuildFeatureCollection_GetIconKey_Location,
        pub lat: Float,
        pub lng: Float,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct Markers_BuildFeatureCollection_GetIconKey_Location {
        pub planned: Option<Boolean>,
        #[serde(rename = "availableForCharging")]
        pub available_for_charging: Option<Boolean>,
        #[serde(rename = "chargingSpeed")]
        pub charging_speed: Option<ChargingSpeed>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct UseActiveChargeSessionMonitor_LoggedInUser {
        #[serde(rename = "activeChargeSession")]
        pub active_charge_session:
            Option<UseActiveChargeSessionMonitorLoggedInUserActiveChargeSession>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct UseActiveChargeSessionMonitorLoggedInUserActiveChargeSession {
        pub id: ID,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        #[serde(rename = "locationsByBounds")]
        pub locations_by_bounds: Option<Vec<HomeMapLocationsByBounds>>,
        #[serde(rename = "loggedInUser")]
        pub logged_in_user: Option<HomeMapLoggedInUser>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct HomeMapLocationsByBounds {
        #[serde(flatten)]
        pub map_location: Map_Location,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct HomeMapLoggedInUser {
        pub id: ID,
        #[serde(flatten)]
        pub use_active_charge_session_monitor_logged_in_user:
            UseActiveChargeSessionMonitor_LoggedInUser,
    }
}
impl graphql_client::GraphQLQuery for HomeMap {
    type Variables = home_map::Variables;
    type ResponseData = home_map::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: home_map::QUERY,
            operation_name: home_map::OPERATION_NAME,
        }
    }
}
