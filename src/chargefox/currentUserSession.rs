#![allow(clippy::all, warnings)]
pub struct currentUserSession;
pub mod current_user_session {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "currentUserSession";
    pub const QUERY : & str = "query currentUserSession {\n  currentUserSession {\n    __typename\n\tid\n\tuser {\n\t\tconfirmed\n\t\temail\n\t\tfirstName\n\t\thasDisabledPushNotifications\n\t\thomeCountryCode\n\t\tid\n\t\tlastName\n        idCards {\n        \tid\n        \tidTag\n        \t__typename\n        }\n\t\tactiveChargeSession {\n\t\t\tcurrentMeterValue\n\t\t\tduration {\n\t\t\t\tformatted\n\t\t\t}\n\t\t\tid\n\t\t\tidleFeeGracePeriodEndTime\n\t\t\tidleFeesAccruing\n\t\t\tinvoice {\n\t\t\t\tamountDue {\n\t\t\t\t\tformat\n\t\t\t\t}\n\t\t\t}\n\t\t\tpaused\n\t\t\treduced\n\t\t\tstartTime\n\t\t\tstateOfCharge\n\t\t\tstopTime\n\t\t\tstopped\n\t\t\ttotalConsumptionFormatted\n\t\t}\n\t}\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    type ISO8601DateTime = crate::chargefox::scalars::ISO8601DateTime;
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct Variables;
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct ResponseData {
        #[serde(rename = "currentUserSession")]
        pub current_user_session: CurrentUserSessionCurrentUserSession,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CurrentUserSessionCurrentUserSession {
        pub id: ID,
        pub user: CurrentUserSessionCurrentUserSessionUser,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CurrentUserSessionCurrentUserSessionUser {
        pub confirmed: Option<Boolean>,
        pub email: Option<String>,
        #[serde(rename = "firstName")]
        pub first_name: Option<String>,
        #[serde(rename = "hasDisabledPushNotifications")]
        pub has_disabled_push_notifications: Option<Boolean>,
        #[serde(rename = "homeCountryCode")]
        pub home_country_code: Option<String>,
        pub id: ID,
        #[serde(rename = "lastName")]
        pub last_name: Option<String>,
        #[serde(rename = "idCards")]
        pub id_cards: Vec<CurrentUserSessionCurrentUserSessionUserIdCards>,
        #[serde(rename = "activeChargeSession")]
        pub active_charge_session:
            Option<CurrentUserSessionCurrentUserSessionUserActiveChargeSession>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CurrentUserSessionCurrentUserSessionUserIdCards {
        pub id: ID,
        #[serde(rename = "idTag")]
        pub id_tag: Option<String>,
        #[serde(flatten)]
        pub on: CurrentUserSessionCurrentUserSessionUserIdCardsOn,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    #[serde(tag = "__typename")]
    pub enum CurrentUserSessionCurrentUserSessionUserIdCardsOn {
        PhysicalIdCard,
        VirtualIdCard,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CurrentUserSessionCurrentUserSessionUserActiveChargeSession {
        #[serde(rename = "currentMeterValue")]
        pub current_meter_value: Option<String>,
        pub duration: Option<CurrentUserSessionCurrentUserSessionUserActiveChargeSessionDuration>,
        pub id: ID,
        #[serde(rename = "idleFeeGracePeriodEndTime")]
        pub idle_fee_grace_period_end_time: Option<ISO8601DateTime>,
        #[serde(rename = "idleFeesAccruing")]
        pub idle_fees_accruing: Option<Boolean>,
        pub invoice: Option<CurrentUserSessionCurrentUserSessionUserActiveChargeSessionInvoice>,
        pub paused: Option<Boolean>,
        pub reduced: Option<Boolean>,
        #[serde(rename = "startTime")]
        pub start_time: ISO8601DateTime,
        #[serde(rename = "stateOfCharge")]
        pub state_of_charge: Option<String>,
        #[serde(rename = "stopTime")]
        pub stop_time: Option<ISO8601DateTime>,
        pub stopped: Option<Boolean>,
        #[serde(rename = "totalConsumptionFormatted")]
        pub total_consumption_formatted: Option<String>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CurrentUserSessionCurrentUserSessionUserActiveChargeSessionDuration {
        pub formatted: String,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CurrentUserSessionCurrentUserSessionUserActiveChargeSessionInvoice {
        #[serde(rename = "amountDue")]
        pub amount_due:
            Option<CurrentUserSessionCurrentUserSessionUserActiveChargeSessionInvoiceAmountDue>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CurrentUserSessionCurrentUserSessionUserActiveChargeSessionInvoiceAmountDue {
        pub format: Option<String>,
    }
}
impl graphql_client::GraphQLQuery for currentUserSession {
    type Variables = current_user_session::Variables;
    type ResponseData = current_user_session::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: current_user_session::QUERY,
            operation_name: current_user_session::OPERATION_NAME,
        }
    }
}
