#![allow(clippy::all, warnings)]
pub struct CreateRfidCardOrder;
pub mod create_rfid_card_order {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "CreateRfidCardOrder";
    pub const QUERY : & str = "mutation CreateRfidCardOrder($input: CreateRfidCardOrderInput!) {\n  createRfidCardOrder(input: $input ){\n\tclientMutationId\n\terrors {\n\t\tattribute\n\t\textensions {\n\t\t\terrorCode\n\t\t\tid\n\t\t} \n\t\tmessage\n\t\tid\n\t\tpath\n\t}\n\trfidCardOrder {\n\t\tcity\n\t\tcountryCode\n\t\tid\n\t\torderNumber\n\t\tpostcode\n\t\tstate\n\t\tstreetAddress1\n\t\tstreetAddress2\n\t}\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct CreateRfidCardOrderInput {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        #[serde(rename = "streetAddress1")]
        pub street_address1: String,
        #[serde(rename = "streetAddress2")]
        pub street_address2: Option<String>,
        pub city: String,
        pub state: String,
        pub postcode: String,
        #[serde(rename = "countryCode")]
        pub country_code: String,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub input: CreateRfidCardOrderInput,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        #[serde(rename = "createRfidCardOrder")]
        pub create_rfid_card_order: Option<CreateRfidCardOrderCreateRfidCardOrder>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct CreateRfidCardOrderCreateRfidCardOrder {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        pub errors: Vec<CreateRfidCardOrderCreateRfidCardOrderErrors>,
        #[serde(rename = "rfidCardOrder")]
        pub rfid_card_order: Option<CreateRfidCardOrderCreateRfidCardOrderRfidCardOrder>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct CreateRfidCardOrderCreateRfidCardOrderErrors {
        pub attribute: Option<String>,
        pub extensions: Option<CreateRfidCardOrderCreateRfidCardOrderErrorsExtensions>,
        pub message: String,
        pub id: ID,
        pub path: Option<Vec<String>>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct CreateRfidCardOrderCreateRfidCardOrderErrorsExtensions {
        #[serde(rename = "errorCode")]
        pub error_code: Option<String>,
        pub id: ID,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct CreateRfidCardOrderCreateRfidCardOrderRfidCardOrder {
        pub city: String,
        #[serde(rename = "countryCode")]
        pub country_code: String,
        pub id: ID,
        #[serde(rename = "orderNumber")]
        pub order_number: String,
        pub postcode: String,
        pub state: String,
        #[serde(rename = "streetAddress1")]
        pub street_address1: String,
        #[serde(rename = "streetAddress2")]
        pub street_address2: Option<String>,
    }
}
impl graphql_client::GraphQLQuery for CreateRfidCardOrder {
    type Variables = create_rfid_card_order::Variables;
    type ResponseData = create_rfid_card_order::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: create_rfid_card_order::QUERY,
            operation_name: create_rfid_card_order::OPERATION_NAME,
        }
    }
}
