#![allow(clippy::all, warnings)]
pub struct AddRfidCard;
pub mod add_rfid_card {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "AddRfidCard";
    pub const QUERY : & str = "mutation AddRfidCard($input: AddIdCardInput!) {\n  addIdCard(input: $input) {\n    loggedInUser {\n      id\n      idCards {\n        id\n        idTag\n        __typename\n      }\n      __typename\n    }\n    errors {\n      attribute\n      message\n      __typename\n    }\n    __typename\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct AddIdCardInput {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        #[serde(rename = "idTag")]
        pub id_tag: String,
    }
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct Variables {
        pub input: AddIdCardInput,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct ResponseData {
        #[serde(rename = "addIdCard")]
        pub add_id_card: Option<AddRfidCardAddIdCard>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddRfidCardAddIdCard {
        #[serde(rename = "loggedInUser")]
        pub logged_in_user: AddRfidCardAddIdCardLoggedInUser,
        pub errors: Vec<AddRfidCardAddIdCardErrors>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddRfidCardAddIdCardLoggedInUser {
        pub id: ID,
        #[serde(rename = "idCards")]
        pub id_cards: Vec<AddRfidCardAddIdCardLoggedInUserIdCards>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddRfidCardAddIdCardLoggedInUserIdCards {
        pub id: ID,
        #[serde(rename = "idTag")]
        pub id_tag: Option<String>,
        #[serde(flatten)]
        pub on: AddRfidCardAddIdCardLoggedInUserIdCardsOn,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    #[serde(tag = "__typename")]
    pub enum AddRfidCardAddIdCardLoggedInUserIdCardsOn {
        PhysicalIdCard,
        VirtualIdCard,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddRfidCardAddIdCardErrors {
        pub attribute: Option<String>,
        pub message: String,
    }
}
impl graphql_client::GraphQLQuery for AddRfidCard {
    type Variables = add_rfid_card::Variables;
    type ResponseData = add_rfid_card::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: add_rfid_card::QUERY,
            operation_name: add_rfid_card::OPERATION_NAME,
        }
    }
}
