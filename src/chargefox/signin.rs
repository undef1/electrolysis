#![allow(clippy::all, warnings)]
pub struct SignIn;
pub mod sign_in {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "SignIn";
    pub const QUERY : & str = "mutation SignIn($input: SignInInput!) {\n  signIn(input: $input ){\n    clientMutationId\n\terrors {\n\t\tattribute\n\t\textensions {\n\t\t\terrorCode\n\t\t\tid\n\t\t} \n\t\tmessage\n\t\tid\n\t\tpath\n\t}\n\tloggedInUser {\n\t\tconfirmed\n\t\tcurrentPaymentMechanism {\n\t\t\tcustomerName\n\t\t\tcustomerToken\n\t\t\tdetailsUpdated\n\t\t\tdisplayNumber\n\t\t\texpiryMonth\n\t\t\texpiryYear\n\t\t\tid\n\t\t\trequiresUpdating\n\t\t}\n\t\temail\n\t\tfirstName\n\t\thasDisabledPushNotifications\n\t\thomeCountryCode\n\t\tid\n\t\tuuid\n\t}\n    token\n    __typename\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct SignInInput {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        pub email: String,
        pub password: String,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub input: SignInInput,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        #[serde(rename = "signIn")]
        pub sign_in: Option<SignInSignIn>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SignInSignIn {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        pub errors: Vec<SignInSignInErrors>,
        #[serde(rename = "loggedInUser")]
        pub logged_in_user: Option<SignInSignInLoggedInUser>,
        pub token: Option<String>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SignInSignInErrors {
        pub attribute: Option<String>,
        pub extensions: Option<SignInSignInErrorsExtensions>,
        pub message: String,
        pub id: ID,
        pub path: Option<Vec<String>>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SignInSignInErrorsExtensions {
        #[serde(rename = "errorCode")]
        pub error_code: Option<String>,
        pub id: ID,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SignInSignInLoggedInUser {
        pub confirmed: Option<Boolean>,
        #[serde(rename = "currentPaymentMechanism")]
        pub current_payment_mechanism: Option<SignInSignInLoggedInUserCurrentPaymentMechanism>,
        pub email: Option<String>,
        #[serde(rename = "firstName")]
        pub first_name: Option<String>,
        #[serde(rename = "hasDisabledPushNotifications")]
        pub has_disabled_push_notifications: Option<Boolean>,
        #[serde(rename = "homeCountryCode")]
        pub home_country_code: Option<String>,
        pub id: ID,
        pub uuid: Option<String>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct SignInSignInLoggedInUserCurrentPaymentMechanism {
        #[serde(rename = "customerName")]
        pub customer_name: Option<String>,
        #[serde(rename = "customerToken")]
        pub customer_token: Option<String>,
        #[serde(rename = "detailsUpdated")]
        pub details_updated: Option<Boolean>,
        #[serde(rename = "displayNumber")]
        pub display_number: Option<String>,
        #[serde(rename = "expiryMonth")]
        pub expiry_month: Option<String>,
        #[serde(rename = "expiryYear")]
        pub expiry_year: Option<String>,
        pub id: ID,
        #[serde(rename = "requiresUpdating")]
        pub requires_updating: Option<Boolean>,
    }
}
impl graphql_client::GraphQLQuery for SignIn {
    type Variables = sign_in::Variables;
    type ResponseData = sign_in::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: sign_in::QUERY,
            operation_name: sign_in::OPERATION_NAME,
        }
    }
}
