#![allow(clippy::all, warnings)]
pub struct AddLoggedInUserPaymentMechanism;
pub mod add_logged_in_user_payment_mechanism {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "AddLoggedInUserPaymentMechanism";
    pub const QUERY : & str = "mutation AddLoggedInUserPaymentMechanism($input: AddLoggedInUserPaymentMechanismInput!) {\n  addLoggedInUserPaymentMechanism(input: $input) {\n    loggedInUser {\n      id\n      idCards {\n        id\n        idTag\n        __typename\n      }\n\t  currentPaymentMechanism {\n\t\t  customerName\n\t\t  customerToken\n\t\t  displayNumber\n\t\t  expiryMonth\n\t\t  expiryYear\n\t\t  id\n\t\t  requiresUpdating\n\t  }\n      __typename\n    }\n    errors {\n      attribute\n      message\n      __typename\n    }\n    __typename\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Clone, Debug)]
    pub enum PaymentPlatform {
        pin,
        Other(String),
    }
    impl ::serde::Serialize for PaymentPlatform {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                PaymentPlatform::pin => "pin",
                PaymentPlatform::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for PaymentPlatform {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "pin" => Ok(PaymentPlatform::pin),
                _ => Ok(PaymentPlatform::Other(s)),
            }
        }
    }
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct AddLoggedInUserPaymentMechanismInput {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        #[serde(rename = "paymentPlatform")]
        pub payment_platform: PaymentPlatform,
        #[serde(rename = "cardToken")]
        pub card_token: Option<String>,
    }
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct Variables {
        pub input: AddLoggedInUserPaymentMechanismInput,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct ResponseData {
        #[serde(rename = "addLoggedInUserPaymentMechanism")]
        pub add_logged_in_user_payment_mechanism:
            Option<AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanism>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanism {
        #[serde(rename = "loggedInUser")]
        pub logged_in_user:
            AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUser,
        pub errors: Vec<AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismErrors>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUser { pub id : ID , # [serde (rename = "idCards")] pub id_cards : Vec < AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserIdCards > , # [serde (rename = "currentPaymentMechanism")] pub current_payment_mechanism : Option < AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserCurrentPaymentMechanism > , }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserIdCards {
        pub id: ID,
        #[serde(rename = "idTag")]
        pub id_tag: Option<String>,
        #[serde(flatten)]
        pub on: AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserIdCardsOn,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    #[serde(tag = "__typename")]
    pub enum AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserIdCardsOn {
        PhysicalIdCard,
        VirtualIdCard,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserCurrentPaymentMechanism
    {
        #[serde(rename = "customerName")]
        pub customer_name: Option<String>,
        #[serde(rename = "customerToken")]
        pub customer_token: Option<String>,
        #[serde(rename = "displayNumber")]
        pub display_number: Option<String>,
        #[serde(rename = "expiryMonth")]
        pub expiry_month: Option<String>,
        #[serde(rename = "expiryYear")]
        pub expiry_year: Option<String>,
        pub id: ID,
        #[serde(rename = "requiresUpdating")]
        pub requires_updating: Option<Boolean>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismErrors {
        pub attribute: Option<String>,
        pub message: String,
    }
}
impl graphql_client::GraphQLQuery for AddLoggedInUserPaymentMechanism {
    type Variables = add_logged_in_user_payment_mechanism::Variables;
    type ResponseData = add_logged_in_user_payment_mechanism::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: add_logged_in_user_payment_mechanism::QUERY,
            operation_name: add_logged_in_user_payment_mechanism::OPERATION_NAME,
        }
    }
}
