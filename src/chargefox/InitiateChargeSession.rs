#![allow(clippy::all, warnings)]
pub struct InitiateChargeSession;
pub mod initiate_charge_session {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "InitiateChargeSession";
    pub const QUERY : & str = "mutation InitiateChargeSession($input: InitiateChargeSessionInput!) {\n  initiateChargeSession(input: $input ){\n\tclientMutationId\n\terrors {\n\t\tattribute\n\t\textensions {\n\t\t\terrorCode\n\t\t\tid\n\t\t} \n\t\tmessage\n\t\tid\n\t\tpath\n\t}\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct InitiateChargeSessionInput {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        #[serde(rename = "connectorId")]
        pub connector_id: ID,
        #[serde(rename = "fleetOwnershipId")]
        pub fleet_ownership_id: Option<ID>,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub input: InitiateChargeSessionInput,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        #[serde(rename = "initiateChargeSession")]
        pub initiate_charge_session: Option<InitiateChargeSessionInitiateChargeSession>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct InitiateChargeSessionInitiateChargeSession {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        pub errors: Vec<InitiateChargeSessionInitiateChargeSessionErrors>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct InitiateChargeSessionInitiateChargeSessionErrors {
        pub attribute: Option<String>,
        pub extensions: Option<InitiateChargeSessionInitiateChargeSessionErrorsExtensions>,
        pub message: String,
        pub id: ID,
        pub path: Option<Vec<String>>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct InitiateChargeSessionInitiateChargeSessionErrorsExtensions {
        #[serde(rename = "errorCode")]
        pub error_code: Option<String>,
        pub id: ID,
    }
}
impl graphql_client::GraphQLQuery for InitiateChargeSession {
    type Variables = initiate_charge_session::Variables;
    type ResponseData = initiate_charge_session::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: initiate_charge_session::QUERY,
            operation_name: initiate_charge_session::OPERATION_NAME,
        }
    }
}
