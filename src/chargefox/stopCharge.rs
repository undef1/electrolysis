#![allow(clippy::all, warnings)]
pub struct StopChargeSession;
pub mod stop_charge_session {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "StopChargeSession";
    pub const QUERY : & str = "mutation StopChargeSession($input: StopChargeSessionInput!) {\n  stopChargeSession(input: $input ){\n\tclientMutationId\n\terrors {\n\t\tattribute\n\t\textensions {\n\t\t\terrorCode\n\t\t\tid\n\t\t} \n\t\tmessage\n\t\tid\n\t\tpath\n\t}\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct StopChargeSessionInput {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        #[serde(rename = "chargeSessionId")]
        pub charge_session_id: ID,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub input: StopChargeSessionInput,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        #[serde(rename = "stopChargeSession")]
        pub stop_charge_session: Option<StopChargeSessionStopChargeSession>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct StopChargeSessionStopChargeSession {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        pub errors: Vec<StopChargeSessionStopChargeSessionErrors>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct StopChargeSessionStopChargeSessionErrors {
        pub attribute: Option<String>,
        pub extensions: Option<StopChargeSessionStopChargeSessionErrorsExtensions>,
        pub message: String,
        pub id: ID,
        pub path: Option<Vec<String>>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct StopChargeSessionStopChargeSessionErrorsExtensions {
        #[serde(rename = "errorCode")]
        pub error_code: Option<String>,
        pub id: ID,
    }
}
impl graphql_client::GraphQLQuery for StopChargeSession {
    type Variables = stop_charge_session::Variables;
    type ResponseData = stop_charge_session::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: stop_charge_session::QUERY,
            operation_name: stop_charge_session::OPERATION_NAME,
        }
    }
}
