#![allow(clippy::all, warnings)]
pub struct LocationDrawer;
pub mod location_drawer {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "LocationDrawer";
    pub const QUERY : & str = "query LocationDrawer($locationId: ID!) {\n  location(id: $locationId) {\n      chargingSpeed\n      maxPower\n      idleFee {\n        gracePeriodDuration {\n          formatted\n          __typename\n        }\n        pricePerMinute {\n          formatted\n          __typename\n        }\n        __typename\n      }\n      planned\n      priceRange {\n        description\n        timeDescription\n        energyDescription\n        free\n        __typename\n      }\n      name\n      address {\n          abbreviated\n      }\n      id\n      lat\n      lng\n      untetheredConnectors\n      tetheredConnectors\n      chargeStations {\n        online\n        planned\n        enabled\n        name\n        id\n        unavailable\n        unavailableMessage\n        publicCharger\n        connectors {\n          activeChargeSession {\n              startTime\n              stateOfCharge\n          }\n          available\n          name\n          id\n          status\n          tethered\n          plug {\n            typeName\n            iconUrl\n          }\n        }\n      }\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    type ISO8601DateTime = crate::chargefox::scalars::ISO8601DateTime;
    #[derive(Clone, Debug)]
    pub enum ChargingSpeed {
        ultra_rapid,
        fast,
        standard,
        Other(String),
    }
    impl ::serde::Serialize for ChargingSpeed {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                ChargingSpeed::ultra_rapid => "ultra_rapid",
                ChargingSpeed::fast => "fast",
                ChargingSpeed::standard => "standard",
                ChargingSpeed::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for ChargingSpeed {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "ultra_rapid" => Ok(ChargingSpeed::ultra_rapid),
                "fast" => Ok(ChargingSpeed::fast),
                "standard" => Ok(ChargingSpeed::standard),
                _ => Ok(ChargingSpeed::Other(s)),
            }
        }
    }
    #[derive(Clone, Debug)]
    pub enum ConnectorStatus {
        available,
        occupied,
        unavailable,
        faulted,
        unknown,
        Other(String),
    }
    impl ::serde::Serialize for ConnectorStatus {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                ConnectorStatus::available => "available",
                ConnectorStatus::occupied => "occupied",
                ConnectorStatus::unavailable => "unavailable",
                ConnectorStatus::faulted => "faulted",
                ConnectorStatus::unknown => "unknown",
                ConnectorStatus::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for ConnectorStatus {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "available" => Ok(ConnectorStatus::available),
                "occupied" => Ok(ConnectorStatus::occupied),
                "unavailable" => Ok(ConnectorStatus::unavailable),
                "faulted" => Ok(ConnectorStatus::faulted),
                "unknown" => Ok(ConnectorStatus::unknown),
                _ => Ok(ConnectorStatus::Other(s)),
            }
        }
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        #[serde(rename = "locationId")]
        pub location_id: ID,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        pub location: Option<LocationDrawerLocation>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocation {
        #[serde(rename = "chargingSpeed")]
        pub charging_speed: Option<ChargingSpeed>,
        #[serde(rename = "maxPower")]
        pub max_power: Option<String>,
        #[serde(rename = "idleFee")]
        pub idle_fee: Option<LocationDrawerLocationIdleFee>,
        pub planned: Option<Boolean>,
        #[serde(rename = "priceRange")]
        pub price_range: Option<LocationDrawerLocationPriceRange>,
        pub name: Option<String>,
        pub address: Option<LocationDrawerLocationAddress>,
        pub id: ID,
        pub lat: Float,
        pub lng: Float,
        #[serde(rename = "untetheredConnectors")]
        pub untethered_connectors: Option<Boolean>,
        #[serde(rename = "tetheredConnectors")]
        pub tethered_connectors: Option<Boolean>,
        #[serde(rename = "chargeStations")]
        pub charge_stations: Option<Vec<LocationDrawerLocationChargeStations>>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationIdleFee {
        #[serde(rename = "gracePeriodDuration")]
        pub grace_period_duration: Option<LocationDrawerLocationIdleFeeGracePeriodDuration>,
        #[serde(rename = "pricePerMinute")]
        pub price_per_minute: Option<LocationDrawerLocationIdleFeePricePerMinute>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationIdleFeeGracePeriodDuration {
        pub formatted: String,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationIdleFeePricePerMinute {
        pub formatted: Option<String>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationPriceRange {
        pub description: Option<String>,
        #[serde(rename = "timeDescription")]
        pub time_description: Option<String>,
        #[serde(rename = "energyDescription")]
        pub energy_description: Option<String>,
        pub free: Option<Boolean>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationAddress {
        pub abbreviated: String,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationChargeStations {
        pub online: Option<Boolean>,
        pub planned: Option<Boolean>,
        pub enabled: Option<Boolean>,
        pub name: Option<String>,
        pub id: ID,
        pub unavailable: Option<Boolean>,
        #[serde(rename = "unavailableMessage")]
        pub unavailable_message: Option<String>,
        #[serde(rename = "publicCharger")]
        pub public_charger: Option<Boolean>,
        pub connectors: Option<Vec<LocationDrawerLocationChargeStationsConnectors>>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationChargeStationsConnectors {
        #[serde(rename = "activeChargeSession")]
        pub active_charge_session:
            Option<LocationDrawerLocationChargeStationsConnectorsActiveChargeSession>,
        pub available: Option<Boolean>,
        pub name: Option<String>,
        pub id: ID,
        pub status: Option<ConnectorStatus>,
        pub tethered: Option<Boolean>,
        pub plug: LocationDrawerLocationChargeStationsConnectorsPlug,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationChargeStationsConnectorsActiveChargeSession {
        #[serde(rename = "startTime")]
        pub start_time: ISO8601DateTime,
        #[serde(rename = "stateOfCharge")]
        pub state_of_charge: Option<String>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct LocationDrawerLocationChargeStationsConnectorsPlug {
        #[serde(rename = "typeName")]
        pub type_name: String,
        #[serde(rename = "iconUrl")]
        pub icon_url: String,
    }
}
impl graphql_client::GraphQLQuery for LocationDrawer {
    type Variables = location_drawer::Variables;
    type ResponseData = location_drawer::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: location_drawer::QUERY,
            operation_name: location_drawer::OPERATION_NAME,
        }
    }
}
