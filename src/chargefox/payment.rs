#![allow(clippy::all, warnings)]
pub struct AddLoggedInUserPaymentMechanism;
pub mod add_logged_in_user_payment_mechanism {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "AddLoggedInUserPaymentMechanism";
    pub const QUERY : & str = "mutation AddLoggedInUserPaymentMechanism($input: AddLoggedInUserPaymentMechanismInput!) {\n  addLoggedInUserPaymentMechanism(input: $input ){\n\tclientMutationId\n\terrors {\n\t\tattribute\n\t\textensions {\n\t\t\terrorCode\n\t\t\tid\n\t\t} \n\t\tmessage\n\t\tid\n\t\tpath\n\t}\n\tloggedInUser {\n\t\tconfirmed\n\t\tcurrentPaymentMechanism {\n\t\t\tcustomerName\n\t\t\tcustomerToken\n\t\t\tdetailsUpdated\n\t\t\tdisplayNumber\n\t\t\texpiryMonth\n\t\t\texpiryYear\n\t\t\tid\n\t\t\trequiresUpdating\n\t\t}\n\t\temail\n\t\tfirstName\n\t\thasDisabledPushNotifications\n\t\thomeCountryCode\n\t\tid\n\t\tuuid\n\t}\n  }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Clone, Debug)]
    pub enum PaymentPlatform {
        pin,
        Other(String),
    }
    impl ::serde::Serialize for PaymentPlatform {
        fn serialize<S: serde::Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(match *self {
                PaymentPlatform::pin => "pin",
                PaymentPlatform::Other(ref s) => &s,
            })
        }
    }
    impl<'de> ::serde::Deserialize<'de> for PaymentPlatform {
        fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
            let s: String = ::serde::Deserialize::deserialize(deserializer)?;
            match s.as_str() {
                "pin" => Ok(PaymentPlatform::pin),
                _ => Ok(PaymentPlatform::Other(s)),
            }
        }
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct AddLoggedInUserPaymentMechanismInput {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        #[serde(rename = "paymentPlatform")]
        pub payment_platform: PaymentPlatform,
        #[serde(rename = "cardToken")]
        pub card_token: Option<String>,
    }
    #[derive(Serialize, Deserialize, Debug, Clone, Default)]
    pub struct Variables {
        pub input: AddLoggedInUserPaymentMechanismInput,
    }
    impl Variables {}
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct ResponseData {
        #[serde(rename = "addLoggedInUserPaymentMechanism")]
        pub add_logged_in_user_payment_mechanism:
            Option<AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanism>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanism {
        #[serde(rename = "clientMutationId")]
        pub client_mutation_id: Option<String>,
        pub errors: Vec<AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismErrors>,
        #[serde(rename = "loggedInUser")]
        pub logged_in_user:
            AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUser,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismErrors {
        pub attribute: Option<String>,
        pub extensions:
            Option<AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismErrorsExtensions>,
        pub message: String,
        pub id: ID,
        pub path: Option<Vec<String>>,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismErrorsExtensions {
        #[serde(rename = "errorCode")]
        pub error_code: Option<String>,
        pub id: ID,
    }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUser { pub confirmed : Boolean , # [serde (rename = "currentPaymentMechanism")] pub current_payment_mechanism : Option < AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserCurrentPaymentMechanism > , pub email : Option < String > , # [serde (rename = "firstName")] pub first_name : Option < String > , # [serde (rename = "hasDisabledPushNotifications")] pub has_disabled_push_notifications : Boolean , # [serde (rename = "homeCountryCode")] pub home_country_code : Option < String > , pub id : ID , pub uuid : Option < String > , }
    #[derive(Deserialize, Serialize, Debug, Clone, Default)]
    pub struct AddLoggedInUserPaymentMechanismAddLoggedInUserPaymentMechanismLoggedInUserCurrentPaymentMechanism
    {
        #[serde(rename = "customerName")]
        pub customer_name: Option<String>,
        #[serde(rename = "customerToken")]
        pub customer_token: Option<String>,
        #[serde(rename = "detailsUpdated")]
        pub details_updated: Option<Boolean>,
        #[serde(rename = "displayNumber")]
        pub display_number: Option<String>,
        #[serde(rename = "expiryMonth")]
        pub expiry_month: Option<String>,
        #[serde(rename = "expiryYear")]
        pub expiry_year: Option<String>,
        pub id: ID,
        #[serde(rename = "requiresUpdating")]
        pub requires_updating: Option<Boolean>,
    }
}
impl graphql_client::GraphQLQuery for AddLoggedInUserPaymentMechanism {
    type Variables = add_logged_in_user_payment_mechanism::Variables;
    type ResponseData = add_logged_in_user_payment_mechanism::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: add_logged_in_user_payment_mechanism::QUERY,
            operation_name: add_logged_in_user_payment_mechanism::OPERATION_NAME,
        }
    }
}
