use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize)]
pub struct Body {
    pub number: String,
    pub name: String,
    pub cvc: String,
    pub expiry_month: String,
    pub expiry_year: String,
    // Note: The below are set to "na" by Chargefox
    pub address_line1: String,
    pub address_city: String,
    pub address_country: String,
    pub publishable_api_key: String,
}

#[derive(Debug, Default, Deserialize)]
pub struct Response {
    pub response: Data,
}

#[derive(Debug, Default, Deserialize)]
pub struct Data {
    pub token: String,
}
