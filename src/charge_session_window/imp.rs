use crate::gui::{Network, APP_ID};
use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::subclass::InitializingObject;
use gtk::gio::Settings;
use gtk::{glib, CompositeTemplate};

#[derive(CompositeTemplate, Default)]
#[template(file = "../../resources/charge_session.ui")]
pub struct ChargeSessionWindow {
    #[template_child]
    pub charge: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub soc_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub soc: TemplateChild<gtk::LevelBar>,
    #[template_child]
    pub duration: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub idle_grace_end: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub paused: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub stopped: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub reduced: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub total: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub stop: TemplateChild<gtk::Button>,
    #[template_child]
    pub toast_overlay: TemplateChild<adw::ToastOverlay>,
}

#[glib::object_subclass]
impl ObjectSubclass for ChargeSessionWindow {
    const NAME: &'static str = "ChargeSessionWindow";
    type Type = super::ChargeSessionWindow;
    type ParentType = adw::PreferencesDialog;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for ChargeSessionWindow {
    fn constructed(&self) {
        self.parent_constructed();
        let settings = Settings::new(APP_ID);
        let network = Network::from_str(settings.string("network").as_ref());
        let obj = self.obj();
        let obj_clone = obj.clone();
        glib::spawn_future_local(async move {
            obj_clone.set_charge_session(network).await;
        });
    }
}

impl WidgetImpl for ChargeSessionWindow {}
impl PreferencesDialogImpl for ChargeSessionWindow {}
impl AdwDialogImpl for ChargeSessionWindow {}
