mod imp;

use crate::bp_pulse::imp as bp;
use crate::chargefox::imp as cfimp;
use crate::exploren::imp as exploren;
use crate::gui::Network;
use crate::tesla::imp as tesla;
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::Toast;
use adw::{gio, glib};
use glib::Object;
use gtk::Label;

glib::wrapper! {
    pub struct ChargeSessionWindow(ObjectSubclass<imp::ChargeSessionWindow>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}
impl Default for ChargeSessionWindow {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug)]
pub struct ChargeSession {
    pub id: String,
    pub charge: String,
    pub soc: f64,
    pub duration: String,
    pub idle_grace_end: String,
    pub paused: bool,
    pub stopped: bool,
    pub reduced: bool,
    pub total: String,
}

impl ChargeSessionWindow {
    pub fn new() -> Self {
        Object::builder().build()
    }
    pub async fn set_charge_session(&self, network: Network) {
        let data = match network {
            Network::Chargefox => match cfimp::charge_session().await {
                Ok(d) => d,
                Err(e) => {
                    println!("Error getting charge session: {e:#?}");
                    self.imp()
                        .charge
                        .get()
                        .set_subtitle(format!("{}", e).as_ref());
                    return;
                }
            },
            Network::Tesla => match tesla::charge_session().await {
                Ok(d) => d,
                Err(e) => {
                    println!("Error getting charge session: {e:#?}");
                    self.imp()
                        .charge
                        .get()
                        .set_subtitle(format!("{}", e).as_ref());
                    return;
                }
            },
            Network::Bppulse => match bp::charge_session().await {
                Ok(d) => d,
                Err(e) => {
                    println!("Error getting charge session: {e:#?}");
                    self.imp()
                        .charge
                        .get()
                        .set_subtitle(format!("{}", e).as_ref());
                    return;
                }
            },
            Network::Exploren => match exploren::charge_session().await {
                Ok(d) => d,
                Err(e) => {
                    println!("Error getting charge session: {e:#?}");
                    self.imp()
                        .charge
                        .get()
                        .set_subtitle(format!("{}", e).as_ref());
                    return;
                }
            },
            _ => {
                self.imp().charge.get().set_subtitle("Network Unsupported");
                return;
            }
        };
        println!("{:#?}", data);
        self.imp()
            .charge
            .get()
            .add_suffix(&Label::new(Some(data.charge.as_ref())));
        self.imp().soc.get().set_value(data.soc);
        self.imp().soc.get().add_offset_value("low", 20.0);
        self.imp().soc.get().add_offset_value("half", 50.0);
        self.imp().soc.get().add_offset_value("high", 80.0);
        self.imp().soc.get().add_offset_value("Full", 99.0);

        let soc_10 = (data.soc / 10.0) as i64 * 10;
        let soc_icon =
            // gtk::Image::from_icon_name(&"battery-level-60-charging-symbolic");
            gtk::Image::from_file(format!("/usr/share/icons/Adwaita/symbolic/status/battery-level-{soc_10}-charging-symbolic.svg"));
        self.imp().soc_row.get().add_suffix(&soc_icon);

        self.imp().duration.get().set_subtitle(&data.duration);
        self.imp()
            .idle_grace_end
            .get()
            .add_suffix(&Label::new(Some(data.idle_grace_end.as_ref())));
        self.imp()
            .paused
            .get()
            .add_suffix(&Label::new(Some(data.paused.to_string().as_ref())));
        self.imp()
            .stopped
            .get()
            .add_suffix(&Label::new(Some(data.stopped.to_string().as_ref())));
        self.imp()
            .reduced
            .get()
            .add_suffix(&Label::new(Some(data.reduced.to_string().as_ref())));
        self.imp()
            .total
            .get()
            .add_suffix(&Label::new(Some(&data.total)));
        let toast_overlay = self.imp().toast_overlay.get();
        self.imp().stop.get().connect_clicked(move |_| {
            let toast_overlay_clone = toast_overlay.clone();
            let id = data.id.clone();
            glib::spawn_future_local(async move {
                match network {
                    Network::Chargefox => match cfimp::stop_charge_session(&id).await {
                        Ok(d) => d,
                        Err(e) => {
                            println!("Error stopping charge: {e:#?}");
                            let toast = Toast::builder().timeout(5).title(format!("{}", e)).build();
                            toast_overlay_clone.add_toast(toast);
                            return;
                        }
                    },
                    Network::Tesla => match tesla::stop_charge_session(&id).await {
                        Ok(d) => d,
                        Err(e) => {
                            println!("Error stopping charge: {e:#?}");
                            let toast = Toast::builder().timeout(5).title(format!("{}", e)).build();
                            toast_overlay_clone.add_toast(toast);
                            return;
                        }
                    },
                    _ => {
                        let toast = Toast::builder()
                            .timeout(5)
                            .title("Unsupported network to stop chargge")
                            .build();
                        toast_overlay_clone.add_toast(toast);
                        return;
                    }
                };
                let toast = Toast::builder()
                    .timeout(5)
                    .title("Charge Successfuly Stopped")
                    .build();
                toast_overlay_clone.add_toast(toast);
            });
        });
        self.imp().stop.get().set_sensitive(true);
    }
}
