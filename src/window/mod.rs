mod imp;

use crate::bp_pulse;
use crate::chargefox;
use crate::charger::ChargerType;
use crate::evie;
use crate::exploren;
use crate::gui::Network;
use crate::tesla;

use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::{Application, StyleManager, Toast};
use glib::{clone, Object};
use gtk::{gio, glib};
use gtk::{Image, Label};
use shumate::prelude::LocationExt;
use shumate::Coordinate;
use shumate::Marker;

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends adw::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl Window {
    pub fn new(app: &Application) -> Self {
        Object::builder().property("application", app).build()
    }

    async fn refresh_button_triggered(
        &self,
        lat_ne: f64,
        long_ne: f64,
        lat_sw: f64,
        long_sw: f64,
        min_speed: i64,
    ) {
        let net = *self.imp().network.read().unwrap();
        let toast_overlay = self.imp().toast_overlay.get();
        // The APIs seems to want miles.
        let distance = Coordinate::new_full(lat_ne, long_ne)
            .distance(&Coordinate::new_full(lat_sw, long_sw))
            / 1609.344;
        let (toast_sender, toast_receiver) = async_channel::unbounded();
        let (charger_sender, charger_receiver) = async_channel::unbounded();
        glib::spawn_future_local(async move {
            let chargers = match net {
                Network::Chargefox => {
                    match chargefox::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, min_speed,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    }
                }
                Network::Tesla => {
                    match tesla::imp::populate_chargers(lat_ne, long_ne, lat_sw, long_sw, min_speed)
                        .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    }
                }
                Network::All => {
                    let mut c = match chargefox::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, min_speed,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    };
                    let mut t = match tesla::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, min_speed,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    };
                    c.append(&mut t);
                    let mut e = match evie::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, min_speed,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    };
                    c.append(&mut e);
                    let mut b = match bp_pulse::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, distance,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    };
                    c.append(&mut b);
                    let mut e2 = match exploren::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, min_speed,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    };
                    c.append(&mut e2);
                    c
                }
                Network::Evie => {
                    match evie::imp::populate_chargers(lat_ne, long_ne, lat_sw, long_sw, min_speed)
                        .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    }
                }
                Network::Bppulse => {
                    match bp_pulse::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, distance,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    }
                }
                Network::Exploren => {
                    match exploren::imp::populate_chargers(
                        lat_ne, long_ne, lat_sw, long_sw, min_speed,
                    )
                    .await
                    {
                        Ok(c) => c,
                        Err(e) => {
                            toast_sender
                                .send(format!("{}", e))
                                .await
                                .expect("The channel must be open");
                            return;
                        }
                    }
                }
            };
            if chargers.is_empty() {
                toast_sender
                    .send("No chargers found in region".to_owned())
                    .await
                    .expect("The channel must be open");
                return;
            }
            charger_sender
                .send(chargers)
                .await
                .expect("The channel must be open");
        });
        let charger_marker_layer = self.imp().marker_layer.borrow().clone();
        charger_marker_layer.remove_all();
        glib::spawn_future_local(clone!(
            #[weak]
            charger_marker_layer,
            async move {
                while let Ok(chargers) = charger_receiver.recv().await {
                    for charger in chargers {
                        // Some charger APIs dump everything, but on small, memory constrained devices
                        // everything is a little too much. Make sure chargers are actually inside the
                        // viewport.
                        if charger.lat() > lat_ne
                            || charger.lat() < lat_sw
                            || charger.long() > long_ne
                            || charger.long() < long_sw
                        {
                            continue;
                        }
                        let marker_label = Label::builder()
                            .label(charger.location())
                            .lines(2)
                            .max_width_chars(13)
                            .wrap(true)
                            .justify(gtk::Justification::Center)
                            .ellipsize(gtk::pango::EllipsizeMode::End)
                            .build();
                        if StyleManager::default().is_dark() {
                            marker_label.set_widget_name("maplabel-dark");
                        } else {
                            marker_label.set_widget_name("maplabel-light");
                        }
                        let icon_grid = gtk::Grid::builder()
                            .orientation(gtk::Orientation::Vertical)
                            .build();
                        let mut icon_file =
                            "/usr/share/icons/hicolor/scalable/maps-charger-end.svg";
                        if charger.free() {
                            icon_file =
                                "/usr/share/icons/hicolor/scalable/electric-car-symbolic.svg";
                        }
                        let icon = Image::builder()
                            .file(icon_file)
                            .pixel_size(25)
                            .tooltip_text(charger.location())
                            .build();
                        if charger.charger_type() == ChargerType::AC {
                            icon.set_widget_name("mapicon-ac");
                        } else if charger.maxpower() >= 200 {
                            icon.set_widget_name("mapicon-high-speed");
                        } else if charger.maxpower() >= 100 {
                            icon.set_widget_name("mapicon-medium-speed");
                        } else if charger.maxpower() >= 20 {
                            icon.set_widget_name("mapicon-low-speed");
                        } else {
                            icon.set_widget_name("mapicon");
                        }
                        icon_grid.attach(&icon, 0, 0, 1, 1);
                        icon_grid.attach(&marker_label, 0, 1, 1, 1);
                        icon_grid.attach(&charger, 0, 2, 1, 1); // HACK: adding this without
                                                                // width/height drops it.
                        let marker = Marker::builder()
                            .selectable(true)
                            .name(charger.location())
                            .latitude(charger.lat())
                            .longitude(charger.long())
                            .name(charger.location())
                            .child(&icon_grid)
                            .build();
                        charger_marker_layer.add_marker(&marker);
                    }
                }
            }
        ));

        glib::spawn_future_local(clone!(
            #[weak]
            toast_overlay,
            async move {
                while let Ok(title) = toast_receiver.recv().await {
                    let toast = Toast::builder().timeout(5).title(title).build();
                    toast_overlay.add_toast(toast);
                }
            }
        ));
    }
}
