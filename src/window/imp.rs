use adw::prelude::*;
use adw::subclass::prelude::*;

use adw::{StyleManager, Toast};
use glib::subclass::InitializingObject;
use gtk::{gdk, glib, CompositeTemplate, GestureLongPress};
use shumate::prelude::*;
use shumate::{Map, MapLayer, MarkerLayer, VectorRenderer};
use std::cell::RefCell;
use std::sync::RwLock;
use std::time;

use crate::charge_session_window::ChargeSessionWindow;
use crate::charge_station_window::ChargeStationWindow;
use crate::charger::Charger;
use crate::gui::{Network, APP_ID};
use crate::loginwindow::LoginWindow;
use crate::payments_window::PaymentsWindow;
use crate::preferenceswindow::PreferencesWindow;
use crate::rfidwindow::RfidWindow;
use crate::user::UserWindow;

#[derive(CompositeTemplate, Default)]
#[template(file = "../../resources/window.ui")]
pub struct Window {
    #[template_child]
    pub window_box: TemplateChild<gtk::Box>,
    #[template_child]
    pub header: TemplateChild<adw::HeaderBar>,
    #[template_child]
    pub main_menu: TemplateChild<gio::Menu>,
    #[template_child]
    pub toast_overlay: TemplateChild<adw::ToastOverlay>,
    pub map: RefCell<shumate::Map>,
    pub marker_layer: RefCell<shumate::MarkerLayer>,
    pub network: RwLock<Network>,
}

#[glib::object_subclass]
impl ObjectSubclass for Window {
    const NAME: &'static str = "MyGtkWindow";
    type Type = super::Window;
    type ParentType = adw::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for Window {
    fn constructed(&self) {
        self.parent_constructed();
        let obj = self.obj();

        let display = gdk::Display::default().expect("Could not get default display");
        let provider = gtk::CssProvider::new();
        provider.load_from_string(include_str!("../../resources/style.css"));
        gtk::style_context_add_provider_for_display(
            &display,
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        // Load config
        let settings = gio::Settings::new(APP_ID);
        let lat = settings.double("last-latitude");
        let long = settings.double("last-longitude");
        let zoom = settings.double("last-zoom");
        let network = Network::from_str(settings.string("network").as_ref());
        let mut n = obj.imp().network.write().unwrap();
        *n = network;

        // Setup Map
        let map = Map::builder()
            .animate_zoom(true)
            .zoom_on_double_click(true)
            .can_target(true)
            .focus_on_click(true)
            .hexpand(true)
            .vexpand(true)
            .margin_start(0)
            .margin_top(0)
            .name("map")
            .sensitive(true)
            .build();
        let map_source = if StyleManager::default().is_dark() {
            match VectorRenderer::new(
                "gnome-maps",
                include_str!("../../resources/gnome-maps-dark.json"),
            ) {
                Ok(ms) => ms,
                Err(e) => panic!("{}", e),
            }
        } else {
            match VectorRenderer::new(
                "gnome-maps",
                include_str!("../../resources/gnome-maps-light.json"),
            ) {
                Ok(ms) => ms,
                Err(e) => panic!("{}", e),
            }
        };
        map_source.set_min_zoom_level(1);
        let viewport = map.viewport().expect("Shumate map didn't have a viewport");
        viewport.set_reference_map_source(Some(&map_source));
        map.set_map_source(&map_source);
        let map_layer = MapLayer::new(&map_source, &viewport);
        map.add_layer(&map_layer);
        map.set_hexpand(true);
        map.set_vexpand(true);
        map.go_to_full_with_duration(lat, long, zoom, 0);
        obj.imp().map.replace(map.clone());

        let vp = match map.viewport() {
            Some(v) => v,
            None => return,
        };
        let charger_marker_layer = MarkerLayer::new_full(&vp, gtk::SelectionMode::Single);
        let parent_for_charge_station = self.obj().clone();
        charger_marker_layer.connect_marker_selected(move |_, m| {
            let charger = m
                .child()
                .and_downcast::<gtk::Grid>()
                .expect("The marker's child is not a Grid")
                .child_at(0, 2)
                .and_downcast::<Charger>()
                .expect("The Grid's child is not a Charger");
            let parent_for_charge_station = parent_for_charge_station.clone();
            glib::spawn_future_local(async move {
                let csw = ChargeStationWindow::new();
                csw.charger_triggered(charger).await;
                csw.present(Some(&parent_for_charge_station));
            });
        });
        obj.imp().marker_layer.replace(charger_marker_layer.clone());
        map.add_layer(&charger_marker_layer);
        obj.imp()
            .window_box
            .get()
            .insert_child_after(&map, Some(&obj.imp().header.get()));
        let obj_clone = obj.clone();
        let settings_clone = settings.clone();
        map.connect_realize(move |map| {
            let obj_clone2 = obj_clone.clone();
            let map_clone = map.clone();
            let settings_clone2 = settings_clone.clone();
            glib::spawn_future_local(async move {
                glib::timeout_future(time::Duration::from_millis(100)).await;
                let vp = match map_clone.viewport() {
                    Some(v) => v,
                    None => return,
                };
                let bounds = match map_clone.compute_bounds(&obj_clone2) {
                    Some(b) => b,
                    None => return,
                };
                let (lat_ne, long_ne) =
                    vp.widget_coords_to_location(&map_clone, f64::from(bounds.width()), 0.0);
                let (lat_sw, long_sw) =
                    vp.widget_coords_to_location(&map_clone, 0.0, f64::from(bounds.height()));
                let min_speed = settings_clone2.int64("min-speed");
                glib::spawn_future_local(async move {
                    obj_clone2
                        .refresh_button_triggered(lat_ne, long_ne, lat_sw, long_sw, min_speed)
                        .await;
                });
            });
        });
        let map_lp = GestureLongPress::new();
        let map_clone = map.clone();
        let obj_clone = obj.clone();
        let settings_clone = settings.clone();
        map_lp.connect_pressed(move |_, x, y| {
            let map_clone2 = map_clone.clone();
            let obj_clone2 = obj_clone.clone();
            let settings_clone2 = settings_clone.clone();
            glib::spawn_future_local(async move {
                let vp = match map_clone2.viewport() {
                    Some(v) => v,
                    None => return,
                };
                vp.set_rotation(0.0);
                let (lat, long) = vp.widget_coords_to_location(&map_clone2, x, y);
                settings_clone2
                    .set_double("last-latitude", lat)
                    .expect("Unable to set last latitude");
                settings_clone2
                    .set_double("last-longitude", long)
                    .expect("Unable to set last longitude");
                settings_clone2
                    .set_double("last-zoom", vp.zoom_level())
                    .expect("Unable to set last zoom");
                let bounds = match map_clone2.compute_bounds(&obj_clone2) {
                    Some(b) => b,
                    None => return,
                };
                let (lat_ne, long_ne) =
                    vp.widget_coords_to_location(&map_clone2, f64::from(bounds.width()), 0.0);
                let (lat_sw, long_sw) =
                    vp.widget_coords_to_location(&map_clone2, 0.0, f64::from(bounds.height()));
                println!("NE: {}, {}; SW: {}, {}", lat_ne, long_ne, lat_sw, long_sw);
                let min_speed = settings_clone2.int64("min-speed");
                glib::spawn_future_local(async move {
                    obj_clone2
                        .refresh_button_triggered(lat_ne, long_ne, lat_sw, long_sw, min_speed)
                        .await;
                });
            });
        });
        map.add_controller(map_lp);

        // Setup Menus
        let login_action = gio::SimpleAction::new("login", None);
        let parent_for_login = self.obj().clone();
        login_action.connect_activate(move |_, _| {
            let pref_window = LoginWindow::new();
            pref_window.present(Some(&parent_for_login));
        });
        let preferences_action = gio::SimpleAction::new("preferences", None);
        let parent_for_preferences = self.obj().clone();
        preferences_action.connect_activate(move |_, _| {
            let pref_window = PreferencesWindow::new();
            pref_window.present(Some(&parent_for_preferences));
        });
        let user_action = gio::SimpleAction::new("user", None);
        let parent_for_user = self.obj().clone();
        user_action.connect_activate(move |_, _| {
            let parent_for_user = parent_for_user.clone();
            glib::spawn_future_local(async move {
                let network = *parent_for_user.imp().network.read().unwrap();
                let pref_window = UserWindow::new();
                match pref_window.set_user(&network).await {
                    Ok(_) => {
                        pref_window.present(Some(&parent_for_user));
                    }
                    Err(e) => {
                        let toast = Toast::builder().timeout(5).title(e.to_string()).build();
                        parent_for_user.imp().toast_overlay.add_toast(toast);
                    }
                }
            });
        });
        let rfid_action = gio::SimpleAction::new("rfid", None);
        let parent_for_rfid = self.obj().clone();
        rfid_action.connect_activate(move |_, _| {
            let network = *parent_for_rfid.imp().network.read().unwrap();
            let parent_for_rfid = parent_for_rfid.clone();
            glib::spawn_future_local(async move {
                let pref_window = RfidWindow::new();
                match pref_window.set_cards(network).await {
                    Ok(_) => {
                        pref_window.present(Some(&parent_for_rfid));
                    }
                    Err(e) => {
                        let toast = Toast::builder().timeout(5).title(e.to_string()).build();
                        parent_for_rfid.imp().toast_overlay.add_toast(toast);
                    }
                };
            });
        });
        let session_action = gio::SimpleAction::new("session", None);
        let parent_for_session = self.obj().clone();
        session_action.connect_activate(move |_, _| {
            let pref_window = ChargeSessionWindow::new();
            pref_window.present(Some(&parent_for_session));
        });
        let payments_action = gio::SimpleAction::new("payments", None);
        let parent_for_payments = self.obj().clone();
        payments_action.connect_activate(move |_, _| {
            let pref_window = PaymentsWindow::new();
            pref_window.present(Some(&parent_for_payments));
        });
        let network_action = gio::SimpleAction::new_stateful(
            "network",
            Some(glib::VariantTy::STRING),
            &network.to_variant(),
        );
        let obj_clone2 = obj.clone();
        let map_clone2 = map;
        let settings_clone2 = settings.clone();
        network_action.connect_activate(move |action, state| {
            let param = state
                .expect("could not get state")
                .get::<Network>()
                .expect("The state must be a network enum");
            action.set_state(&param.to_variant());
            *obj_clone2.imp().network.write().unwrap() = param;
            settings
                .set_string("network", param.to_str())
                .expect("Unable to set network");
            let map_clone3 = map_clone2.clone();
            let obj_clone3 = obj_clone2.clone();
            let settings_clone3 = settings_clone2.clone();
            glib::spawn_future_local(async move {
                let vp = match map_clone3.viewport() {
                    Some(v) => v,
                    None => return,
                };
                settings_clone3
                    .set_double("last-latitude", lat)
                    .expect("Unable to set last latitude");
                settings_clone3
                    .set_double("last-longitude", long)
                    .expect("Unable to set last longitude");
                settings_clone3
                    .set_double("last-zoom", vp.zoom_level())
                    .expect("Unable to set last zoom");
                let bounds = match map_clone3.compute_bounds(&obj_clone3) {
                    Some(b) => b,
                    None => return,
                };
                let (lat_ne, long_ne) =
                    vp.widget_coords_to_location(&map_clone3, f64::from(bounds.width()), 0.0);
                let (lat_sw, long_sw) =
                    vp.widget_coords_to_location(&map_clone3, 0.0, f64::from(bounds.height()));
                println!("NE: {}, {}; SW: {}, {}", lat_ne, long_ne, lat_sw, long_sw);
                let min_speed = settings_clone3.int64("min-speed");
                glib::spawn_future_local(async move {
                    obj_clone3
                        .refresh_button_triggered(lat_ne, long_ne, lat_sw, long_sw, min_speed)
                        .await;
                });
            });
        });
        // obj.add_action(&login_action);
        let menu_action_group = gio::SimpleActionGroup::new();
        menu_action_group.add_action(&login_action);
        menu_action_group.add_action(&preferences_action);
        menu_action_group.add_action(&user_action);
        menu_action_group.add_action(&rfid_action);
        menu_action_group.add_action(&session_action);
        menu_action_group.add_action(&payments_action);
        menu_action_group.add_action(&network_action);
        obj.insert_action_group("menu", Some(&menu_action_group));
    }
}

impl WidgetImpl for Window {}
impl WindowImpl for Window {}
impl AdwApplicationWindowImpl for Window {}
impl ApplicationWindowImpl for Window {}
