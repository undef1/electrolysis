use adw::subclass::prelude::*;

use glib::subclass::InitializingObject;
use gtk::{glib, CompositeTemplate};

#[derive(CompositeTemplate, Default)]
#[template(file = "../../resources/user.ui")]
pub struct UserWindow {
    #[template_child]
    pub email: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub first_name: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub last_name: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub confirmed: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub home_country_code: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub has_disabled_push_notifications: TemplateChild<adw::ActionRow>,
}

#[glib::object_subclass]
impl ObjectSubclass for UserWindow {
    const NAME: &'static str = "UserWindow";
    type Type = super::UserWindow;
    type ParentType = adw::PreferencesDialog;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for UserWindow {
    fn constructed(&self) {
        self.parent_constructed();
    }
}

impl WidgetImpl for UserWindow {}
impl PreferencesDialogImpl for UserWindow {}
impl AdwDialogImpl for UserWindow {}
