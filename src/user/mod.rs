mod imp;

use crate::bp_pulse::imp as bp;
use crate::chargefox::imp as cf;
use crate::exploren::imp as exploren;
use crate::gui::Network;
use crate::tesla::imp as tesla;

use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::{gio, glib};
use glib::Object;
use std::error::Error;

glib::wrapper! {
    pub struct UserWindow(ObjectSubclass<imp::UserWindow>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl Default for UserWindow {
    fn default() -> Self {
        Self::new()
    }
}

impl UserWindow {
    pub fn new() -> Self {
        Object::builder().build()
    }
    pub async fn set_user(&self, net: &Network) -> Result<(), Box<dyn Error>> {
        let res = match net {
            Network::Chargefox => cf::user_details().await,
            Network::Tesla => tesla::user_details().await,
            Network::Bppulse => bp::user_details().await,
            Network::Exploren => exploren::user_details().await,
            _ => Err(format!("Not Implemented for {net}").into()),
        };
        let user = match res {
            Ok(u) => u,
            Err(e) => return Err(e),
        };
        let email = match user.email {
            Some(e) => e,
            None => {
                self.imp().email.set_visible(false);
                "".to_owned()
            }
        };
        self.imp().email.get().set_subtitle(email.as_ref());
        let confirmed = match user.confirmed {
            Some(e) => format!("{}", e),
            None => {
                self.imp().confirmed.set_visible(false);
                "".to_owned()
            }
        };
        self.imp().confirmed.get().set_subtitle(confirmed.as_ref());
        let first_name = match user.first_name {
            Some(e) => e,
            None => {
                self.imp().first_name.set_visible(false);
                "".to_owned()
            }
        };
        self.imp()
            .first_name
            .get()
            .set_subtitle(first_name.as_ref());
        let has_disabled_push_notifications = match user.has_disabled_push_notifications {
            Some(e) => format!("{}", e),
            None => {
                self.imp()
                    .has_disabled_push_notifications
                    .set_visible(false);
                "".to_owned()
            }
        };
        self.imp()
            .has_disabled_push_notifications
            .get()
            .set_subtitle(has_disabled_push_notifications.as_ref());
        let last_name = match user.last_name {
            Some(e) => e,
            None => {
                self.imp().last_name.set_visible(false);
                "".to_owned()
            }
        };
        self.imp().last_name.get().set_subtitle(last_name.as_ref());
        let home_country_code = match user.home_country_code {
            Some(e) => e,
            None => {
                self.imp().home_country_code.set_visible(false);
                "".to_owned()
            }
        };
        self.imp()
            .home_country_code
            .get()
            .set_subtitle(home_country_code.as_ref());
        Ok(())
    }
}

pub struct User {
    pub email: Option<String>,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub confirmed: Option<bool>,
    pub home_country_code: Option<String>,
    pub has_disabled_push_notifications: Option<bool>,
}
